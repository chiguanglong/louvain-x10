G
1.Compile or delete file using makefile
	- make
	- make clean

2.Convert txt graph data into binary data.

	-./convert -i inputfilename.txt -o outputfilename.bin -n maxnode 

  If the txt graph data is directed graph data, use option -d.

	-./convert -i inputfilename.txt -o outputfilename.bin -n maxnode -d 

3.Calculate community 

	-./community  filename.bin
