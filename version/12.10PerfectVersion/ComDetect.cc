/*************************************************/
/* START of ComDetect */
#include <ComDetect.h>

#include <x10/lang/Int.h>
#include <x10/lang/Double.h>
#include <x10/lang/String.h>
#include <x10/array/Array.h>
#include <x10/io/Printer.h>
#include <x10/io/Console.h>
#include <x10/lang/Any.h>
#include <x10/lang/Boolean.h>
#include <x10/compiler/CompilerFlags.h>
#include <x10/lang/FailedDynamicCheckException.h>
#include <x10/util/IndexedMemoryChunk.h>
#include <x10/array/Region.h>
#include <x10/util/Timer.h>
#include <x10/lang/Long.h>
#include <Community.h>
#include <Graph.h>

//#line 5 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10FieldDecl_c

//#line 9 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10FieldDecl_c

//#line 10 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10FieldDecl_c

//#line 11 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10FieldDecl_c

//#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10FieldDecl_c

//#line 14 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10MethodDecl_c
void ComDetect::checkArgs(x10::array::Array<x10::lang::String*>* args) {
    
    //#line 16 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
    x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(reinterpret_cast<x10::lang::Any*>(x10aux::makeStringLit("Check args!")));
    
    //#line 18 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
    this->FMGL(filename) = (__extension__ ({
        
        //#line 18 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
        x10::array::Array<x10::lang::String*>* p__9767 = args;
        
        //#line 18 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": polyglot.ast.Empty_c
        ;
        
        //#line 18 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
        x10::lang::String* ret9774;
        
        //#line 18 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
        x10::array::Array<x10::lang::String*>* x__11759 = p__9767;
        
        //#line 18 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
        if (!((x10aux::struct_equals(x10aux::nullCheck(x__11759)->FMGL(rank),
                                     ((x10_int)1))))) {
            
            //#line 18 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
            if (true) {
                
                //#line 18 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": polyglot.ast.Throw_c
                x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
            }
            
        }
        
        //#line 18 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
        ret9774 = (__extension__ ({
            
            //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
            ;
            
            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
            x10::lang::String* ret11760;
            
            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
            goto __ret11761; __ret11761: {
            {
                
                //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                if (x10aux::nullCheck(x__11759)->FMGL(rail)) {
                    
                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                    ret11760 = (x10aux::nullCheck(x__11759)->FMGL(raw))->__apply(((x10_int)0));
                    
                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                    goto __ret11761_end_;
                } else {
                    
                    //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                    if (true && !(x10aux::nullCheck(x__11759)->FMGL(region)->contains(
                                    ((x10_int)0)))) {
                        
                        //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                        x10::array::Array<void>::raiseBoundsError(
                          ((x10_int)0));
                    }
                    
                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                    ret11760 = (x10aux::nullCheck(x__11759)->FMGL(raw))->__apply(((x10_int) ((((x10_int)0)) - (x10aux::nullCheck(x__11759)->
                                                                                                                 FMGL(layout_min0)))));
                    
                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                    goto __ret11761_end_;
                }
                
            }goto __ret11761_end_; __ret11761_end_: ;
            }
            ret11760;
            }))
            ;
        ret9774;
        }))
        ;
    
    //#line 20 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
    if (((x10aux::nullCheck(args)->FMGL(size)) > (((x10_int)1))))
    {
        
        //#line 22 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
        this->FMGL(it_random) = x10::lang::IntNatives::parseInt((__extension__ ({
            
            //#line 22 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10::array::Array<x10::lang::String*>* p__9779 =
              args;
            
            //#line 22 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": polyglot.ast.Empty_c
            ;
            
            //#line 22 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10::lang::String* ret9786;
            
            //#line 22 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10::array::Array<x10::lang::String*>* x__11762 =
              p__9779;
            
            //#line 22 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
            if (!((x10aux::struct_equals(x10aux::nullCheck(x__11762)->
                                           FMGL(rank), ((x10_int)1)))))
            {
                
                //#line 22 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
                if (true) {
                    
                    //#line 22 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": polyglot.ast.Throw_c
                    x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                }
                
            }
            
            //#line 22 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
            ret9786 = (__extension__ ({
                
                //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                ;
                
                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                x10::lang::String* ret11763;
                
                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                goto __ret11764; __ret11764: {
                {
                    
                    //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                    if (x10aux::nullCheck(x__11762)->FMGL(rail))
                    {
                        
                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                        ret11763 = (x10aux::nullCheck(x__11762)->
                                      FMGL(raw))->__apply(((x10_int)1));
                        
                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                        goto __ret11764_end_;
                    } else {
                        
                        //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                        if (true && !(x10aux::nullCheck(x__11762)->
                                        FMGL(region)->contains(
                                        ((x10_int)1)))) {
                            
                            //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                            x10::array::Array<void>::raiseBoundsError(
                              ((x10_int)1));
                        }
                        
                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                        ret11763 = (x10aux::nullCheck(x__11762)->
                                      FMGL(raw))->__apply(((x10_int) ((((x10_int)1)) - (x10aux::nullCheck(x__11762)->
                                                                                          FMGL(layout_min0)))));
                        
                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                        goto __ret11764_end_;
                    }
                    
                }goto __ret11764_end_; __ret11764_end_: ;
                }
                ret11763;
                }))
                ;
            ret9786;
            }))
            );
        
        //#line 24 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
        if (((x10aux::nullCheck(args)->FMGL(size)) > (((x10_int)2))))
        {
            
            //#line 26 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            this->FMGL(precision) = x10::lang::DoubleNatives::parseDouble((__extension__ ({
                
                //#line 26 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                x10::array::Array<x10::lang::String*>* p__9791 =
                  args;
                
                //#line 26 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": polyglot.ast.Empty_c
                ;
                
                //#line 26 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                x10::lang::String* ret9798;
                
                //#line 26 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                x10::array::Array<x10::lang::String*>* x__11765 =
                  p__9791;
                
                //#line 26 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
                if (!((x10aux::struct_equals(x10aux::nullCheck(x__11765)->
                                               FMGL(rank),
                                             ((x10_int)1)))))
                {
                    
                    //#line 26 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
                    if (true) {
                        
                        //#line 26 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": polyglot.ast.Throw_c
                        x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                    }
                    
                }
                
                //#line 26 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
                ret9798 = (__extension__ ({
                    
                    //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                    ;
                    
                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                    x10::lang::String* ret11766;
                    
                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                    goto __ret11767; __ret11767: {
                    {
                        
                        //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                        if (x10aux::nullCheck(x__11765)->
                              FMGL(rail)) {
                            
                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                            ret11766 = (x10aux::nullCheck(x__11765)->
                                          FMGL(raw))->__apply(((x10_int)2));
                            
                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                            goto __ret11767_end_;
                        } else {
                            
                            //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                            if (true && !(x10aux::nullCheck(x__11765)->
                                            FMGL(region)->contains(
                                            ((x10_int)2))))
                            {
                                
                                //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                x10::array::Array<void>::raiseBoundsError(
                                  ((x10_int)2));
                            }
                            
                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                            ret11766 = (x10aux::nullCheck(x__11765)->
                                          FMGL(raw))->__apply(((x10_int) ((((x10_int)2)) - (x10aux::nullCheck(x__11765)->
                                                                                              FMGL(layout_min0)))));
                            
                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                            goto __ret11767_end_;
                        }
                        
                    }goto __ret11767_end_; __ret11767_end_: ;
                    }
                    ret11766;
                    }))
                    ;
                ret9798;
                }))
                );
            
            //#line 28 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
            if (((x10aux::nullCheck(args)->FMGL(size)) > (((x10_int)3))))
            {
                
                //#line 30 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
                this->FMGL(type_file) = x10::lang::IntNatives::parseInt((__extension__ ({
                    
                    //#line 30 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                    x10::array::Array<x10::lang::String*>* p__9816 =
                      args;
                    
                    //#line 30 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": polyglot.ast.Empty_c
                    ;
                    
                    //#line 30 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                    x10::lang::String* ret9823;
                    
                    //#line 30 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                    x10::array::Array<x10::lang::String*>* x__11768 =
                      p__9816;
                    
                    //#line 30 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__11768)->
                                                   FMGL(rank),
                                                 ((x10_int)1)))))
                    {
                        
                        //#line 30 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
                        if (true) {
                            
                            //#line 30 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": polyglot.ast.Throw_c
                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                        }
                        
                    }
                    
                    //#line 30 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
                    ret9823 = (__extension__ ({
                        
                        //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                        ;
                        
                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                        x10::lang::String* ret11769;
                        
                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                        goto __ret11770; __ret11770: {
                        {
                            
                            //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                            if (x10aux::nullCheck(x__11768)->
                                  FMGL(rail)) {
                                
                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                ret11769 = (x10aux::nullCheck(x__11768)->
                                              FMGL(raw))->__apply(((x10_int)3));
                                
                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                goto __ret11770_end_;
                            } else {
                                
                                //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                if (true && !(x10aux::nullCheck(x__11768)->
                                                FMGL(region)->contains(
                                                ((x10_int)3))))
                                {
                                    
                                    //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                    x10::array::Array<void>::raiseBoundsError(
                                      ((x10_int)3));
                                }
                                
                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                ret11769 = (x10aux::nullCheck(x__11768)->
                                              FMGL(raw))->__apply(((x10_int) ((((x10_int)3)) - (x10aux::nullCheck(x__11768)->
                                                                                                  FMGL(layout_min0)))));
                                
                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                goto __ret11770_end_;
                            }
                            
                        }goto __ret11770_end_; __ret11770_end_: ;
                        }
                        ret11769;
                        }))
                        ;
                    ret9823;
                    }))
                    );
                
                //#line 32 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
                if (((x10aux::nullCheck(args)->FMGL(size)) > (((x10_int)4))))
                {
                    
                    //#line 34 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
                    this->FMGL(passTimes) = x10::lang::IntNatives::parseInt((__extension__ ({
                        
                        //#line 34 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                        x10::array::Array<x10::lang::String*>* p__9828 =
                          args;
                        
                        //#line 34 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": polyglot.ast.Empty_c
                        ;
                        
                        //#line 34 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                        x10::lang::String* ret9835;
                        
                        //#line 34 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                        x10::array::Array<x10::lang::String*>* x__11771 =
                          p__9828;
                        
                        //#line 34 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
                        if (!((x10aux::struct_equals(x10aux::nullCheck(x__11771)->
                                                       FMGL(rank),
                                                     ((x10_int)1)))))
                        {
                            
                            //#line 34 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
                            if (true) {
                                
                                //#line 34 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": polyglot.ast.Throw_c
                                x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                            }
                            
                        }
                        
                        //#line 34 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
                        ret9835 = (__extension__ ({
                            
                            //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                            ;
                            
                            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                            x10::lang::String* ret11772;
                            
                            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                            goto __ret11773; __ret11773: {
                            {
                                
                                //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                if (x10aux::nullCheck(x__11771)->
                                      FMGL(rail)) {
                                    
                                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                    ret11772 = (x10aux::nullCheck(x__11771)->
                                                  FMGL(raw))->__apply(((x10_int)4));
                                    
                                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                    goto __ret11773_end_;
                                } else {
                                    
                                    //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                    if (true && !(x10aux::nullCheck(x__11771)->
                                                    FMGL(region)->contains(
                                                    ((x10_int)4))))
                                    {
                                        
                                        //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                        x10::array::Array<void>::raiseBoundsError(
                                          ((x10_int)4));
                                    }
                                    
                                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                    ret11772 = (x10aux::nullCheck(x__11771)->
                                                  FMGL(raw))->__apply(((x10_int) ((((x10_int)4)) - (x10aux::nullCheck(x__11771)->
                                                                                                      FMGL(layout_min0)))));
                                    
                                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                    goto __ret11773_end_;
                                }
                                
                            }goto __ret11773_end_; __ret11773_end_: ;
                            }
                            ret11772;
                            }))
                            ;
                        ret9835;
                        }))
                        );
                    }
                    
                }
                
            }
            }
            
        }
        
        //#line 41 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10MethodDecl_c
        void ComDetect::main(x10::array::Array<x10::lang::String*>* args) {
            
            //#line 43 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10::util::Timer* timer =  ((new (memset(x10aux::alloc<x10::util::Timer>(), 0, sizeof(x10::util::Timer))) x10::util::Timer()))
            ;
            
            //#line 44 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10_long start = x10::lang::RuntimeNatives::currentTimeMillis();
            
            //#line 46 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            ComDetect* cd =  ((new (memset(x10aux::alloc<ComDetect>(), 0, sizeof(ComDetect))) ComDetect()))
            ;
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            x10aux::nullCheck(cd)->FMGL(filename) = (x10aux::class_cast_unchecked<x10::lang::String*>(X10_NULL));
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            x10aux::nullCheck(cd)->FMGL(it_random) = ((x10_int)0);
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            x10aux::nullCheck(cd)->FMGL(precision) = 1.0E-6;
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            x10aux::nullCheck(cd)->FMGL(type_file) = ((x10_int)0);
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            x10aux::nullCheck(cd)->FMGL(passTimes) = ((x10_int)-1);
            
            //#line 47 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
            x10aux::nullCheck(cd)->checkArgs(args);
            
            //#line 49 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10_long readStart = x10::lang::RuntimeNatives::currentTimeMillis();
            
            //#line 51 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            Community* com =  ((new (memset(x10aux::alloc<Community>(), 0, sizeof(Community))) Community()))
            ;
            
            //#line 51 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10ConstructorCall_c
            (com)->::Community::_constructor(x10aux::nullCheck(cd)->
                                               FMGL(filename),
                                             x10aux::nullCheck(cd)->
                                               FMGL(type_file),
                                             x10aux::nullCheck(cd)->
                                               FMGL(passTimes),
                                             x10aux::nullCheck(cd)->
                                               FMGL(precision));
            
            //#line 52 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10_long readEnd = x10::lang::RuntimeNatives::currentTimeMillis();
            
            //#line 55 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            Graph* g =  ((new (memset(x10aux::alloc<Graph>(), 0, sizeof(Graph))) Graph()))
            ;
            
            //#line 55 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10ConstructorCall_c
            (g)->::Graph::_constructor();
            
            //#line 56 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10_boolean improvement = true;
            
            //#line 57 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10_double mod;
            
            //#line 59 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
            mod = x10aux::nullCheck(com)->modularity();
            
            //#line 61 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10_double new_mod;
            
            //#line 62 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10_int level = ((x10_int)0);
            
            //#line 63 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10_boolean verbose = true;
            
            //#line 65 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10_long resetTotal = ((x10_long) (((x10_int)0)));
            
            //#line 66 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10_long computeTotal = ((x10_long) (((x10_int)0)));
            
            //#line 68 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10Do_c
            do {
                
                //#line 71 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
                x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
                  reinterpret_cast<x10::lang::Any*>(x10aux::makeStringLit("-------------------------------------------------------------------------------------------------------------")));
                
                //#line 72 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
                x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
                  reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10aux::makeStringLit("level: "), level)));
                
                //#line 73 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
                x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
                  reinterpret_cast<x10::lang::Any*>(x10aux::makeStringLit("start computation")));
                
                //#line 74 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
                x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
                  reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10::lang::String::__plus(x10::lang::String::__plus(x10::lang::String::__plus(x10::lang::String::__plus(x10::lang::String::__plus(x10aux::makeStringLit("network size:"), x10aux::nullCheck(x10aux::nullCheck(com)->
                                                                                                                                                                                                                                                                            FMGL(g))->
                                                                                                                                                                                                                                                          FMGL(nb_nodes)), x10aux::makeStringLit(" nodes, ")), x10aux::nullCheck(x10aux::nullCheck(com)->
                                                                                                                                                                                                                                                                                                                                   FMGL(g))->
                                                                                                                                                                                                                                                                                                                 FMGL(nb_links)), x10aux::makeStringLit(" links, ")), x10aux::nullCheck(x10aux::nullCheck(com)->
                                                                                                                                                                                                                                                                                                                                                                                          FMGL(g))->
                                                                                                                                                                                                                                                                                                                                                                        FMGL(total_weight)), x10aux::makeStringLit(" weight."))));
                
                //#line 75 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
                level = ((x10_int) ((level) + (((x10_int)1))));
                
                //#line 78 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                x10_long startCompute = x10::lang::RuntimeNatives::currentTimeMillis();
                
                //#line 79 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
                improvement = x10aux::nullCheck(com)->one_level(
                                level);
                
                //#line 80 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                x10_long endCompute = x10::lang::RuntimeNatives::currentTimeMillis();
                
                //#line 81 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
                x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
                  reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10::lang::String::__plus(x10aux::makeStringLit("It used "), ((x10_long) ((endCompute) - (startCompute)))), x10aux::makeStringLit("ms to Compute"))));
                
                //#line 82 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
                computeTotal = ((x10_long) ((computeTotal) + (((x10_long) ((endCompute) - (startCompute))))));
                
                //#line 84 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
                new_mod = x10aux::nullCheck(com)->modularity();
                
                //#line 86 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                x10_long startReset = x10::lang::RuntimeNatives::currentTimeMillis();
                
                //#line 87 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
                g = x10aux::nullCheck(com)->resetCom();
                
                //#line 88 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
                com = (__extension__ ({
                    
                    //#line 88 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                    Community* alloc3690 =  ((new (memset(x10aux::alloc<Community>(), 0, sizeof(Community))) Community()))
                    ;
                    
                    //#line 88 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10ConstructorCall_c
                    (alloc3690)->::Community::_constructor(
                      g, ((x10_int)-1), x10aux::nullCheck(cd)->
                                          FMGL(precision),
                      x10aux::nullCheck(com)->FMGL(inside_tmp));
                    alloc3690;
                }))
                ;
                
                //#line 89 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
                x10_long endReset = x10::lang::RuntimeNatives::currentTimeMillis();
                
                //#line 90 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
                x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
                  reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10::lang::String::__plus(x10aux::makeStringLit("It used "), ((x10_long) ((endReset) - (startReset)))), x10aux::makeStringLit("ms to Reset"))));
                
                //#line 91 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
                resetTotal = ((x10_long) ((resetTotal) + (((x10_long) ((endReset) - (startReset))))));
                
                //#line 95 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
                x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
                  reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10::lang::String::__plus(x10::lang::String::__plus(x10aux::makeStringLit("mod -> new_mod: "), mod), x10aux::makeStringLit("->")), new_mod)));
                
                //#line 97 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
                mod = new_mod;
                
                //#line 98 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10If_c
                if ((x10aux::struct_equals(level, ((x10_int)1))))
                {
                    
                    //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10LocalAssign_c
                    improvement = true;
                }
                
            } while (improvement);
            
            //#line 105 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            x10_long end = x10::lang::RuntimeNatives::currentTimeMillis();
            
            //#line 106 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
            x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
              reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10::lang::String::__plus(x10aux::makeStringLit("Read: "), ((x10_long) ((readEnd) - (readStart)))), x10aux::makeStringLit("ms"))));
            
            //#line 107 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
            x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
              reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10::lang::String::__plus(x10aux::makeStringLit("Compute: "), computeTotal), x10aux::makeStringLit("ms"))));
            
            //#line 108 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
            x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
              reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10::lang::String::__plus(x10aux::makeStringLit("Reset: "), resetTotal), x10aux::makeStringLit("ms"))));
            
            //#line 109 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
            x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
              reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10::lang::String::__plus(x10aux::makeStringLit("All: "), ((x10_long) ((end) - (start)))), x10aux::makeStringLit("ms"))));
            
            //#line 110 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10Call_c
            x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
              x10aux::class_cast_unchecked<x10::lang::Any*>(new_mod));
        }
        
        //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10MethodDecl_c
        ComDetect* ComDetect::ComDetect____this__ComDetect(
          ) {
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10Return_c
            return this;
            
        }
        
        //#line 4 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10ConstructorDecl_c
        void ComDetect::_constructor() {
            
            //#line 4 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.AssignPropertyCall_c
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10LocalDecl_c
            ComDetect* this11774 = this;
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            x10aux::nullCheck(this11774)->FMGL(filename) =
              (x10aux::class_cast_unchecked<x10::lang::String*>(X10_NULL));
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            x10aux::nullCheck(this11774)->FMGL(it_random) =
              ((x10_int)0);
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            x10aux::nullCheck(this11774)->FMGL(precision) =
              1.0E-6;
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            x10aux::nullCheck(this11774)->FMGL(type_file) =
              ((x10_int)0);
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            x10aux::nullCheck(this11774)->FMGL(passTimes) =
              ((x10_int)-1);
        }
        ComDetect* ComDetect::_make() {
            ComDetect* this_ = new (memset(x10aux::alloc<ComDetect>(), 0, sizeof(ComDetect))) ComDetect();
            this_->_constructor();
            return this_;
        }
        
        
        
        //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": x10.ast.X10MethodDecl_c
        void ComDetect::__fieldInitializers3118() {
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            this->FMGL(filename) = (x10aux::class_cast_unchecked<x10::lang::String*>(X10_NULL));
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            this->FMGL(it_random) = ((x10_int)0);
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            this->FMGL(precision) = 1.0E-6;
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            this->FMGL(type_file) = ((x10_int)0);
            
            //#line 3 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/ComDetect.x10": Eval of x10.ast.X10FieldAssign_c
            this->FMGL(passTimes) = ((x10_int)-1);
        }
        const x10aux::serialization_id_t ComDetect::_serialization_id = 
            x10aux::DeserializationDispatcher::addDeserializer(ComDetect::_deserializer, x10aux::CLOSURE_KIND_NOT_ASYNC);
        
        void ComDetect::_serialize_body(x10aux::serialization_buffer& buf) {
            buf.write(this->FMGL(filename));
            buf.write(this->FMGL(it_random));
            buf.write(this->FMGL(precision));
            buf.write(this->FMGL(type_file));
            buf.write(this->FMGL(passTimes));
            
        }
        
        x10::lang::Reference* ComDetect::_deserializer(x10aux::deserialization_buffer& buf) {
            ComDetect* this_ = new (memset(x10aux::alloc<ComDetect>(), 0, sizeof(ComDetect))) ComDetect();
            buf.record_reference(this_);
            this_->_deserialize_body(buf);
            return this_;
        }
        
        void ComDetect::_deserialize_body(x10aux::deserialization_buffer& buf) {
            FMGL(filename) = buf.read<x10::lang::String*>();
            FMGL(it_random) = buf.read<x10_int>();
            FMGL(precision) = buf.read<x10_double>();
            FMGL(type_file) = buf.read<x10_int>();
            FMGL(passTimes) = buf.read<x10_int>();
        }
        
        
    x10aux::RuntimeType ComDetect::rtt;
    void ComDetect::_initRTT() {
        if (rtt.initStageOne(&rtt)) return;
        const x10aux::RuntimeType** parents = NULL; 
        rtt.initStageTwo("ComDetect",x10aux::RuntimeType::class_kind, 0, parents, 0, NULL, NULL);
    }
    
    /* END of ComDetect */
/*************************************************/
