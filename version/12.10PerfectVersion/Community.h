#ifndef __COMMUNITY_H
#define __COMMUNITY_H

#include <x10rt.h>


#define X10_LANG_INT_H_NODEPS
#include <x10/lang/Int.h>
#undef X10_LANG_INT_H_NODEPS
#define X10_LANG_INT_H_NODEPS
#include <x10/lang/Int.h>
#undef X10_LANG_INT_H_NODEPS
#define X10_LANG_DOUBLE_H_NODEPS
#include <x10/lang/Double.h>
#undef X10_LANG_DOUBLE_H_NODEPS
#define X10_LANG_DOUBLE_H_NODEPS
#include <x10/lang/Double.h>
#undef X10_LANG_DOUBLE_H_NODEPS
#define X10_LANG_BOOLEAN_H_NODEPS
#include <x10/lang/Boolean.h>
#undef X10_LANG_BOOLEAN_H_NODEPS
#define X10_LANG_BOOLEAN_H_NODEPS
#include <x10/lang/Boolean.h>
#undef X10_LANG_BOOLEAN_H_NODEPS
class Graph;
namespace x10 { namespace array { 
template<class TPMGL(T)> class Array;
} } 
namespace x10 { namespace util { 
class Random;
} } 
namespace x10 { namespace lang { 
class String;
} } 
namespace x10 { namespace io { 
class Printer;
} } 
namespace x10 { namespace io { 
class Console;
} } 
namespace x10 { namespace lang { 
class Any;
} } 
namespace x10 { namespace array { 
class RectRegion1D;
} } 
namespace x10 { namespace array { 
class Region;
} } 
namespace x10 { namespace util { 
template<class TPMGL(T)> class IndexedMemoryChunk;
} } 
namespace x10 { namespace compiler { 
class CompilerFlags;
} } 
namespace x10 { namespace lang { 
class FailedDynamicCheckException;
} } 
namespace x10 { namespace util { 
template<class TPMGL(T)> class ArrayList;
} } 
namespace x10 { namespace compiler { 
class Inline;
} } 
namespace x10 { namespace util { 
class Timer;
} } 
namespace x10 { namespace lang { 
class Long;
} } 
namespace x10 { namespace util { 
template<class TPMGL(T)> class Set;
} } 
namespace x10 { namespace lang { 
template<class TPMGL(T)> class Iterator;
} } 
namespace x10 { namespace util { 
template<class TPMGL(K), class TPMGL(V)> class HashMap;
} } 
namespace x10 { namespace util { 
template<class TPMGL(T)> class Box;
} } 
namespace x10 { namespace lang { 
template<class TPMGL(T)> class Iterable;
} } 
class Community : public x10::lang::X10Class   {
    public:
    RTT_H_DECLS_CLASS
    
    Graph* FMGL(g);
    
    x10_int FMGL(size);
    
    x10::array::Array<x10_double>* FMGL(neigh_weight);
    
    x10::array::Array<x10_int>* FMGL(neigh_pos);
    
    x10_int FMGL(neigh_last);
    
    x10::array::Array<x10_int>* FMGL(n2c);
    
    x10::array::Array<x10_double>* FMGL(inside);
    
    x10::array::Array<x10_double>* FMGL(inside_tmp);
    
    x10::array::Array<x10_double>* FMGL(tot);
    
    x10_int FMGL(nb_pass);
    
    x10_double FMGL(min_modularity);
    
    x10_double FMGL(totc);
    
    x10_double FMGL(degc);
    
    x10_double FMGL(m2);
    
    x10_double FMGL(dnc);
    
    x10_double FMGL(q);
    
    x10_int FMGL(i);
    
    x10_int FMGL(startIndex);
    
    x10_int FMGL(deg);
    
    x10_int FMGL(neigh);
    
    x10_int FMGL(neigh_comm);
    
    x10_double FMGL(neigh_w);
    
    x10_boolean FMGL(improvement);
    
    x10_int FMGL(nb_moves);
    
    x10_int FMGL(nb_pass_done);
    
    x10_double FMGL(new_mod);
    
    x10_double FMGL(cur_mod);
    
    x10::util::Random* FMGL(r);
    
    x10_int FMGL(rand_pos);
    
    x10_int FMGL(tmp);
    
    x10_int FMGL(node);
    
    x10_int FMGL(node_comm);
    
    x10_double FMGL(w_degree);
    
    x10_int FMGL(best_comm);
    
    x10_double FMGL(best_nblinks);
    
    x10_double FMGL(best_increase);
    
    x10_double FMGL(increase);
    
    x10::array::Array<x10_int>* FMGL(renumber);
    
    x10_int FMGL(finalCount);
    
    void _constructor();
    
    static Community* _make();
    
    void _constructor(x10::lang::String* filename, x10_int type_file, x10_int passTimes,
                      x10_double precision);
    
    static Community* _make(x10::lang::String* filename, x10_int type_file,
                            x10_int passTimes, x10_double precision);
    
    void _constructor(Graph* gc, x10_int passTimes, x10_double precision,
                      x10::array::Array<x10_double>* inside_tmp);
    
    static Community* _make(Graph* gc, x10_int passTimes, x10_double precision,
                            x10::array::Array<x10_double>* inside_tmp);
    
    void _constructor(x10::lang::String* filename, x10_int type_file,
                      x10_int maxNode, x10_int edgeNum, x10_int passTimes,
                      x10_double precision);
    
    static Community* _make(x10::lang::String* filename, x10_int type_file,
                            x10_int maxNode, x10_int edgeNum, x10_int passTimes,
                            x10_double precision);
    
    virtual x10_double modularity();
    virtual void neigh_comm(x10_int node);
    virtual void remove(x10_int node, x10_int comm, x10_double dnodecomm);
    virtual void insert(x10_int node, x10_int comm, x10_double dnodecomm);
    virtual x10_double modularity_gain(x10_int node, x10_int comm,
                                       x10_double dnodecomm, x10_double w_degree);
    virtual x10_boolean one_level(x10_int level);
    virtual Graph* resetCom();
    virtual x10::array::Array<x10_int>* Shell(x10::array::Array<x10_int>* d);
    static void main(x10::array::Array<x10::lang::String*>* id__79);
    virtual Community* Community____this__Community();
    virtual void __fieldInitializers9841();
    
    // Serialization
    public: static const x10aux::serialization_id_t _serialization_id;
    
    public: virtual x10aux::serialization_id_t _get_serialization_id() {
         return _serialization_id;
    }
    
    public: virtual void _serialize_body(x10aux::serialization_buffer& buf);
    
    public: static x10::lang::Reference* _deserializer(x10aux::deserialization_buffer& buf);
    
    public: void _deserialize_body(x10aux::deserialization_buffer& buf);
    
};

#endif // COMMUNITY_H

class Community;

#ifndef COMMUNITY_H_NODEPS
#define COMMUNITY_H_NODEPS
#ifndef COMMUNITY_H_GENERICS
#define COMMUNITY_H_GENERICS
#endif // COMMUNITY_H_GENERICS
#endif // __COMMUNITY_H_NODEPS
