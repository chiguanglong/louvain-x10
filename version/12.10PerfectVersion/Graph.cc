/*************************************************/
/* START of Graph */
#include <Graph.h>

#include <x10/lang/Double.h>
#include <x10/lang/Int.h>
#include <x10/util/Pair.h>
#include <x10/array/Array.h>
#include <x10/util/ArrayList.h>
#include <x10/lang/String.h>
#include <x10/io/File.h>
#include <x10/io/FileReader.h>
#include <x10/io/Reader.h>
#include <x10/io/Printer.h>
#include <x10/io/Console.h>
#include <x10/lang/Any.h>
#include <x10/array/RectRegion1D.h>
#include <x10/array/Region.h>
#include <x10/util/IndexedMemoryChunk.h>
#include <x10/lang/Boolean.h>
#include <x10/compiler/CompilerFlags.h>
#include <x10/lang/FailedDynamicCheckException.h>
#include <x10/lang/Math.h>
#include <x10/lang/Iterator.h>
#include <x10/io/ReaderIterator.h>
#include <x10/compiler/Inline.h>
#include <x10/compiler/NonEscaping.h>
#include <x10/io/FileWriter.h>
#include <x10/io/Writer.h>
#include <x10/io/OutputStreamWriter.h>
#include <x10/util/Timer.h>
#include <x10/lang/Long.h>

//#line 13 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10FieldDecl_c

//#line 14 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10FieldDecl_c

//#line 15 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10FieldDecl_c

//#line 17 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10FieldDecl_c

//#line 18 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10FieldDecl_c

//#line 19 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10FieldDecl_c

//#line 20 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10FieldDecl_c

//#line 22 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10FieldDecl_c

//#line 23 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10FieldDecl_c

//#line 25 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10FieldDecl_c

//#line 27 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorDecl_c
void Graph::_constructor() {
    
    //#line 27 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.AssignPropertyCall_c
    
    //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
    this->Graph::__fieldInitializers11008();
    
    //#line 29 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
    this->FMGL(links) = (__extension__ ({
        
        //#line 29 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
        x10::util::ArrayList<x10_int>* alloc11643 =  ((new (memset(x10aux::alloc<x10::util::ArrayList<x10_int> >(), 0, sizeof(x10::util::ArrayList<x10_int>))) x10::util::ArrayList<x10_int>()))
        ;
        
        //#line 29 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
        (alloc11643)->::x10::util::ArrayList<x10_int>::_constructor();
        alloc11643;
    }))
    ;
    
    //#line 30 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
    this->FMGL(weights) = (__extension__ ({
        
        //#line 30 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
        x10::util::ArrayList<x10_double>* alloc11644 =  ((new (memset(x10aux::alloc<x10::util::ArrayList<x10_double> >(), 0, sizeof(x10::util::ArrayList<x10_double>))) x10::util::ArrayList<x10_double>()))
        ;
        
        //#line 30 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
        (alloc11644)->::x10::util::ArrayList<x10_double>::_constructor();
        alloc11644;
    }))
    ;
    
    //#line 31 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
    x10aux::nullCheck(this->FMGL(links))->clear();
    
    //#line 32 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
    x10aux::nullCheck(this->FMGL(weights))->clear();
}
Graph* Graph::_make() {
    Graph* this_ = new (memset(x10aux::alloc<Graph>(), 0, sizeof(Graph))) Graph();
    this_->_constructor();
    return this_;
}



//#line 35 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorDecl_c
void Graph::_constructor(x10::lang::String* filename, x10::lang::String* filename_w,
                         x10_int type_file) {
    
    //#line 35 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.AssignPropertyCall_c
    
    //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
    this->Graph::__fieldInitializers11008();
    
    //#line 37 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
    x10::io::File* finput =  ((new (memset(x10aux::alloc<x10::io::File>(), 0, sizeof(x10::io::File))) x10::io::File()))
    ;
    
    //#line 37 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
    (finput)->::x10::io::File::_constructor(filename);
    
    //#line 38 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
    x10::io::FileReader* freader = finput->openRead();
    
    //#line 40 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
    this->FMGL(nb_nodes) = x10aux::nullCheck(freader)->readInt();
    
    //#line 41 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
    x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
      reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10aux::makeStringLit("nb_nodes = "), this->
                                                                                                          FMGL(nb_nodes))));
    
    //#line 43 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
    this->FMGL(degrees) = (__extension__ ({
        
        //#line 43 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
        x10::array::Array<x10_int>* alloc11645 =  ((new (memset(x10aux::alloc<x10::array::Array<x10_int> >(), 0, sizeof(x10::array::Array<x10_int>))) x10::array::Array<x10_int>()))
        ;
        
        //#line 268 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
        x10_int size22336 = this->FMGL(nb_nodes);
        
        //#line 270 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
        x10::array::RectRegion1D* myReg22335 =  ((new (memset(x10aux::alloc<x10::array::RectRegion1D>(), 0, sizeof(x10::array::RectRegion1D))) x10::array::RectRegion1D()))
        ;
        
        //#line 270 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10ConstructorCall_c
        (myReg22335)->::x10::array::RectRegion1D::_constructor(
          ((x10_int) ((size22336) - (((x10_int)1)))));
        
        //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
        alloc11645->FMGL(region) = reinterpret_cast<x10::array::Region*>(myReg22335);
        
        //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
        alloc11645->FMGL(rank) = ((x10_int)1);
        
        //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
        alloc11645->FMGL(rect) = true;
        
        //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
        alloc11645->FMGL(zeroBased) = true;
        
        //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
        alloc11645->FMGL(rail) = true;
        
        //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
        alloc11645->FMGL(size) = size22336;
        
        //#line 273 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
        alloc11645->FMGL(layout_min0) = alloc11645->FMGL(layout_stride1) =
          alloc11645->FMGL(layout_min1) = ((x10_int)0);
        
        //#line 274 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
        alloc11645->FMGL(layout) = (x10aux::class_cast_unchecked<x10::array::Array<x10_int>*>(X10_NULL));
        
        //#line 275 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
        alloc11645->FMGL(raw) = x10::util::IndexedMemoryChunk<void>::allocate<x10_int >(size22336, 8, false, true);
        alloc11645;
    }))
    ;
    
    //#line 44 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
    this->FMGL(deg) = this->FMGL(nb_nodes);
    
    //#line 45 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
    {
        for (
             //#line 45 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
             this->FMGL(startIndex) = ((x10_int)0); ((this->
                                                        FMGL(startIndex)) < (this->
                                                                               FMGL(deg)));
             
             //#line 45 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.StmtExpr_c
             (__extension__ ({
                 
                 //#line 45 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                 Graph* x19742 = this;
                 
                 //#line 45 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                 ;
                 x10aux::nullCheck(x19742)->FMGL(startIndex) =
                   ((x10_int) ((x10aux::nullCheck(x19742)->
                                  FMGL(startIndex)) + (((x10_int)1))));
             }))
             ) {
            
            //#line 47 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
            x10::array::Array<x10_int>* p__22343 = this->
                                                     FMGL(degrees);
            
            //#line 47 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
            x10_int p__22344 = this->FMGL(startIndex);
            
            //#line 47 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
            x10_int p__22345 = x10aux::nullCheck(freader)->readInt();
            
            //#line 47 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
            x10_int ret22346;
            
            //#line 47 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
            x10::array::Array<x10_int>* x__22337 = p__22343;
            
            //#line 47 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
            x10_int x__i22338 = p__22344;
            
            //#line 47 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
            x10_int x__v22339 = p__22345;
            
            //#line 47 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
            if (!((x10aux::struct_equals(x10aux::nullCheck(x__22337)->
                                           FMGL(rank), ((x10_int)1)))))
            {
                
                //#line 47 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                if (true) {
                    
                    //#line 47 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                    x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                }
                
            }
            
            //#line 47 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
            ret22346 = (__extension__ ({
                
                //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                x10_int i22340 = x__i22338;
                
                //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                x10_int v22341 = x__v22339;
                
                //#line 553 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                x10_int ret22342;
                
                //#line 555 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                if (x10aux::nullCheck(x__22337)->FMGL(rail))
                {
                    
                    //#line 557 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                    (x10aux::nullCheck(x__22337)->FMGL(raw))->__set(i22340, v22341);
                } else {
                    
                    //#line 559 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                    if (true && !(x10aux::nullCheck(x__22337)->
                                    FMGL(region)->contains(
                                    i22340))) {
                        
                        //#line 560 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                        x10::array::Array<void>::raiseBoundsError(
                          i22340);
                    }
                    
                    //#line 562 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                    (x10aux::nullCheck(x__22337)->FMGL(raw))->__set(((x10_int) ((i22340) - (x10aux::nullCheck(x__22337)->
                                                                                              FMGL(layout_min0)))), v22341);
                }
                
                //#line 564 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                ret22342 = v22341;
                ret22342;
            }))
            ;
            
            //#line 47 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Local_c
            ret22346;
        }
    }
    
    //#line 50 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
    this->FMGL(nb_links) = (__extension__ ({
        
        //#line 50 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
        x10::array::Array<x10_int>* p__19763 = this->FMGL(degrees);
        
        //#line 50 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
        x10_int p__19764 = ((x10_int) ((this->FMGL(nb_nodes)) - (((x10_int)1))));
        
        //#line 50 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
        x10_int ret19770;
        
        //#line 50 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
        x10::array::Array<x10_int>* x__22347 = p__19763;
        
        //#line 50 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
        x10_int x__i22348 = p__19764;
        
        //#line 50 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
        if (!((x10aux::struct_equals(x10aux::nullCheck(x__22347)->
                                       FMGL(rank), ((x10_int)1)))))
        {
            
            //#line 50 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
            if (true) {
                
                //#line 50 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
            }
            
        }
        
        //#line 50 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
        ret19770 = (__extension__ ({
            
            //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
            x10_int i22349 = x__i22348;
            
            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
            x10_int ret22350;
            
            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
            goto __ret22351; __ret22351: {
            {
                
                //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                if (x10aux::nullCheck(x__22347)->FMGL(rail))
                {
                    
                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                    ret22350 = (x10aux::nullCheck(x__22347)->
                                  FMGL(raw))->__apply(i22349);
                    
                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                    goto __ret22351_end_;
                } else {
                    
                    //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                    if (true && !(x10aux::nullCheck(x__22347)->
                                    FMGL(region)->contains(
                                    i22349))) {
                        
                        //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                        x10::array::Array<void>::raiseBoundsError(
                          i22349);
                    }
                    
                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                    ret22350 = (x10aux::nullCheck(x__22347)->
                                  FMGL(raw))->__apply(((x10_int) ((i22349) - (x10aux::nullCheck(x__22347)->
                                                                                FMGL(layout_min0)))));
                    
                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                    goto __ret22351_end_;
                }
                
            }goto __ret22351_end_; __ret22351_end_: ;
            }
            ret22350;
            }))
            ;
        ret19770;
        }))
        ;
    
    //#line 51 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
    this->FMGL(links) = (__extension__ ({
        
        //#line 51 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
        x10::util::ArrayList<x10_int>* alloc11646 =  ((new (memset(x10aux::alloc<x10::util::ArrayList<x10_int> >(), 0, sizeof(x10::util::ArrayList<x10_int>))) x10::util::ArrayList<x10_int>()))
        ;
        
        //#line 51 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
        (alloc11646)->::x10::util::ArrayList<x10_int>::_constructor(
          this->FMGL(nb_links));
        alloc11646;
    }))
    ;
    
    //#line 52 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
    this->FMGL(deg) = this->FMGL(nb_links);
    
    //#line 53 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
    {
        for (
             //#line 53 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
             this->FMGL(startIndex) = ((x10_int)0); ((this->
                                                        FMGL(startIndex)) < (this->
                                                                               FMGL(deg)));
             
             //#line 53 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.StmtExpr_c
             (__extension__ ({
                 
                 //#line 53 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                 Graph* x19772 = this;
                 
                 //#line 53 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                 ;
                 x10aux::nullCheck(x19772)->FMGL(startIndex) =
                   ((x10_int) ((x10aux::nullCheck(x19772)->
                                  FMGL(startIndex)) + (((x10_int)1))));
             }))
             ) {
            
            //#line 54 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
            x10aux::nullCheck(this->FMGL(links))->__set(this->
                                                          FMGL(startIndex),
                                                        x10aux::nullCheck(freader)->readInt());
        }
    }
    
    //#line 58 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
    this->FMGL(weights) = (__extension__ ({
        
        //#line 58 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
        x10::util::ArrayList<x10_double>* alloc11647 =  ((new (memset(x10aux::alloc<x10::util::ArrayList<x10_double> >(), 0, sizeof(x10::util::ArrayList<x10_double>))) x10::util::ArrayList<x10_double>()))
        ;
        
        //#line 58 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
        (alloc11647)->::x10::util::ArrayList<x10_double>::_constructor();
        alloc11647;
    }))
    ;
    
    //#line 59 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
    if ((x10aux::struct_equals(type_file, ((x10_int)1))))
    {
        
        //#line 61 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
        x10::io::File* finput_w =  ((new (memset(x10aux::alloc<x10::io::File>(), 0, sizeof(x10::io::File))) x10::io::File()))
        ;
        
        //#line 61 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
        (finput_w)->::x10::io::File::_constructor(filename_w);
        
        //#line 62 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
        x10::io::FileReader* freader_w = finput_w->openRead();
        
        //#line 63 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
        this->FMGL(weights) = (__extension__ ({
            
            //#line 63 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
            x10::util::ArrayList<x10_double>* alloc11648 =
               ((new (memset(x10aux::alloc<x10::util::ArrayList<x10_double> >(), 0, sizeof(x10::util::ArrayList<x10_double>))) x10::util::ArrayList<x10_double>()))
            ;
            
            //#line 63 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
            (alloc11648)->::x10::util::ArrayList<x10_double>::_constructor(
              this->FMGL(nb_links));
            alloc11648;
        }))
        ;
        
        //#line 64 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
        this->FMGL(deg) = this->FMGL(nb_links);
        
        //#line 65 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
        {
            for (
                 //#line 65 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                 this->FMGL(startIndex) = ((x10_int)0); ((this->
                                                            FMGL(startIndex)) < (this->
                                                                                   FMGL(deg)));
                 
                 //#line 65 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.StmtExpr_c
                 (__extension__ ({
                     
                     //#line 65 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                     Graph* x19774 = this;
                     
                     //#line 65 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                     ;
                     x10aux::nullCheck(x19774)->FMGL(startIndex) =
                       ((x10_int) ((x10aux::nullCheck(x19774)->
                                      FMGL(startIndex)) + (((x10_int)1))));
                 }))
                 ) {
                
                //#line 67 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                x10aux::nullCheck(this->FMGL(weights))->__set(
                  this->FMGL(startIndex), x10aux::nullCheck(freader_w)->readDouble());
            }
        }
        
    }
    
    //#line 78 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
    this->FMGL(total_weight) = 0.0;
    
    //#line 79 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
    {
        for (
             //#line 79 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
             this->FMGL(startIndex) = ((x10_int)0); ((this->
                                                        FMGL(startIndex)) < (this->
                                                                               FMGL(nb_nodes)));
             
             //#line 79 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.StmtExpr_c
             (__extension__ ({
                 
                 //#line 79 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                 Graph* x19776 = this;
                 
                 //#line 79 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                 ;
                 x10aux::nullCheck(x19776)->FMGL(startIndex) =
                   ((x10_int) ((x10aux::nullCheck(x19776)->
                                  FMGL(startIndex)) + (((x10_int)1))));
             }))
             ) {
            
            //#line 81 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
            Graph* x22394 = this;
            
            //#line 81 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
            x10_double y22395 = (__extension__ ({
                
                //#line 81 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                Graph* this22396 = this;
                
                //#line 185 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                x10_int node22397 = this->FMGL(startIndex);
                
                //#line 185 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                x10_double ret22398;
                
                //#line 190 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                x10_double res22375 = 0.0;
                
                //#line 192 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                if ((x10aux::struct_equals(x10aux::nullCheck(x10aux::nullCheck(this22396)->
                                                               FMGL(weights))->size(),
                                           ((x10_int)0))))
                {
                    
                    //#line 194 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                    res22375 = ((x10_double) ((__extension__ ({
                        
                        //#line 213 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                        x10_int node22376 = node22397;
                        
                        //#line 213 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                        x10_int ret22377;
                        
                        //#line 213 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Labeled_c
                        goto __ret22378; __ret22378: {
                        {
                            
                            //#line 217 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                            if ((x10aux::struct_equals(node22376,
                                                       ((x10_int)0))))
                            {
                                
                                //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                ret22377 = (__extension__ ({
                                    
                                    //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::array::Array<x10_int>* p__22379 =
                                      x10aux::nullCheck(this22396)->
                                        FMGL(degrees);
                                    
                                    //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                    ;
                                    
                                    //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10_int ret22380;
                                    
                                    //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::array::Array<x10_int>* x__22352 =
                                      p__22379;
                                    
                                    //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__22352)->
                                                                   FMGL(rank),
                                                                 ((x10_int)1)))))
                                    {
                                        
                                        //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                        if (true) {
                                            
                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                        }
                                        
                                    }
                                    
                                    //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                    ret22380 = (__extension__ ({
                                        
                                        //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                                        ;
                                        
                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                        x10_int ret22353;
                                        
                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                        goto __ret22354; __ret22354: {
                                        {
                                            
                                            //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                            if (x10aux::nullCheck(x__22352)->
                                                  FMGL(rail))
                                            {
                                                
                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                ret22353 =
                                                  (x10aux::nullCheck(x__22352)->
                                                     FMGL(raw))->__apply(((x10_int)0));
                                                
                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                goto __ret22354_end_;
                                            } else {
                                                
                                                //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                if (true &&
                                                    !(x10aux::nullCheck(x__22352)->
                                                        FMGL(region)->contains(
                                                        ((x10_int)0))))
                                                {
                                                    
                                                    //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                    x10::array::Array<void>::raiseBoundsError(
                                                      ((x10_int)0));
                                                }
                                                
                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                ret22353 =
                                                  (x10aux::nullCheck(x__22352)->
                                                     FMGL(raw))->__apply(((x10_int) ((((x10_int)0)) - (x10aux::nullCheck(x__22352)->
                                                                                                         FMGL(layout_min0)))));
                                                
                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                goto __ret22354_end_;
                                            }
                                            
                                        }goto __ret22354_end_; __ret22354_end_: ;
                                        }
                                        ret22353;
                                        }))
                                        ;
                                    ret22380;
                                    }))
                                    ;
                                
                                //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Branch_c
                                goto __ret22378_end_;
                                } else {
                                    
                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                    ret22377 = ((x10_int) (((__extension__ ({
                                        
                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                        x10::array::Array<x10_int>* p__22381 =
                                          x10aux::nullCheck(this22396)->
                                            FMGL(degrees);
                                        
                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                        x10_int p__22382 =
                                          node22376;
                                        
                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                        x10_int ret22383;
                                        
                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                        x10::array::Array<x10_int>* x__22355 =
                                          p__22381;
                                        
                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                        x10_int x__i22356 =
                                          p__22382;
                                        
                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                        if (!((x10aux::struct_equals(x10aux::nullCheck(x__22355)->
                                                                       FMGL(rank),
                                                                     ((x10_int)1)))))
                                        {
                                            
                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                            if (true) {
                                                
                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                            }
                                            
                                        }
                                        
                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                        ret22383 = (__extension__ ({
                                            
                                            //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                            x10_int i22357 =
                                              x__i22356;
                                            
                                            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                            x10_int ret22358;
                                            
                                            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                            goto __ret22359; __ret22359: {
                                            {
                                                
                                                //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                if (x10aux::nullCheck(x__22355)->
                                                      FMGL(rail))
                                                {
                                                    
                                                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                    ret22358 =
                                                      (x10aux::nullCheck(x__22355)->
                                                         FMGL(raw))->__apply(i22357);
                                                    
                                                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                    goto __ret22359_end_;
                                                } else {
                                                    
                                                    //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                    if (true &&
                                                        !(x10aux::nullCheck(x__22355)->
                                                            FMGL(region)->contains(
                                                            i22357)))
                                                    {
                                                        
                                                        //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                        x10::array::Array<void>::raiseBoundsError(
                                                          i22357);
                                                    }
                                                    
                                                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                    ret22358 =
                                                      (x10aux::nullCheck(x__22355)->
                                                         FMGL(raw))->__apply(((x10_int) ((i22357) - (x10aux::nullCheck(x__22355)->
                                                                                                       FMGL(layout_min0)))));
                                                    
                                                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                    goto __ret22359_end_;
                                                }
                                                
                                            }goto __ret22359_end_; __ret22359_end_: ;
                                            }
                                            ret22358;
                                            }))
                                            ;
                                        ret22383;
                                        }))
                                        ) - ((__extension__ ({
                                            
                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                            x10::array::Array<x10_int>* p__22384 =
                                              x10aux::nullCheck(this22396)->
                                                FMGL(degrees);
                                            
                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                            x10_int p__22385 =
                                              ((x10_int) ((node22376) - (((x10_int)1))));
                                            
                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                            x10_int ret22386;
                                            
                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                            x10::array::Array<x10_int>* x__22360 =
                                              p__22384;
                                            
                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                            x10_int x__i22361 =
                                              p__22385;
                                            
                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                            if (!((x10aux::struct_equals(x10aux::nullCheck(x__22360)->
                                                                           FMGL(rank),
                                                                         ((x10_int)1)))))
                                            {
                                                
                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                if (true)
                                                {
                                                    
                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                    x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                }
                                                
                                            }
                                            
                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                            ret22386 = (__extension__ ({
                                                
                                                //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                x10_int i22362 =
                                                  x__i22361;
                                                
                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                x10_int ret22363;
                                                
                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                goto __ret22364; __ret22364: {
                                                {
                                                    
                                                    //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                    if (x10aux::nullCheck(x__22360)->
                                                          FMGL(rail))
                                                    {
                                                        
                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                        ret22363 =
                                                          (x10aux::nullCheck(x__22360)->
                                                             FMGL(raw))->__apply(i22362);
                                                        
                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                        goto __ret22364_end_;
                                                    } else
                                                    {
                                                        
                                                        //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                        if (true &&
                                                            !(x10aux::nullCheck(x__22360)->
                                                                FMGL(region)->contains(
                                                                i22362)))
                                                        {
                                                            
                                                            //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                            x10::array::Array<void>::raiseBoundsError(
                                                              i22362);
                                                        }
                                                        
                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                        ret22363 =
                                                          (x10aux::nullCheck(x__22360)->
                                                             FMGL(raw))->__apply(((x10_int) ((i22362) - (x10aux::nullCheck(x__22360)->
                                                                                                           FMGL(layout_min0)))));
                                                        
                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                        goto __ret22364_end_;
                                                    }
                                                    
                                                }goto __ret22364_end_; __ret22364_end_: ;
                                                }
                                                ret22363;
                                                }))
                                                ;
                                            ret22386;
                                            }))
                                            )));
                                        
                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Branch_c
                                        goto __ret22378_end_;
                                    }
                                    
                                }goto __ret22378_end_; __ret22378_end_: ;
                                }
                                
                            ret22377;
                            }))
                            ));
                        } else {
                            
                            //#line 199 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                            if ((x10aux::struct_equals(node22397,
                                                       ((x10_int)0))))
                            {
                                
                                //#line 200 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                x10aux::nullCheck(this22396)->
                                  FMGL(startIndex) = ((x10_int)0);
                            } else {
                                
                                //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                x10aux::nullCheck(this22396)->
                                  FMGL(startIndex) = (__extension__ ({
                                    
                                    //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::array::Array<x10_int>* p__22387 =
                                      x10aux::nullCheck(this22396)->
                                        FMGL(degrees);
                                    
                                    //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10_int p__22388 = ((x10_int) ((node22397) - (((x10_int)1))));
                                    
                                    //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10_int ret22389;
                                    
                                    //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::array::Array<x10_int>* x__22365 =
                                      p__22387;
                                    
                                    //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10_int x__i22366 = p__22388;
                                    
                                    //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__22365)->
                                                                   FMGL(rank),
                                                                 ((x10_int)1)))))
                                    {
                                        
                                        //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                        if (true) {
                                            
                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                        }
                                        
                                    }
                                    
                                    //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                    ret22389 = (__extension__ ({
                                        
                                        //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                        x10_int i22367 = x__i22366;
                                        
                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                        x10_int ret22368;
                                        
                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                        goto __ret22369; __ret22369: {
                                        {
                                            
                                            //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                            if (x10aux::nullCheck(x__22365)->
                                                  FMGL(rail))
                                            {
                                                
                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                ret22368 =
                                                  (x10aux::nullCheck(x__22365)->
                                                     FMGL(raw))->__apply(i22367);
                                                
                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                goto __ret22369_end_;
                                            } else {
                                                
                                                //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                if (true &&
                                                    !(x10aux::nullCheck(x__22365)->
                                                        FMGL(region)->contains(
                                                        i22367)))
                                                {
                                                    
                                                    //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                    x10::array::Array<void>::raiseBoundsError(
                                                      i22367);
                                                }
                                                
                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                ret22368 =
                                                  (x10aux::nullCheck(x__22365)->
                                                     FMGL(raw))->__apply(((x10_int) ((i22367) - (x10aux::nullCheck(x__22365)->
                                                                                                   FMGL(layout_min0)))));
                                                
                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                goto __ret22369_end_;
                                            }
                                            
                                        }goto __ret22369_end_; __ret22369_end_: ;
                                        }
                                        ret22368;
                                        }))
                                        ;
                                    ret22389;
                                    }))
                                    ;
                                }
                                
                            
                            //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                            x10aux::nullCheck(this22396)->
                              FMGL(deg) = (__extension__ ({
                                
                                //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::array::Array<x10_int>* p__22390 =
                                  x10aux::nullCheck(this22396)->
                                    FMGL(degrees);
                                
                                //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10_int p__22391 = node22397;
                                
                                //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10_int ret22392;
                                
                                //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::array::Array<x10_int>* x__22370 =
                                  p__22390;
                                
                                //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10_int x__i22371 = p__22391;
                                
                                //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                if (!((x10aux::struct_equals(x10aux::nullCheck(x__22370)->
                                                               FMGL(rank),
                                                             ((x10_int)1)))))
                                {
                                    
                                    //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                    if (true) {
                                        
                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                        x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                    }
                                    
                                }
                                
                                //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                ret22392 = (__extension__ ({
                                    
                                    //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                    x10_int i22372 = x__i22371;
                                    
                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                    x10_int ret22373;
                                    
                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                    goto __ret22374; __ret22374: {
                                    {
                                        
                                        //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                        if (x10aux::nullCheck(x__22370)->
                                              FMGL(rail))
                                        {
                                            
                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                            ret22373 = (x10aux::nullCheck(x__22370)->
                                                          FMGL(raw))->__apply(i22372);
                                            
                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                            goto __ret22374_end_;
                                        } else {
                                            
                                            //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                            if (true && !(x10aux::nullCheck(x__22370)->
                                                            FMGL(region)->contains(
                                                            i22372)))
                                            {
                                                
                                                //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                x10::array::Array<void>::raiseBoundsError(
                                                  i22372);
                                            }
                                            
                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                            ret22373 = (x10aux::nullCheck(x__22370)->
                                                          FMGL(raw))->__apply(((x10_int) ((i22372) - (x10aux::nullCheck(x__22370)->
                                                                                                        FMGL(layout_min0)))));
                                            
                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                            goto __ret22374_end_;
                                        }
                                        
                                    }goto __ret22374_end_; __ret22374_end_: ;
                                    }
                                    ret22373;
                                    }))
                                    ;
                                ret22392;
                                }))
                                ;
                            
                            //#line 205 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
                            {
                                x10_int i22393;
                                for (
                                     //#line 205 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                     i22393 = x10aux::nullCheck(this22396)->
                                                FMGL(startIndex);
                                     ((i22393) < (x10aux::nullCheck(this22396)->
                                                    FMGL(deg)));
                                     
                                     //#line 205 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                     i22393 = ((x10_int) ((i22393) + (((x10_int)1)))))
                                {
                                    
                                    //#line 206 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                    res22375 = ((res22375) + (x10aux::nullCheck(x10aux::nullCheck(this22396)->
                                                                                  FMGL(weights))->__apply(
                                                                i22393)));
                                }
                            }
                            }
                            
                            //#line 210 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                            ret22398 = res22375;
                            ret22398;
                        }))
                        ;
                        
                        //#line 81 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                        x10aux::nullCheck(x22394)->FMGL(total_weight) =
                          ((x10aux::nullCheck(x22394)->FMGL(total_weight)) + (y22395));
                    }
                    }
                    
                
                //#line 84 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
                  reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10aux::makeStringLit("total_weight = "), this->
                                                                                                                          FMGL(total_weight))));
                
                //#line 85 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                this->FMGL(nb_nodes) = ((x10_int) ((this->
                                                      FMGL(nb_nodes)) - (((x10_int)1))));
                }
                Graph* Graph::_make(x10::lang::String* filename,
                                    x10::lang::String* filename_w,
                                    x10_int type_file) {
                    Graph* this_ = new (memset(x10aux::alloc<Graph>(), 0, sizeof(Graph))) Graph();
                    this_->_constructor(filename, filename_w,
                    type_file);
                    return this_;
                }
                
                
                
            
            //#line 88 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorDecl_c
            void Graph::_constructor(x10::lang::String* filename,
                                     x10_int type_file, x10_int maxNode,
                                     x10_int direct, x10_boolean do_renumber) {
                
                //#line 88 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.AssignPropertyCall_c
                
                //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                this->Graph::__fieldInitializers11008();
                
                //#line 90 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
                  reinterpret_cast<x10::lang::Any*>(x10aux::makeStringLit("In read file part!!")));
                
                //#line 91 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                x10::io::File* finput =  ((new (memset(x10aux::alloc<x10::io::File>(), 0, sizeof(x10::io::File))) x10::io::File()))
                ;
                
                //#line 91 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
                (finput)->::x10::io::File::_constructor(filename);
                
                //#line 92 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                x10::array::Array<x10::lang::String*>* keys;
                
                //#line 93 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                x10::lang::Math* math =  ((new (memset(x10aux::alloc<x10::lang::Math>(), 0, sizeof(x10::lang::Math))) x10::lang::Math()))
                ;
                
                //#line 97 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                this->FMGL(links_r) = (__extension__ ({
                    
                    //#line 97 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                    x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* alloc11649 =
                       ((new (memset(x10aux::alloc<x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*> >(), 0, sizeof(x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>))) x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>()))
                    ;
                    
                    //#line 268 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                    x10_int size22400 = ((x10_int) ((maxNode) + (((x10_int)1))));
                    
                    //#line 270 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                    x10::array::RectRegion1D* myReg22399 =
                       ((new (memset(x10aux::alloc<x10::array::RectRegion1D>(), 0, sizeof(x10::array::RectRegion1D))) x10::array::RectRegion1D()))
                    ;
                    
                    //#line 270 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10ConstructorCall_c
                    (myReg22399)->::x10::array::RectRegion1D::_constructor(
                      ((x10_int) ((size22400) - (((x10_int)1)))));
                    
                    //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                    alloc11649->FMGL(region) = reinterpret_cast<x10::array::Region*>(myReg22399);
                    
                    //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                    alloc11649->FMGL(rank) = ((x10_int)1);
                    
                    //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                    alloc11649->FMGL(rect) = true;
                    
                    //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                    alloc11649->FMGL(zeroBased) = true;
                    
                    //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                    alloc11649->FMGL(rail) = true;
                    
                    //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                    alloc11649->FMGL(size) = size22400;
                    
                    //#line 273 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                    alloc11649->FMGL(layout_min0) = alloc11649->
                                                      FMGL(layout_stride1) =
                      alloc11649->FMGL(layout_min1) = ((x10_int)0);
                    
                    //#line 274 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                    alloc11649->FMGL(layout) = (x10aux::class_cast_unchecked<x10::array::Array<x10_int>*>(X10_NULL));
                    
                    //#line 275 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                    alloc11649->FMGL(raw) = x10::util::IndexedMemoryChunk<void>::allocate<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* >(size22400, 8, false, true);
                    alloc11649;
                }))
                ;
                
                //#line 98 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
                {
                    for (
                         //#line 98 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                         this->FMGL(startIndex) = ((x10_int)0);
                         ((this->FMGL(startIndex)) <= (maxNode));
                         
                         //#line 98 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.StmtExpr_c
                         (__extension__ ({
                             
                             //#line 98 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                             Graph* x21248 = this;
                             
                             //#line 98 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                             ;
                             x10aux::nullCheck(x21248)->FMGL(startIndex) =
                               ((x10_int) ((x10aux::nullCheck(x21248)->
                                              FMGL(startIndex)) + (((x10_int)1))));
                         }))
                         ) {
                        
                        //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                        x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* p__22407 =
                          this->FMGL(links_r);
                        
                        //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                        x10_int p__22408 = this->FMGL(startIndex);
                        
                        //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                        x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* p__22409 =
                          (__extension__ ({
                            
                            //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                            x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* alloc22410 =
                               ((new (memset(x10aux::alloc<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> > >(), 0, sizeof(x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >))) x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >()))
                            ;
                            
                            //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
                            (alloc22410)->::x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >::_constructor();
                            alloc22410;
                        }))
                        ;
                        
                        //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                        x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22411;
                        
                        //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                        x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* x__22401 =
                          p__22407;
                        
                        //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                        x10_int x__i22402 = p__22408;
                        
                        //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                        x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* x__v22403 =
                          p__22409;
                        
                        //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                        if (!((x10aux::struct_equals(x10aux::nullCheck(x__22401)->
                                                       FMGL(rank),
                                                     ((x10_int)1)))))
                        {
                            
                            //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                            if (true) {
                                
                                //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                            }
                            
                        }
                        
                        //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                        ret22411 = (__extension__ ({
                            
                            //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                            x10_int i22404 = x__i22402;
                            
                            //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                            x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* v22405 =
                              x__v22403;
                            
                            //#line 553 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                            x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22406;
                            
                            //#line 555 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                            if (x10aux::nullCheck(x__22401)->
                                  FMGL(rail)) {
                                
                                //#line 557 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                (x10aux::nullCheck(x__22401)->
                                   FMGL(raw))->__set(i22404, v22405);
                            } else {
                                
                                //#line 559 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                if (true && !(x10aux::nullCheck(x__22401)->
                                                FMGL(region)->contains(
                                                i22404)))
                                {
                                    
                                    //#line 560 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                    x10::array::Array<void>::raiseBoundsError(
                                      i22404);
                                }
                                
                                //#line 562 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                (x10aux::nullCheck(x__22401)->
                                   FMGL(raw))->__set(((x10_int) ((i22404) - (x10aux::nullCheck(x__22401)->
                                                                               FMGL(layout_min0)))), v22405);
                            }
                            
                            //#line 564 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                            ret22406 = v22405;
                            ret22406;
                        }))
                        ;
                        
                        //#line 100 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Local_c
                        ret22411;
                    }
                }
                
                //#line 103 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                this->FMGL(nb_links) = ((x10_int)0);
                
                //#line 104 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
                {
                    x10::lang::Iterator<x10::lang::String*>* line11658;
                    for (
                         //#line 104 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                         line11658 = x10aux::nullCheck(finput->lines())->iterator();
                         x10::lang::Iterator<x10::lang::String*>::hasNext(x10aux::nullCheck(line11658));
                         ) {
                        
                        //#line 104 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                        x10::lang::String* line = x10::lang::Iterator<x10::lang::String*>::next(x10aux::nullCheck(line11658));
                        
                        //#line 105 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                        x10_int src;
                        
                        //#line 106 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                        x10_int dest;
                        
                        //#line 107 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                        x10_double weight = 1.0;
                        
                        //#line 109 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                        keys = x10::lang::StringHelper::split(x10aux::makeStringLit(" "), line);
                        
                        //#line 111 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                        if ((x10aux::struct_equals(type_file,
                                                   ((x10_int)1))))
                        {
                            
                            //#line 113 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                            src = x10::lang::IntNatives::parseInt((__extension__ ({
                                
                                //#line 113 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::array::Array<x10::lang::String*>* p__21273 =
                                  keys;
                                
                                //#line 113 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                ;
                                
                                //#line 113 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::lang::String* ret21280;
                                
                                //#line 113 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::array::Array<x10::lang::String*>* x__22412 =
                                  p__21273;
                                
                                //#line 113 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                if (!((x10aux::struct_equals(x10aux::nullCheck(x__22412)->
                                                               FMGL(rank),
                                                             ((x10_int)1)))))
                                {
                                    
                                    //#line 113 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                    if (true) {
                                        
                                        //#line 113 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                        x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                    }
                                    
                                }
                                
                                //#line 113 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                ret21280 = (__extension__ ({
                                    
                                    //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                                    ;
                                    
                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                    x10::lang::String* ret22413;
                                    
                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                    goto __ret22414; __ret22414: {
                                    {
                                        
                                        //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                        if (x10aux::nullCheck(x__22412)->
                                              FMGL(rail))
                                        {
                                            
                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                            ret22413 = (x10aux::nullCheck(x__22412)->
                                                          FMGL(raw))->__apply(((x10_int)0));
                                            
                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                            goto __ret22414_end_;
                                        } else {
                                            
                                            //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                            if (true && !(x10aux::nullCheck(x__22412)->
                                                            FMGL(region)->contains(
                                                            ((x10_int)0))))
                                            {
                                                
                                                //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                x10::array::Array<void>::raiseBoundsError(
                                                  ((x10_int)0));
                                            }
                                            
                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                            ret22413 = (x10aux::nullCheck(x__22412)->
                                                          FMGL(raw))->__apply(((x10_int) ((((x10_int)0)) - (x10aux::nullCheck(x__22412)->
                                                                                                              FMGL(layout_min0)))));
                                            
                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                            goto __ret22414_end_;
                                        }
                                        
                                    }goto __ret22414_end_; __ret22414_end_: ;
                                    }
                                    ret22413;
                                    }))
                                    ;
                                ret21280;
                                }))
                                );
                            
                            //#line 114 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                            dest = x10::lang::IntNatives::parseInt((__extension__ ({
                                
                                //#line 114 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::array::Array<x10::lang::String*>* p__21285 =
                                  keys;
                                
                                //#line 114 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                ;
                                
                                //#line 114 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::lang::String* ret21292;
                                
                                //#line 114 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::array::Array<x10::lang::String*>* x__22415 =
                                  p__21285;
                                
                                //#line 114 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                if (!((x10aux::struct_equals(x10aux::nullCheck(x__22415)->
                                                               FMGL(rank),
                                                             ((x10_int)1)))))
                                {
                                    
                                    //#line 114 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                    if (true) {
                                        
                                        //#line 114 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                        x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                    }
                                    
                                }
                                
                                //#line 114 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                ret21292 = (__extension__ ({
                                    
                                    //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                                    ;
                                    
                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                    x10::lang::String* ret22416;
                                    
                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                    goto __ret22417; __ret22417: {
                                    {
                                        
                                        //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                        if (x10aux::nullCheck(x__22415)->
                                              FMGL(rail))
                                        {
                                            
                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                            ret22416 = (x10aux::nullCheck(x__22415)->
                                                          FMGL(raw))->__apply(((x10_int)1));
                                            
                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                            goto __ret22417_end_;
                                        } else {
                                            
                                            //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                            if (true && !(x10aux::nullCheck(x__22415)->
                                                            FMGL(region)->contains(
                                                            ((x10_int)1))))
                                            {
                                                
                                                //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                x10::array::Array<void>::raiseBoundsError(
                                                  ((x10_int)1));
                                            }
                                            
                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                            ret22416 = (x10aux::nullCheck(x__22415)->
                                                          FMGL(raw))->__apply(((x10_int) ((((x10_int)1)) - (x10aux::nullCheck(x__22415)->
                                                                                                              FMGL(layout_min0)))));
                                            
                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                            goto __ret22417_end_;
                                        }
                                        
                                    }goto __ret22417_end_; __ret22417_end_: ;
                                    }
                                    ret22416;
                                    }))
                                    ;
                                ret21292;
                                }))
                                );
                            
                            //#line 115 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                            weight = ((x10_double) (x10::lang::IntNatives::parseInt((__extension__ ({
                                
                                //#line 115 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::array::Array<x10::lang::String*>* p__21297 =
                                  keys;
                                
                                //#line 115 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                ;
                                
                                //#line 115 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::lang::String* ret21304;
                                
                                //#line 115 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::array::Array<x10::lang::String*>* x__22418 =
                                  p__21297;
                                
                                //#line 115 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                if (!((x10aux::struct_equals(x10aux::nullCheck(x__22418)->
                                                               FMGL(rank),
                                                             ((x10_int)1)))))
                                {
                                    
                                    //#line 115 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                    if (true) {
                                        
                                        //#line 115 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                        x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                    }
                                    
                                }
                                
                                //#line 115 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                ret21304 = (__extension__ ({
                                    
                                    //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                                    ;
                                    
                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                    x10::lang::String* ret22419;
                                    
                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                    goto __ret22420; __ret22420: {
                                    {
                                        
                                        //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                        if (x10aux::nullCheck(x__22418)->
                                              FMGL(rail))
                                        {
                                            
                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                            ret22419 = (x10aux::nullCheck(x__22418)->
                                                          FMGL(raw))->__apply(((x10_int)2));
                                            
                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                            goto __ret22420_end_;
                                        } else {
                                            
                                            //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                            if (true && !(x10aux::nullCheck(x__22418)->
                                                            FMGL(region)->contains(
                                                            ((x10_int)2))))
                                            {
                                                
                                                //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                x10::array::Array<void>::raiseBoundsError(
                                                  ((x10_int)2));
                                            }
                                            
                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                            ret22419 = (x10aux::nullCheck(x__22418)->
                                                          FMGL(raw))->__apply(((x10_int) ((((x10_int)2)) - (x10aux::nullCheck(x__22418)->
                                                                                                              FMGL(layout_min0)))));
                                            
                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                            goto __ret22420_end_;
                                        }
                                        
                                    }goto __ret22420_end_; __ret22420_end_: ;
                                    }
                                    ret22419;
                                    }))
                                    ;
                                ret21304;
                                }))
                                )));
                            } else {
                                
                                //#line 119 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                src = x10::lang::IntNatives::parseInt((__extension__ ({
                                    
                                    //#line 119 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::array::Array<x10::lang::String*>* p__21309 =
                                      keys;
                                    
                                    //#line 119 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                    ;
                                    
                                    //#line 119 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::lang::String* ret21316;
                                    
                                    //#line 119 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::array::Array<x10::lang::String*>* x__22421 =
                                      p__21309;
                                    
                                    //#line 119 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__22421)->
                                                                   FMGL(rank),
                                                                 ((x10_int)1)))))
                                    {
                                        
                                        //#line 119 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                        if (true) {
                                            
                                            //#line 119 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                        }
                                        
                                    }
                                    
                                    //#line 119 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                    ret21316 = (__extension__ ({
                                        
                                        //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                                        ;
                                        
                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                        x10::lang::String* ret22422;
                                        
                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                        goto __ret22423; __ret22423: {
                                        {
                                            
                                            //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                            if (x10aux::nullCheck(x__22421)->
                                                  FMGL(rail))
                                            {
                                                
                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                ret22422 =
                                                  (x10aux::nullCheck(x__22421)->
                                                     FMGL(raw))->__apply(((x10_int)0));
                                                
                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                goto __ret22423_end_;
                                            } else {
                                                
                                                //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                if (true &&
                                                    !(x10aux::nullCheck(x__22421)->
                                                        FMGL(region)->contains(
                                                        ((x10_int)0))))
                                                {
                                                    
                                                    //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                    x10::array::Array<void>::raiseBoundsError(
                                                      ((x10_int)0));
                                                }
                                                
                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                ret22422 =
                                                  (x10aux::nullCheck(x__22421)->
                                                     FMGL(raw))->__apply(((x10_int) ((((x10_int)0)) - (x10aux::nullCheck(x__22421)->
                                                                                                         FMGL(layout_min0)))));
                                                
                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                goto __ret22423_end_;
                                            }
                                            
                                        }goto __ret22423_end_; __ret22423_end_: ;
                                        }
                                        ret22422;
                                        }))
                                        ;
                                    ret21316;
                                    }))
                                    );
                                
                                //#line 120 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                dest = x10::lang::IntNatives::parseInt((__extension__ ({
                                    
                                    //#line 120 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::array::Array<x10::lang::String*>* p__21321 =
                                      keys;
                                    
                                    //#line 120 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                    ;
                                    
                                    //#line 120 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::lang::String* ret21328;
                                    
                                    //#line 120 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::array::Array<x10::lang::String*>* x__22424 =
                                      p__21321;
                                    
                                    //#line 120 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__22424)->
                                                                   FMGL(rank),
                                                                 ((x10_int)1)))))
                                    {
                                        
                                        //#line 120 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                        if (true) {
                                            
                                            //#line 120 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                        }
                                        
                                    }
                                    
                                    //#line 120 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                    ret21328 = (__extension__ ({
                                        
                                        //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                                        ;
                                        
                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                        x10::lang::String* ret22425;
                                        
                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                        goto __ret22426; __ret22426: {
                                        {
                                            
                                            //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                            if (x10aux::nullCheck(x__22424)->
                                                  FMGL(rail))
                                            {
                                                
                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                ret22425 =
                                                  (x10aux::nullCheck(x__22424)->
                                                     FMGL(raw))->__apply(((x10_int)1));
                                                
                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                goto __ret22426_end_;
                                            } else {
                                                
                                                //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                if (true &&
                                                    !(x10aux::nullCheck(x__22424)->
                                                        FMGL(region)->contains(
                                                        ((x10_int)1))))
                                                {
                                                    
                                                    //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                    x10::array::Array<void>::raiseBoundsError(
                                                      ((x10_int)1));
                                                }
                                                
                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                ret22425 =
                                                  (x10aux::nullCheck(x__22424)->
                                                     FMGL(raw))->__apply(((x10_int) ((((x10_int)1)) - (x10aux::nullCheck(x__22424)->
                                                                                                         FMGL(layout_min0)))));
                                                
                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                goto __ret22426_end_;
                                            }
                                            
                                        }goto __ret22426_end_; __ret22426_end_: ;
                                        }
                                        ret22425;
                                        }))
                                        ;
                                    ret21328;
                                    }))
                                    );
                                }
                                
                                //#line 123 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                x10aux::nullCheck((__extension__ ({
                                    
                                    //#line 123 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* p__21333 =
                                      this->FMGL(links_r);
                                    
                                    //#line 123 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10_int p__21334 = src;
                                    
                                    //#line 123 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret21340;
                                    
                                    //#line 123 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* x__22427 =
                                      p__21333;
                                    
                                    //#line 123 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10_int x__i22428 = p__21334;
                                    
                                    //#line 123 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__22427)->
                                                                   FMGL(rank),
                                                                 ((x10_int)1)))))
                                    {
                                        
                                        //#line 123 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                        if (true) {
                                            
                                            //#line 123 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                        }
                                        
                                    }
                                    
                                    //#line 123 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                    ret21340 = (__extension__ ({
                                        
                                        //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                        x10_int i22429 = x__i22428;
                                        
                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                        x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22430;
                                        
                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                        goto __ret22431; __ret22431: {
                                        {
                                            
                                            //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                            if (x10aux::nullCheck(x__22427)->
                                                  FMGL(rail))
                                            {
                                                
                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                ret22430 =
                                                  (x10aux::nullCheck(x__22427)->
                                                     FMGL(raw))->__apply(i22429);
                                                
                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                goto __ret22431_end_;
                                            } else {
                                                
                                                //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                if (true &&
                                                    !(x10aux::nullCheck(x__22427)->
                                                        FMGL(region)->contains(
                                                        i22429)))
                                                {
                                                    
                                                    //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                    x10::array::Array<void>::raiseBoundsError(
                                                      i22429);
                                                }
                                                
                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                ret22430 =
                                                  (x10aux::nullCheck(x__22427)->
                                                     FMGL(raw))->__apply(((x10_int) ((i22429) - (x10aux::nullCheck(x__22427)->
                                                                                                   FMGL(layout_min0)))));
                                                
                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                goto __ret22431_end_;
                                            }
                                            
                                        }goto __ret22431_end_; __ret22431_end_: ;
                                        }
                                        ret22430;
                                        }))
                                        ;
                                    ret21340;
                                    }))
                                    )->add((__extension__ ({
                                               
                                               //#line 123 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                               x10::util::Pair<x10_int, x10_double> alloc11651 =
                                                  x10::util::Pair<x10_int, x10_double>::_alloc();
                                               
                                               //#line 21 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/util/Pair.x10": x10.ast.X10LocalDecl_c
                                               x10_int first22432 =
                                                 dest;
                                               
                                               //#line 21 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/util/Pair.x10": x10.ast.X10LocalDecl_c
                                               x10_double second22433 =
                                                 weight;
                                               
                                               //#line 22 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/util/Pair.x10": Eval of x10.ast.X10FieldAssign_c
                                               alloc11651->
                                                 FMGL(first) =
                                                 first22432;
                                               
                                               //#line 23 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/util/Pair.x10": Eval of x10.ast.X10FieldAssign_c
                                               alloc11651->
                                                 FMGL(second) =
                                                 second22433;
                                               alloc11651;
                                           }))
                                           );
                                
                                //#line 124 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                if ((x10aux::struct_equals(direct,
                                                           ((x10_int)1))) &&
                                    (!x10aux::struct_equals(src,
                                                            dest)))
                                {
                                    
                                    //#line 126 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                    x10aux::nullCheck((__extension__ ({
                                        
                                        //#line 126 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                        x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* p__21612 =
                                          this->FMGL(links_r);
                                        
                                        //#line 126 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                        x10_int p__21613 =
                                          dest;
                                        
                                        //#line 126 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                        x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret21619;
                                        
                                        //#line 126 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                        x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* x__22434 =
                                          p__21612;
                                        
                                        //#line 126 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                        x10_int x__i22435 =
                                          p__21613;
                                        
                                        //#line 126 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                        if (!((x10aux::struct_equals(x10aux::nullCheck(x__22434)->
                                                                       FMGL(rank),
                                                                     ((x10_int)1)))))
                                        {
                                            
                                            //#line 126 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                            if (true) {
                                                
                                                //#line 126 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                            }
                                            
                                        }
                                        
                                        //#line 126 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                        ret21619 = (__extension__ ({
                                            
                                            //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                            x10_int i22436 =
                                              x__i22435;
                                            
                                            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                            x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22437;
                                            
                                            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                            goto __ret22438; __ret22438: {
                                            {
                                                
                                                //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                if (x10aux::nullCheck(x__22434)->
                                                      FMGL(rail))
                                                {
                                                    
                                                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                    ret22437 =
                                                      (x10aux::nullCheck(x__22434)->
                                                         FMGL(raw))->__apply(i22436);
                                                    
                                                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                    goto __ret22438_end_;
                                                } else {
                                                    
                                                    //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                    if (true &&
                                                        !(x10aux::nullCheck(x__22434)->
                                                            FMGL(region)->contains(
                                                            i22436)))
                                                    {
                                                        
                                                        //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                        x10::array::Array<void>::raiseBoundsError(
                                                          i22436);
                                                    }
                                                    
                                                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                    ret22437 =
                                                      (x10aux::nullCheck(x__22434)->
                                                         FMGL(raw))->__apply(((x10_int) ((i22436) - (x10aux::nullCheck(x__22434)->
                                                                                                       FMGL(layout_min0)))));
                                                    
                                                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                    goto __ret22438_end_;
                                                }
                                                
                                            }goto __ret22438_end_; __ret22438_end_: ;
                                            }
                                            ret22437;
                                            }))
                                            ;
                                        ret21619;
                                        }))
                                        )->add((__extension__ ({
                                                   
                                                   //#line 126 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                   x10::util::Pair<x10_int, x10_double> alloc11652 =
                                                      x10::util::Pair<x10_int, x10_double>::_alloc();
                                                   
                                                   //#line 21 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/util/Pair.x10": x10.ast.X10LocalDecl_c
                                                   x10_int first22439 =
                                                     src;
                                                   
                                                   //#line 21 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/util/Pair.x10": x10.ast.X10LocalDecl_c
                                                   x10_double second22440 =
                                                     weight;
                                                   
                                                   //#line 22 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/util/Pair.x10": Eval of x10.ast.X10FieldAssign_c
                                                   alloc11652->
                                                     FMGL(first) =
                                                     first22439;
                                                   
                                                   //#line 23 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/util/Pair.x10": Eval of x10.ast.X10FieldAssign_c
                                                   alloc11652->
                                                     FMGL(second) =
                                                     second22440;
                                                   alloc11652;
                                               }))
                                               );
                                    
                                    //#line 127 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    Graph* x22441 = this;
                                    
                                    //#line 127 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                    x10aux::nullCheck(x22441)->
                                      FMGL(nb_links) = ((x10_int) ((x10aux::nullCheck(x22441)->
                                                                      FMGL(nb_links)) + (((x10_int)1))));
                                    }
                                    
                                
                                //#line 130 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                Graph* x22442 = this;
                                
                                //#line 130 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                x10aux::nullCheck(x22442)->
                                  FMGL(nb_links) = ((x10_int) ((x10aux::nullCheck(x22442)->
                                                                  FMGL(nb_links)) + (((x10_int)1))));
                                }
                                }
                                
                            
                            //#line 132 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                            x10::io::Console::FMGL(OUT__get)()->x10::io::Printer::println(
                              reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10::lang::String::__plus(x10::lang::String::__plus(x10aux::makeStringLit("nb_links, links_r.size() = "), this->
                                                                                                                                                                                                      FMGL(nb_links)), x10aux::makeStringLit(", ")), x10aux::nullCheck(this->
                                                                                                                                                                                                                                                                         FMGL(links_r))->
                                                                                                                                                                                                                                                       FMGL(size))));
                            }
                            Graph* Graph::_make(x10::lang::String* filename,
                                                x10_int type_file,
                                                x10_int maxNode,
                                                x10_int direct,
                                                x10_boolean do_renumber)
                            {
                                Graph* this_ = new (memset(x10aux::alloc<Graph>(), 0, sizeof(Graph))) Graph();
                                this_->_constructor(filename,
                                type_file, maxNode, direct,
                                do_renumber);
                                return this_;
                            }
                            
                            
                            
                            //#line 135 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorDecl_c
                            void Graph::_constructor(x10::lang::String* filename,
                                                     x10_int type_file,
                                                     x10_int maxNode,
                                                     x10_int edgeNum) {
                                
                                //#line 135 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.AssignPropertyCall_c
                                
                                //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                this->Graph::__fieldInitializers11008();
                                
                                //#line 137 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                this->FMGL(nb_nodes) = maxNode;
                                
                                //#line 139 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                this->FMGL(degrees) = (__extension__ ({
                                    
                                    //#line 139 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::array::Array<x10_int>* alloc11653 =
                                       ((new (memset(x10aux::alloc<x10::array::Array<x10_int> >(), 0, sizeof(x10::array::Array<x10_int>))) x10::array::Array<x10_int>()))
                                    ;
                                    
                                    //#line 268 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                    x10_int size22444 = ((x10_int) ((this->
                                                                       FMGL(nb_nodes)) + (((x10_int)1))));
                                    
                                    //#line 270 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                    x10::array::RectRegion1D* myReg22443 =
                                       ((new (memset(x10aux::alloc<x10::array::RectRegion1D>(), 0, sizeof(x10::array::RectRegion1D))) x10::array::RectRegion1D()))
                                    ;
                                    
                                    //#line 270 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10ConstructorCall_c
                                    (myReg22443)->::x10::array::RectRegion1D::_constructor(
                                      ((x10_int) ((size22444) - (((x10_int)1)))));
                                    
                                    //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                    alloc11653->FMGL(region) =
                                      reinterpret_cast<x10::array::Region*>(myReg22443);
                                    
                                    //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                    alloc11653->FMGL(rank) =
                                      ((x10_int)1);
                                    
                                    //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                    alloc11653->FMGL(rect) =
                                      true;
                                    
                                    //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                    alloc11653->FMGL(zeroBased) =
                                      true;
                                    
                                    //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                    alloc11653->FMGL(rail) =
                                      true;
                                    
                                    //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                    alloc11653->FMGL(size) =
                                      size22444;
                                    
                                    //#line 273 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                    alloc11653->FMGL(layout_min0) =
                                      alloc11653->FMGL(layout_stride1) =
                                      alloc11653->FMGL(layout_min1) =
                                      ((x10_int)0);
                                    
                                    //#line 274 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                    alloc11653->FMGL(layout) =
                                      (x10aux::class_cast_unchecked<x10::array::Array<x10_int>*>(X10_NULL));
                                    
                                    //#line 275 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                    alloc11653->FMGL(raw) =
                                      x10::util::IndexedMemoryChunk<void>::allocate<x10_int >(size22444, 8, false, true);
                                    alloc11653;
                                }))
                                ;
                                
                                //#line 140 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                this->FMGL(links) = (__extension__ ({
                                    
                                    //#line 140 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::util::ArrayList<x10_int>* alloc11654 =
                                       ((new (memset(x10aux::alloc<x10::util::ArrayList<x10_int> >(), 0, sizeof(x10::util::ArrayList<x10_int>))) x10::util::ArrayList<x10_int>()))
                                    ;
                                    
                                    //#line 140 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
                                    (alloc11654)->::x10::util::ArrayList<x10_int>::_constructor();
                                    alloc11654;
                                }))
                                ;
                                
                                //#line 141 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                this->FMGL(weights) = (__extension__ ({
                                    
                                    //#line 141 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                    x10::util::ArrayList<x10_double>* alloc11655 =
                                       ((new (memset(x10aux::alloc<x10::util::ArrayList<x10_double> >(), 0, sizeof(x10::util::ArrayList<x10_double>))) x10::util::ArrayList<x10_double>()))
                                    ;
                                    
                                    //#line 141 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
                                    (alloc11655)->::x10::util::ArrayList<x10_double>::_constructor();
                                    alloc11655;
                                }))
                                ;
                                
                                //#line 143 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                x10aux::nullCheck(this->FMGL(links))->clear();
                                
                                //#line 144 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                x10aux::nullCheck(this->FMGL(weights))->clear();
                                
                                //#line 145 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                this->FMGL(nb_links) = edgeNum;
                                
                                //#line 147 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::io::File* finput =  ((new (memset(x10aux::alloc<x10::io::File>(), 0, sizeof(x10::io::File))) x10::io::File()))
                                ;
                                
                                //#line 147 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
                                (finput)->::x10::io::File::_constructor(
                                  filename);
                                
                                //#line 148 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::array::Array<x10::lang::String*>* keys;
                                
                                //#line 149 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10_int num = ((x10_int)0);
                                
                                //#line 150 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10::lang::String* frontID =
                                  x10aux::makeStringLit("0");
                                
                                //#line 151 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                x10_int index = ((x10_int)0);
                                
                                //#line 153 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
                                {
                                    x10::lang::Iterator<x10::lang::String*>* line11660;
                                    for (
                                         //#line 153 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                         line11660 = x10aux::nullCheck(finput->lines())->iterator();
                                         x10::lang::Iterator<x10::lang::String*>::hasNext(x10aux::nullCheck(line11660));
                                         ) {
                                        
                                        //#line 153 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                        x10::lang::String* line =
                                          x10::lang::Iterator<x10::lang::String*>::next(x10aux::nullCheck(line11660));
                                        
                                        //#line 155 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                        keys = x10::lang::StringHelper::split(x10aux::makeStringLit(" "), line);
                                        
                                        //#line 156 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                        if (x10aux::equals(frontID,reinterpret_cast<x10::lang::Any*>((__extension__ ({
                                                
                                                //#line 156 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                x10::array::Array<x10::lang::String*>* p__21636 =
                                                  keys;
                                                
                                                //#line 156 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                                ;
                                                
                                                //#line 156 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                x10::lang::String* ret21643;
                                                
                                                //#line 156 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                x10::array::Array<x10::lang::String*>* x__22445 =
                                                  p__21636;
                                                
                                                //#line 156 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                if (!((x10aux::struct_equals(x10aux::nullCheck(x__22445)->
                                                                               FMGL(rank),
                                                                             ((x10_int)1)))))
                                                {
                                                    
                                                    //#line 156 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                    if (true)
                                                    {
                                                        
                                                        //#line 156 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                        x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                    }
                                                    
                                                }
                                                
                                                //#line 156 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                ret21643 =
                                                  (__extension__ ({
                                                    
                                                    //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                                                    ;
                                                    
                                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                    x10::lang::String* ret22446;
                                                    
                                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                    goto __ret22447; __ret22447: {
                                                    {
                                                        
                                                        //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                        if (x10aux::nullCheck(x__22445)->
                                                              FMGL(rail))
                                                        {
                                                            
                                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                            ret22446 =
                                                              (x10aux::nullCheck(x__22445)->
                                                                 FMGL(raw))->__apply(((x10_int)0));
                                                            
                                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                            goto __ret22447_end_;
                                                        }
                                                        else
                                                        {
                                                            
                                                            //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                            if (true &&
                                                                !(x10aux::nullCheck(x__22445)->
                                                                    FMGL(region)->contains(
                                                                    ((x10_int)0))))
                                                            {
                                                                
                                                                //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                x10::array::Array<void>::raiseBoundsError(
                                                                  ((x10_int)0));
                                                            }
                                                            
                                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                            ret22446 =
                                                              (x10aux::nullCheck(x__22445)->
                                                                 FMGL(raw))->__apply(((x10_int) ((((x10_int)0)) - (x10aux::nullCheck(x__22445)->
                                                                                                                     FMGL(layout_min0)))));
                                                            
                                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                            goto __ret22447_end_;
                                                        }
                                                        
                                                    }goto __ret22447_end_; __ret22447_end_: ;
                                                    }
                                                    ret22446;
                                                    }))
                                                    ;
                                                ret21643;
                                                }))
                                                ))) {
                                                
                                                //#line 158 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                num = ((x10_int) ((num) + (((x10_int)1))));
                                            } else {
                                                
                                                //#line 162 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                if ((!x10aux::struct_equals(index,
                                                                            ((x10_int)0))))
                                                {
                                                    
                                                    //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10::array::Array<x10_int>* p__22459 =
                                                      this->
                                                        FMGL(degrees);
                                                    
                                                    //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_int p__22460 =
                                                      index;
                                                    
                                                    //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_int p__22461 =
                                                      ((x10_int) ((num) + ((__extension__ ({
                                                        
                                                        //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10::array::Array<x10_int>* p__22462 =
                                                          this->
                                                            FMGL(degrees);
                                                        
                                                        //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10_int p__22463 =
                                                          ((x10_int) ((index) - (((x10_int)1))));
                                                        
                                                        //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10_int ret22464;
                                                        
                                                        //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10::array::Array<x10_int>* x__22448 =
                                                          p__22462;
                                                        
                                                        //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10_int x__i22449 =
                                                          p__22463;
                                                        
                                                        //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                        if (!((x10aux::struct_equals(x10aux::nullCheck(x__22448)->
                                                                                       FMGL(rank),
                                                                                     ((x10_int)1)))))
                                                        {
                                                            
                                                            //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                            if (true)
                                                            {
                                                                
                                                                //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                            }
                                                            
                                                        }
                                                        
                                                        //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                        ret22464 =
                                                          (__extension__ ({
                                                            
                                                            //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                            x10_int i22450 =
                                                              x__i22449;
                                                            
                                                            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                            x10_int ret22451;
                                                            
                                                            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                            goto __ret22452; __ret22452: {
                                                            {
                                                                
                                                                //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                if (x10aux::nullCheck(x__22448)->
                                                                      FMGL(rail))
                                                                {
                                                                    
                                                                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                    ret22451 =
                                                                      (x10aux::nullCheck(x__22448)->
                                                                         FMGL(raw))->__apply(i22450);
                                                                    
                                                                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                    goto __ret22452_end_;
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                    if (true &&
                                                                        !(x10aux::nullCheck(x__22448)->
                                                                            FMGL(region)->contains(
                                                                            i22450)))
                                                                    {
                                                                        
                                                                        //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                        x10::array::Array<void>::raiseBoundsError(
                                                                          i22450);
                                                                    }
                                                                    
                                                                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                    ret22451 =
                                                                      (x10aux::nullCheck(x__22448)->
                                                                         FMGL(raw))->__apply(((x10_int) ((i22450) - (x10aux::nullCheck(x__22448)->
                                                                                                                       FMGL(layout_min0)))));
                                                                    
                                                                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                    goto __ret22452_end_;
                                                                }
                                                                
                                                            }goto __ret22452_end_; __ret22452_end_: ;
                                                            }
                                                            ret22451;
                                                            }))
                                                            ;
                                                        ret22464;
                                                        }))
                                                        )));
                                                        
                                                    
                                                    //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_int ret22465;
                                                    
                                                    //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10::array::Array<x10_int>* x__22453 =
                                                      p__22459;
                                                    
                                                    //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_int x__i22454 =
                                                      p__22460;
                                                    
                                                    //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_int x__v22455 =
                                                      p__22461;
                                                    
                                                    //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__22453)->
                                                                                   FMGL(rank),
                                                                                 ((x10_int)1)))))
                                                    {
                                                        
                                                        //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                        if (true)
                                                        {
                                                            
                                                            //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                        }
                                                        
                                                    }
                                                    
                                                    //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                    ret22465 =
                                                      (__extension__ ({
                                                        
                                                        //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                        x10_int i22456 =
                                                          x__i22454;
                                                        
                                                        //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                        x10_int v22457 =
                                                          x__v22455;
                                                        
                                                        //#line 553 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                        x10_int ret22458;
                                                        
                                                        //#line 555 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                        if (x10aux::nullCheck(x__22453)->
                                                              FMGL(rail))
                                                        {
                                                            
                                                            //#line 557 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                            (x10aux::nullCheck(x__22453)->
                                                               FMGL(raw))->__set(i22456, v22457);
                                                        }
                                                        else
                                                        {
                                                            
                                                            //#line 559 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                            if (true &&
                                                                !(x10aux::nullCheck(x__22453)->
                                                                    FMGL(region)->contains(
                                                                    i22456)))
                                                            {
                                                                
                                                                //#line 560 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                x10::array::Array<void>::raiseBoundsError(
                                                                  i22456);
                                                            }
                                                            
                                                            //#line 562 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                            (x10aux::nullCheck(x__22453)->
                                                               FMGL(raw))->__set(((x10_int) ((i22456) - (x10aux::nullCheck(x__22453)->
                                                                                                           FMGL(layout_min0)))), v22457);
                                                        }
                                                        
                                                        //#line 564 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                        ret22458 =
                                                          v22457;
                                                        ret22458;
                                                    }))
                                                    ;
                                                    
                                                    //#line 163 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Local_c
                                                    ret22465;
                                                    } else
                                                    {
                                                        
                                                        //#line 165 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10::array::Array<x10_int>* p__22472 =
                                                          this->
                                                            FMGL(degrees);
                                                        
                                                        //#line 165 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10_int p__22473 =
                                                          index;
                                                        
                                                        //#line 165 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10_int p__22474 =
                                                          num;
                                                        
                                                        //#line 165 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10_int ret22475;
                                                        
                                                        //#line 165 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10::array::Array<x10_int>* x__22466 =
                                                          p__22472;
                                                        
                                                        //#line 165 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10_int x__i22467 =
                                                          p__22473;
                                                        
                                                        //#line 165 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10_int x__v22468 =
                                                          p__22474;
                                                        
                                                        //#line 165 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                        if (!((x10aux::struct_equals(x10aux::nullCheck(x__22466)->
                                                                                       FMGL(rank),
                                                                                     ((x10_int)1)))))
                                                        {
                                                            
                                                            //#line 165 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                            if (true)
                                                            {
                                                                
                                                                //#line 165 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                            }
                                                            
                                                        }
                                                        
                                                        //#line 165 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                        ret22475 =
                                                          (__extension__ ({
                                                            
                                                            //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                            x10_int i22469 =
                                                              x__i22467;
                                                            
                                                            //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                            x10_int v22470 =
                                                              x__v22468;
                                                            
                                                            //#line 553 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                            x10_int ret22471;
                                                            
                                                            //#line 555 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                            if (x10aux::nullCheck(x__22466)->
                                                                  FMGL(rail))
                                                            {
                                                                
                                                                //#line 557 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                (x10aux::nullCheck(x__22466)->
                                                                   FMGL(raw))->__set(i22469, v22470);
                                                            }
                                                            else
                                                            {
                                                                
                                                                //#line 559 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                if (true &&
                                                                    !(x10aux::nullCheck(x__22466)->
                                                                        FMGL(region)->contains(
                                                                        i22469)))
                                                                {
                                                                    
                                                                    //#line 560 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                    x10::array::Array<void>::raiseBoundsError(
                                                                      i22469);
                                                                }
                                                                
                                                                //#line 562 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                (x10aux::nullCheck(x__22466)->
                                                                   FMGL(raw))->__set(((x10_int) ((i22469) - (x10aux::nullCheck(x__22466)->
                                                                                                               FMGL(layout_min0)))), v22470);
                                                            }
                                                            
                                                            //#line 564 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                            ret22471 =
                                                              v22470;
                                                            ret22471;
                                                        }))
                                                        ;
                                                        
                                                        //#line 165 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Local_c
                                                        ret22475;
                                                    }
                                                    
                                                
                                                //#line 166 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                num = ((x10_int)1);
                                                
                                                //#line 167 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                frontID =
                                                  (__extension__ ({
                                                    
                                                    //#line 167 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10::array::Array<x10::lang::String*>* p__21692 =
                                                      keys;
                                                    
                                                    //#line 167 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                                    ;
                                                    
                                                    //#line 167 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10::lang::String* ret21699;
                                                    
                                                    //#line 167 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10::array::Array<x10::lang::String*>* x__22476 =
                                                      p__21692;
                                                    
                                                    //#line 167 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__22476)->
                                                                                   FMGL(rank),
                                                                                 ((x10_int)1)))))
                                                    {
                                                        
                                                        //#line 167 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                        if (true)
                                                        {
                                                            
                                                            //#line 167 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                        }
                                                        
                                                    }
                                                    
                                                    //#line 167 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                    ret21699 =
                                                      (__extension__ ({
                                                        
                                                        //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                                                        ;
                                                        
                                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                        x10::lang::String* ret22477;
                                                        
                                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                        goto __ret22478; __ret22478: {
                                                        {
                                                            
                                                            //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                            if (x10aux::nullCheck(x__22476)->
                                                                  FMGL(rail))
                                                            {
                                                                
                                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                ret22477 =
                                                                  (x10aux::nullCheck(x__22476)->
                                                                     FMGL(raw))->__apply(((x10_int)0));
                                                                
                                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                goto __ret22478_end_;
                                                            }
                                                            else
                                                            {
                                                                
                                                                //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                if (true &&
                                                                    !(x10aux::nullCheck(x__22476)->
                                                                        FMGL(region)->contains(
                                                                        ((x10_int)0))))
                                                                {
                                                                    
                                                                    //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                    x10::array::Array<void>::raiseBoundsError(
                                                                      ((x10_int)0));
                                                                }
                                                                
                                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                ret22477 =
                                                                  (x10aux::nullCheck(x__22476)->
                                                                     FMGL(raw))->__apply(((x10_int) ((((x10_int)0)) - (x10aux::nullCheck(x__22476)->
                                                                                                                         FMGL(layout_min0)))));
                                                                
                                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                goto __ret22478_end_;
                                                            }
                                                            
                                                        }goto __ret22478_end_; __ret22478_end_: ;
                                                        }
                                                        ret22477;
                                                        }))
                                                        ;
                                                    ret21699;
                                                    }))
                                                    ;
                                                
                                                //#line 168 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                index = ((x10_int) ((index) + (((x10_int)1))));
                                                }
                                                
                                                //#line 170 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                                x10aux::nullCheck(this->
                                                                    FMGL(links))->add(
                                                  x10::lang::IntNatives::parseInt((__extension__ ({
                                                      
                                                      //#line 170 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                      x10::array::Array<x10::lang::String*>* p__21704 =
                                                        keys;
                                                      
                                                      //#line 170 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                                      ;
                                                      
                                                      //#line 170 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                      x10::lang::String* ret21711;
                                                      
                                                      //#line 170 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                      x10::array::Array<x10::lang::String*>* x__22479 =
                                                        p__21704;
                                                      
                                                      //#line 170 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                      if (!((x10aux::struct_equals(x10aux::nullCheck(x__22479)->
                                                                                     FMGL(rank),
                                                                                   ((x10_int)1)))))
                                                      {
                                                          
                                                          //#line 170 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                          if (true)
                                                          {
                                                              
                                                              //#line 170 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                              x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                          }
                                                          
                                                      }
                                                      
                                                      //#line 170 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                      ret21711 =
                                                        (__extension__ ({
                                                          
                                                          //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                                                          ;
                                                          
                                                          //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                          x10::lang::String* ret22480;
                                                          
                                                          //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                          goto __ret22481; __ret22481: {
                                                          {
                                                              
                                                              //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                              if (x10aux::nullCheck(x__22479)->
                                                                    FMGL(rail))
                                                              {
                                                                  
                                                                  //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                  ret22480 =
                                                                    (x10aux::nullCheck(x__22479)->
                                                                       FMGL(raw))->__apply(((x10_int)1));
                                                                  
                                                                  //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                  goto __ret22481_end_;
                                                              }
                                                              else
                                                              {
                                                                  
                                                                  //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                  if (true &&
                                                                      !(x10aux::nullCheck(x__22479)->
                                                                          FMGL(region)->contains(
                                                                          ((x10_int)1))))
                                                                  {
                                                                      
                                                                      //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                      x10::array::Array<void>::raiseBoundsError(
                                                                        ((x10_int)1));
                                                                  }
                                                                  
                                                                  //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                  ret22480 =
                                                                    (x10aux::nullCheck(x__22479)->
                                                                       FMGL(raw))->__apply(((x10_int) ((((x10_int)1)) - (x10aux::nullCheck(x__22479)->
                                                                                                                           FMGL(layout_min0)))));
                                                                  
                                                                  //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                  goto __ret22481_end_;
                                                              }
                                                              
                                                          }goto __ret22481_end_; __ret22481_end_: ;
                                                          }
                                                          ret22480;
                                                          }))
                                                          ;
                                                      ret21711;
                                                      }))
                                                      ));
                                                
                                                //#line 171 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                if ((!x10aux::struct_equals(type_file,
                                                                            ((x10_int)0))))
                                                {
                                                    
                                                    //#line 172 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                                    x10aux::nullCheck(this->
                                                                        FMGL(weights))->add(
                                                      x10::lang::DoubleNatives::parseDouble((__extension__ ({
                                                          
                                                          //#line 172 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                          x10::array::Array<x10::lang::String*>* p__21716 =
                                                            keys;
                                                          
                                                          //#line 172 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                                          ;
                                                          
                                                          //#line 172 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                          x10::lang::String* ret21723;
                                                          
                                                          //#line 172 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                          x10::array::Array<x10::lang::String*>* x__22482 =
                                                            p__21716;
                                                          
                                                          //#line 172 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                          if (!((x10aux::struct_equals(x10aux::nullCheck(x__22482)->
                                                                                         FMGL(rank),
                                                                                       ((x10_int)1)))))
                                                          {
                                                              
                                                              //#line 172 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                              if (true)
                                                              {
                                                                  
                                                                  //#line 172 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                  x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                              }
                                                              
                                                          }
                                                          
                                                          //#line 172 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                          ret21723 =
                                                            (__extension__ ({
                                                              
                                                              //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                                                              ;
                                                              
                                                              //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                              x10::lang::String* ret22483;
                                                              
                                                              //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                              goto __ret22484; __ret22484: {
                                                              {
                                                                  
                                                                  //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                  if (x10aux::nullCheck(x__22482)->
                                                                        FMGL(rail))
                                                                  {
                                                                      
                                                                      //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                      ret22483 =
                                                                        (x10aux::nullCheck(x__22482)->
                                                                           FMGL(raw))->__apply(((x10_int)2));
                                                                      
                                                                      //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                      goto __ret22484_end_;
                                                                  }
                                                                  else
                                                                  {
                                                                      
                                                                      //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                      if (true &&
                                                                          !(x10aux::nullCheck(x__22482)->
                                                                              FMGL(region)->contains(
                                                                              ((x10_int)2))))
                                                                      {
                                                                          
                                                                          //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                          x10::array::Array<void>::raiseBoundsError(
                                                                            ((x10_int)2));
                                                                      }
                                                                      
                                                                      //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                      ret22483 =
                                                                        (x10aux::nullCheck(x__22482)->
                                                                           FMGL(raw))->__apply(((x10_int) ((((x10_int)2)) - (x10aux::nullCheck(x__22482)->
                                                                                                                               FMGL(layout_min0)))));
                                                                      
                                                                      //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                      goto __ret22484_end_;
                                                                  }
                                                                  
                                                              }goto __ret22484_end_; __ret22484_end_: ;
                                                              }
                                                              ret22483;
                                                              }))
                                                              ;
                                                          ret21723;
                                                          }))
                                                          ));
                                                    }
                                                    
                                                }
                                                }
                                                
                                            
                                            //#line 175 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                            if ((!x10aux::struct_equals(x10aux::nullCheck(this->
                                                                                            FMGL(degrees))->
                                                                          FMGL(size),
                                                                        ((x10_int)0))))
                                            {
                                                
                                                //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                x10::array::Array<x10_int>* p__22496 =
                                                  this->FMGL(degrees);
                                                
                                                //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                x10_int p__22497 =
                                                  index;
                                                
                                                //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                x10_int p__22498 =
                                                  ((x10_int) ((num) + ((__extension__ ({
                                                    
                                                    //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10::array::Array<x10_int>* p__22499 =
                                                      this->
                                                        FMGL(degrees);
                                                    
                                                    //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_int p__22500 =
                                                      ((x10_int) ((index) - (((x10_int)1))));
                                                    
                                                    //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_int ret22501;
                                                    
                                                    //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10::array::Array<x10_int>* x__22485 =
                                                      p__22499;
                                                    
                                                    //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_int x__i22486 =
                                                      p__22500;
                                                    
                                                    //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__22485)->
                                                                                   FMGL(rank),
                                                                                 ((x10_int)1)))))
                                                    {
                                                        
                                                        //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                        if (true)
                                                        {
                                                            
                                                            //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                        }
                                                        
                                                    }
                                                    
                                                    //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                    ret22501 =
                                                      (__extension__ ({
                                                        
                                                        //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                        x10_int i22487 =
                                                          x__i22486;
                                                        
                                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                        x10_int ret22488;
                                                        
                                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                        goto __ret22489; __ret22489: {
                                                        {
                                                            
                                                            //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                            if (x10aux::nullCheck(x__22485)->
                                                                  FMGL(rail))
                                                            {
                                                                
                                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                ret22488 =
                                                                  (x10aux::nullCheck(x__22485)->
                                                                     FMGL(raw))->__apply(i22487);
                                                                
                                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                goto __ret22489_end_;
                                                            }
                                                            else
                                                            {
                                                                
                                                                //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                if (true &&
                                                                    !(x10aux::nullCheck(x__22485)->
                                                                        FMGL(region)->contains(
                                                                        i22487)))
                                                                {
                                                                    
                                                                    //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                    x10::array::Array<void>::raiseBoundsError(
                                                                      i22487);
                                                                }
                                                                
                                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                ret22488 =
                                                                  (x10aux::nullCheck(x__22485)->
                                                                     FMGL(raw))->__apply(((x10_int) ((i22487) - (x10aux::nullCheck(x__22485)->
                                                                                                                   FMGL(layout_min0)))));
                                                                
                                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                goto __ret22489_end_;
                                                            }
                                                            
                                                        }goto __ret22489_end_; __ret22489_end_: ;
                                                        }
                                                        ret22488;
                                                        }))
                                                        ;
                                                    ret22501;
                                                    }))
                                                    )));
                                                    
                                                
                                                //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                x10_int ret22502;
                                                
                                                //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                x10::array::Array<x10_int>* x__22490 =
                                                  p__22496;
                                                
                                                //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                x10_int x__i22491 =
                                                  p__22497;
                                                
                                                //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                x10_int x__v22492 =
                                                  p__22498;
                                                
                                                //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                if (!((x10aux::struct_equals(x10aux::nullCheck(x__22490)->
                                                                               FMGL(rank),
                                                                             ((x10_int)1)))))
                                                {
                                                    
                                                    //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                    if (true)
                                                    {
                                                        
                                                        //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                        x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                    }
                                                    
                                                }
                                                
                                                //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                ret22502 =
                                                  (__extension__ ({
                                                    
                                                    //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                    x10_int i22493 =
                                                      x__i22491;
                                                    
                                                    //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                    x10_int v22494 =
                                                      x__v22492;
                                                    
                                                    //#line 553 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                    x10_int ret22495;
                                                    
                                                    //#line 555 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                    if (x10aux::nullCheck(x__22490)->
                                                          FMGL(rail))
                                                    {
                                                        
                                                        //#line 557 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                        (x10aux::nullCheck(x__22490)->
                                                           FMGL(raw))->__set(i22493, v22494);
                                                    } else
                                                    {
                                                        
                                                        //#line 559 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                        if (true &&
                                                            !(x10aux::nullCheck(x__22490)->
                                                                FMGL(region)->contains(
                                                                i22493)))
                                                        {
                                                            
                                                            //#line 560 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                            x10::array::Array<void>::raiseBoundsError(
                                                              i22493);
                                                        }
                                                        
                                                        //#line 562 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                        (x10aux::nullCheck(x__22490)->
                                                           FMGL(raw))->__set(((x10_int) ((i22493) - (x10aux::nullCheck(x__22490)->
                                                                                                       FMGL(layout_min0)))), v22494);
                                                    }
                                                    
                                                    //#line 564 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                    ret22495 =
                                                      v22494;
                                                    ret22495;
                                                }))
                                                ;
                                                
                                                //#line 176 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Local_c
                                                ret22502;
                                                } else {
                                                    
                                                    //#line 178 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10::array::Array<x10_int>* p__22509 =
                                                      this->
                                                        FMGL(degrees);
                                                    
                                                    //#line 178 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_int p__22510 =
                                                      index;
                                                    
                                                    //#line 178 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_int p__22511 =
                                                      num;
                                                    
                                                    //#line 178 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_int ret22512;
                                                    
                                                    //#line 178 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10::array::Array<x10_int>* x__22503 =
                                                      p__22509;
                                                    
                                                    //#line 178 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_int x__i22504 =
                                                      p__22510;
                                                    
                                                    //#line 178 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_int x__v22505 =
                                                      p__22511;
                                                    
                                                    //#line 178 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__22503)->
                                                                                   FMGL(rank),
                                                                                 ((x10_int)1)))))
                                                    {
                                                        
                                                        //#line 178 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                        if (true)
                                                        {
                                                            
                                                            //#line 178 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                        }
                                                        
                                                    }
                                                    
                                                    //#line 178 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                    ret22512 =
                                                      (__extension__ ({
                                                        
                                                        //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                        x10_int i22506 =
                                                          x__i22504;
                                                        
                                                        //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                        x10_int v22507 =
                                                          x__v22505;
                                                        
                                                        //#line 553 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                        x10_int ret22508;
                                                        
                                                        //#line 555 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                        if (x10aux::nullCheck(x__22503)->
                                                              FMGL(rail))
                                                        {
                                                            
                                                            //#line 557 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                            (x10aux::nullCheck(x__22503)->
                                                               FMGL(raw))->__set(i22506, v22507);
                                                        }
                                                        else
                                                        {
                                                            
                                                            //#line 559 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                            if (true &&
                                                                !(x10aux::nullCheck(x__22503)->
                                                                    FMGL(region)->contains(
                                                                    i22506)))
                                                            {
                                                                
                                                                //#line 560 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                x10::array::Array<void>::raiseBoundsError(
                                                                  i22506);
                                                            }
                                                            
                                                            //#line 562 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                            (x10aux::nullCheck(x__22503)->
                                                               FMGL(raw))->__set(((x10_int) ((i22506) - (x10aux::nullCheck(x__22503)->
                                                                                                           FMGL(layout_min0)))), v22507);
                                                        }
                                                        
                                                        //#line 564 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                        ret22508 =
                                                          v22507;
                                                        ret22508;
                                                    }))
                                                    ;
                                                    
                                                    //#line 178 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Local_c
                                                    ret22512;
                                                }
                                                
                                            
                                            //#line 180 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
                                            {
                                                x10_int i;
                                                for (
                                                     //#line 180 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                     i = ((x10_int)0);
                                                     ((i) <= (this->
                                                                FMGL(nb_nodes)));
                                                     
                                                     //#line 180 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                     i = ((x10_int) ((i) + (((x10_int)1)))))
                                                {
                                                    
                                                    //#line 181 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    Graph* x22555 =
                                                      this;
                                                    
                                                    //#line 181 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                    x10_double y22556 =
                                                      (__extension__ ({
                                                        
                                                        //#line 181 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        Graph* this22557 =
                                                          this;
                                                        
                                                        //#line 185 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10_int node22558 =
                                                          i;
                                                        
                                                        //#line 185 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10_double ret22559;
                                                        
                                                        //#line 190 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10_double res22536 =
                                                          0.0;
                                                        
                                                        //#line 192 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                        if ((x10aux::struct_equals(x10aux::nullCheck(x10aux::nullCheck(this22557)->
                                                                                                       FMGL(weights))->size(),
                                                                                   ((x10_int)0))))
                                                        {
                                                            
                                                            //#line 194 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                            res22536 =
                                                              ((x10_double) ((__extension__ ({
                                                                
                                                                //#line 213 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                x10_int node22537 =
                                                                  node22558;
                                                                
                                                                //#line 213 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                x10_int ret22538;
                                                                
                                                                //#line 213 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Labeled_c
                                                                goto __ret22539; __ret22539: {
                                                                {
                                                                    
                                                                    //#line 217 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                    if ((x10aux::struct_equals(node22537,
                                                                                               ((x10_int)0))))
                                                                    {
                                                                        
                                                                        //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                        ret22538 =
                                                                          (__extension__ ({
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::array::Array<x10_int>* p__22540 =
                                                                              x10aux::nullCheck(this22557)->
                                                                                FMGL(degrees);
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                                                            ;
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int ret22541;
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::array::Array<x10_int>* x__22513 =
                                                                              p__22540;
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                            if (!((x10aux::struct_equals(x10aux::nullCheck(x__22513)->
                                                                                                           FMGL(rank),
                                                                                                         ((x10_int)1)))))
                                                                            {
                                                                                
                                                                                //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                if (true)
                                                                                {
                                                                                    
                                                                                    //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                    x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                }
                                                                                
                                                                            }
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                            ret22541 =
                                                                              (__extension__ ({
                                                                                
                                                                                //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                                                                                ;
                                                                                
                                                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int ret22514;
                                                                                
                                                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                goto __ret22515; __ret22515: {
                                                                                {
                                                                                    
                                                                                    //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                    if (x10aux::nullCheck(x__22513)->
                                                                                          FMGL(rail))
                                                                                    {
                                                                                        
                                                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                        ret22514 =
                                                                                          (x10aux::nullCheck(x__22513)->
                                                                                             FMGL(raw))->__apply(((x10_int)0));
                                                                                        
                                                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                        goto __ret22515_end_;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        
                                                                                        //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                        if (true &&
                                                                                            !(x10aux::nullCheck(x__22513)->
                                                                                                FMGL(region)->contains(
                                                                                                ((x10_int)0))))
                                                                                        {
                                                                                            
                                                                                            //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                            x10::array::Array<void>::raiseBoundsError(
                                                                                              ((x10_int)0));
                                                                                        }
                                                                                        
                                                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                        ret22514 =
                                                                                          (x10aux::nullCheck(x__22513)->
                                                                                             FMGL(raw))->__apply(((x10_int) ((((x10_int)0)) - (x10aux::nullCheck(x__22513)->
                                                                                                                                                 FMGL(layout_min0)))));
                                                                                        
                                                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                        goto __ret22515_end_;
                                                                                    }
                                                                                    
                                                                                }goto __ret22515_end_; __ret22515_end_: ;
                                                                                }
                                                                                ret22514;
                                                                                }))
                                                                                ;
                                                                            ret22541;
                                                                            }))
                                                                            ;
                                                                        
                                                                        //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Branch_c
                                                                        goto __ret22539_end_;
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                            ret22538 =
                                                                              ((x10_int) (((__extension__ ({
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10::array::Array<x10_int>* p__22542 =
                                                                                  x10aux::nullCheck(this22557)->
                                                                                    FMGL(degrees);
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int p__22543 =
                                                                                  node22537;
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int ret22544;
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10::array::Array<x10_int>* x__22516 =
                                                                                  p__22542;
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int x__i22517 =
                                                                                  p__22543;
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                if (!((x10aux::struct_equals(x10aux::nullCheck(x__22516)->
                                                                                                               FMGL(rank),
                                                                                                             ((x10_int)1)))))
                                                                                {
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                    if (true)
                                                                                    {
                                                                                        
                                                                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                        x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                    }
                                                                                    
                                                                                }
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                ret22544 =
                                                                                  (__extension__ ({
                                                                                    
                                                                                    //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int i22518 =
                                                                                      x__i22517;
                                                                                    
                                                                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int ret22519;
                                                                                    
                                                                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                    goto __ret22520; __ret22520: {
                                                                                    {
                                                                                        
                                                                                        //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                        if (x10aux::nullCheck(x__22516)->
                                                                                              FMGL(rail))
                                                                                        {
                                                                                            
                                                                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                            ret22519 =
                                                                                              (x10aux::nullCheck(x__22516)->
                                                                                                 FMGL(raw))->__apply(i22518);
                                                                                            
                                                                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                            goto __ret22520_end_;
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            
                                                                                            //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                            if (true &&
                                                                                                !(x10aux::nullCheck(x__22516)->
                                                                                                    FMGL(region)->contains(
                                                                                                    i22518)))
                                                                                            {
                                                                                                
                                                                                                //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                x10::array::Array<void>::raiseBoundsError(
                                                                                                  i22518);
                                                                                            }
                                                                                            
                                                                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                            ret22519 =
                                                                                              (x10aux::nullCheck(x__22516)->
                                                                                                 FMGL(raw))->__apply(((x10_int) ((i22518) - (x10aux::nullCheck(x__22516)->
                                                                                                                                               FMGL(layout_min0)))));
                                                                                            
                                                                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                            goto __ret22520_end_;
                                                                                        }
                                                                                        
                                                                                    }goto __ret22520_end_; __ret22520_end_: ;
                                                                                    }
                                                                                    ret22519;
                                                                                    }))
                                                                                    ;
                                                                                ret22544;
                                                                                }))
                                                                                ) - ((__extension__ ({
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10::array::Array<x10_int>* p__22545 =
                                                                                      x10aux::nullCheck(this22557)->
                                                                                        FMGL(degrees);
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int p__22546 =
                                                                                      ((x10_int) ((node22537) - (((x10_int)1))));
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int ret22547;
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10::array::Array<x10_int>* x__22521 =
                                                                                      p__22545;
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int x__i22522 =
                                                                                      p__22546;
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__22521)->
                                                                                                                   FMGL(rank),
                                                                                                                 ((x10_int)1)))))
                                                                                    {
                                                                                        
                                                                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                        if (true)
                                                                                        {
                                                                                            
                                                                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                        }
                                                                                        
                                                                                    }
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                    ret22547 =
                                                                                      (__extension__ ({
                                                                                        
                                                                                        //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                        x10_int i22523 =
                                                                                          x__i22522;
                                                                                        
                                                                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                        x10_int ret22524;
                                                                                        
                                                                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                        goto __ret22525; __ret22525: {
                                                                                        {
                                                                                            
                                                                                            //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                            if (x10aux::nullCheck(x__22521)->
                                                                                                  FMGL(rail))
                                                                                            {
                                                                                                
                                                                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                ret22524 =
                                                                                                  (x10aux::nullCheck(x__22521)->
                                                                                                     FMGL(raw))->__apply(i22523);
                                                                                                
                                                                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                goto __ret22525_end_;
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                
                                                                                                //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                if (true &&
                                                                                                    !(x10aux::nullCheck(x__22521)->
                                                                                                        FMGL(region)->contains(
                                                                                                        i22523)))
                                                                                                {
                                                                                                    
                                                                                                    //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                    x10::array::Array<void>::raiseBoundsError(
                                                                                                      i22523);
                                                                                                }
                                                                                                
                                                                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                ret22524 =
                                                                                                  (x10aux::nullCheck(x__22521)->
                                                                                                     FMGL(raw))->__apply(((x10_int) ((i22523) - (x10aux::nullCheck(x__22521)->
                                                                                                                                                   FMGL(layout_min0)))));
                                                                                                
                                                                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                goto __ret22525_end_;
                                                                                            }
                                                                                            
                                                                                        }goto __ret22525_end_; __ret22525_end_: ;
                                                                                        }
                                                                                        ret22524;
                                                                                        }))
                                                                                        ;
                                                                                    ret22547;
                                                                                    }))
                                                                                    )));
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Branch_c
                                                                                goto __ret22539_end_;
                                                                            }
                                                                            
                                                                        }goto __ret22539_end_; __ret22539_end_: ;
                                                                        }
                                                                        
                                                                    ret22538;
                                                                    }))
                                                                    ));
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    //#line 199 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                    if ((x10aux::struct_equals(node22558,
                                                                                               ((x10_int)0))))
                                                                    {
                                                                        
                                                                        //#line 200 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                        x10aux::nullCheck(this22557)->
                                                                          FMGL(startIndex) =
                                                                          ((x10_int)0);
                                                                    }
                                                                    else
                                                                    {
                                                                        
                                                                        //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                        x10aux::nullCheck(this22557)->
                                                                          FMGL(startIndex) =
                                                                          (__extension__ ({
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::array::Array<x10_int>* p__22548 =
                                                                              x10aux::nullCheck(this22557)->
                                                                                FMGL(degrees);
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int p__22549 =
                                                                              ((x10_int) ((node22558) - (((x10_int)1))));
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int ret22550;
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::array::Array<x10_int>* x__22526 =
                                                                              p__22548;
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int x__i22527 =
                                                                              p__22549;
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                            if (!((x10aux::struct_equals(x10aux::nullCheck(x__22526)->
                                                                                                           FMGL(rank),
                                                                                                         ((x10_int)1)))))
                                                                            {
                                                                                
                                                                                //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                if (true)
                                                                                {
                                                                                    
                                                                                    //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                    x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                }
                                                                                
                                                                            }
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                            ret22550 =
                                                                              (__extension__ ({
                                                                                
                                                                                //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int i22528 =
                                                                                  x__i22527;
                                                                                
                                                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int ret22529;
                                                                                
                                                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                goto __ret22530; __ret22530: {
                                                                                {
                                                                                    
                                                                                    //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                    if (x10aux::nullCheck(x__22526)->
                                                                                          FMGL(rail))
                                                                                    {
                                                                                        
                                                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                        ret22529 =
                                                                                          (x10aux::nullCheck(x__22526)->
                                                                                             FMGL(raw))->__apply(i22528);
                                                                                        
                                                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                        goto __ret22530_end_;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        
                                                                                        //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                        if (true &&
                                                                                            !(x10aux::nullCheck(x__22526)->
                                                                                                FMGL(region)->contains(
                                                                                                i22528)))
                                                                                        {
                                                                                            
                                                                                            //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                            x10::array::Array<void>::raiseBoundsError(
                                                                                              i22528);
                                                                                        }
                                                                                        
                                                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                        ret22529 =
                                                                                          (x10aux::nullCheck(x__22526)->
                                                                                             FMGL(raw))->__apply(((x10_int) ((i22528) - (x10aux::nullCheck(x__22526)->
                                                                                                                                           FMGL(layout_min0)))));
                                                                                        
                                                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                        goto __ret22530_end_;
                                                                                    }
                                                                                    
                                                                                }goto __ret22530_end_; __ret22530_end_: ;
                                                                                }
                                                                                ret22529;
                                                                                }))
                                                                                ;
                                                                            ret22550;
                                                                            }))
                                                                            ;
                                                                        }
                                                                        
                                                                    
                                                                    //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                    x10aux::nullCheck(this22557)->
                                                                      FMGL(deg) =
                                                                      (__extension__ ({
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                        x10::array::Array<x10_int>* p__22551 =
                                                                          x10aux::nullCheck(this22557)->
                                                                            FMGL(degrees);
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                        x10_int p__22552 =
                                                                          node22558;
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                        x10_int ret22553;
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                        x10::array::Array<x10_int>* x__22531 =
                                                                          p__22551;
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                        x10_int x__i22532 =
                                                                          p__22552;
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                        if (!((x10aux::struct_equals(x10aux::nullCheck(x__22531)->
                                                                                                       FMGL(rank),
                                                                                                     ((x10_int)1)))))
                                                                        {
                                                                            
                                                                            //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                            if (true)
                                                                            {
                                                                                
                                                                                //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                            }
                                                                            
                                                                        }
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                        ret22553 =
                                                                          (__extension__ ({
                                                                            
                                                                            //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int i22533 =
                                                                              x__i22532;
                                                                            
                                                                            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int ret22534;
                                                                            
                                                                            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                            goto __ret22535; __ret22535: {
                                                                            {
                                                                                
                                                                                //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                if (x10aux::nullCheck(x__22531)->
                                                                                      FMGL(rail))
                                                                                {
                                                                                    
                                                                                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                    ret22534 =
                                                                                      (x10aux::nullCheck(x__22531)->
                                                                                         FMGL(raw))->__apply(i22533);
                                                                                    
                                                                                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                    goto __ret22535_end_;
                                                                                }
                                                                                else
                                                                                {
                                                                                    
                                                                                    //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                    if (true &&
                                                                                        !(x10aux::nullCheck(x__22531)->
                                                                                            FMGL(region)->contains(
                                                                                            i22533)))
                                                                                    {
                                                                                        
                                                                                        //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                        x10::array::Array<void>::raiseBoundsError(
                                                                                          i22533);
                                                                                    }
                                                                                    
                                                                                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                    ret22534 =
                                                                                      (x10aux::nullCheck(x__22531)->
                                                                                         FMGL(raw))->__apply(((x10_int) ((i22533) - (x10aux::nullCheck(x__22531)->
                                                                                                                                       FMGL(layout_min0)))));
                                                                                    
                                                                                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                    goto __ret22535_end_;
                                                                                }
                                                                                
                                                                            }goto __ret22535_end_; __ret22535_end_: ;
                                                                            }
                                                                            ret22534;
                                                                            }))
                                                                            ;
                                                                        ret22553;
                                                                        }))
                                                                        ;
                                                                    
                                                                    //#line 205 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
                                                                    {
                                                                        x10_int i22554;
                                                                        for (
                                                                             //#line 205 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                             i22554 =
                                                                               x10aux::nullCheck(this22557)->
                                                                                 FMGL(startIndex);
                                                                             ((i22554) < (x10aux::nullCheck(this22557)->
                                                                                            FMGL(deg)));
                                                                             
                                                                             //#line 205 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                             i22554 =
                                                                               ((x10_int) ((i22554) + (((x10_int)1)))))
                                                                        {
                                                                            
                                                                            //#line 206 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                            res22536 =
                                                                              ((res22536) + (x10aux::nullCheck(x10aux::nullCheck(this22557)->
                                                                                                                 FMGL(weights))->__apply(
                                                                                               i22554)));
                                                                        }
                                                                    }
                                                                    }
                                                                    
                                                                    //#line 210 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                    ret22559 =
                                                                      res22536;
                                                                    ret22559;
                                                                }))
                                                                ;
                                                                
                                                                //#line 181 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                x10aux::nullCheck(x22555)->
                                                                  FMGL(total_weight) =
                                                                  ((x10aux::nullCheck(x22555)->
                                                                      FMGL(total_weight)) + (y22556));
                                                            }
                                                            }
                                                            
                                                        }
                                                        Graph* Graph::_make(
                                                          x10::lang::String* filename,
                                                          x10_int type_file,
                                                          x10_int maxNode,
                                                          x10_int edgeNum)
                                                        {
                                                            Graph* this_ = new (memset(x10aux::alloc<Graph>(), 0, sizeof(Graph))) Graph();
                                                            this_->_constructor(filename,
                                                            type_file,
                                                            maxNode,
                                                            edgeNum);
                                                            return this_;
                                                        }
                                                        
                                                        
                                                        
                                                    
                                                    //#line 185 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10MethodDecl_c
                                                    x10_double
                                                      Graph::weighted_degree(
                                                      x10_int node) {
                                                        
                                                        //#line 190 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                        x10_double res =
                                                          0.0;
                                                        
                                                        //#line 192 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                        if ((x10aux::struct_equals(x10aux::nullCheck(this->
                                                                                                       FMGL(weights))->size(),
                                                                                   ((x10_int)0))))
                                                        {
                                                            
                                                            //#line 194 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                            res =
                                                              ((x10_double) ((__extension__ ({
                                                                
                                                                //#line 194 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                Graph* this21873 =
                                                                  this;
                                                                
                                                                //#line 213 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                x10_int node21860 =
                                                                  node;
                                                                
                                                                //#line 213 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                x10_int ret21874;
                                                                
                                                                //#line 213 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Labeled_c
                                                                goto __ret21875; __ret21875: {
                                                                {
                                                                    
                                                                    //#line 217 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                    if ((x10aux::struct_equals(node21860,
                                                                                               ((x10_int)0))))
                                                                    {
                                                                        
                                                                        //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                        ret21874 =
                                                                          (__extension__ ({
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::array::Array<x10_int>* p__21879 =
                                                                              x10aux::nullCheck(this21873)->
                                                                                FMGL(degrees);
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                                                            ;
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int ret21886;
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::array::Array<x10_int>* x__22560 =
                                                                              p__21879;
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                            if (!((x10aux::struct_equals(x10aux::nullCheck(x__22560)->
                                                                                                           FMGL(rank),
                                                                                                         ((x10_int)1)))))
                                                                            {
                                                                                
                                                                                //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                if (true)
                                                                                {
                                                                                    
                                                                                    //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                    x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                }
                                                                                
                                                                            }
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                            ret21886 =
                                                                              (__extension__ ({
                                                                                
                                                                                //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                                                                                ;
                                                                                
                                                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int ret22561;
                                                                                
                                                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                goto __ret22562; __ret22562: {
                                                                                {
                                                                                    
                                                                                    //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                    if (x10aux::nullCheck(x__22560)->
                                                                                          FMGL(rail))
                                                                                    {
                                                                                        
                                                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                        ret22561 =
                                                                                          (x10aux::nullCheck(x__22560)->
                                                                                             FMGL(raw))->__apply(((x10_int)0));
                                                                                        
                                                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                        goto __ret22562_end_;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        
                                                                                        //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                        if (true &&
                                                                                            !(x10aux::nullCheck(x__22560)->
                                                                                                FMGL(region)->contains(
                                                                                                ((x10_int)0))))
                                                                                        {
                                                                                            
                                                                                            //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                            x10::array::Array<void>::raiseBoundsError(
                                                                                              ((x10_int)0));
                                                                                        }
                                                                                        
                                                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                        ret22561 =
                                                                                          (x10aux::nullCheck(x__22560)->
                                                                                             FMGL(raw))->__apply(((x10_int) ((((x10_int)0)) - (x10aux::nullCheck(x__22560)->
                                                                                                                                                 FMGL(layout_min0)))));
                                                                                        
                                                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                        goto __ret22562_end_;
                                                                                    }
                                                                                    
                                                                                }goto __ret22562_end_; __ret22562_end_: ;
                                                                                }
                                                                                ret22561;
                                                                                }))
                                                                                ;
                                                                            ret21886;
                                                                            }))
                                                                            ;
                                                                        
                                                                        //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Branch_c
                                                                        goto __ret21875_end_;
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                            ret21874 =
                                                                              ((x10_int) (((__extension__ ({
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10::array::Array<x10_int>* p__21891 =
                                                                                  x10aux::nullCheck(this21873)->
                                                                                    FMGL(degrees);
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int p__21892 =
                                                                                  node21860;
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int ret21898;
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10::array::Array<x10_int>* x__22563 =
                                                                                  p__21891;
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int x__i22564 =
                                                                                  p__21892;
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                if (!((x10aux::struct_equals(x10aux::nullCheck(x__22563)->
                                                                                                               FMGL(rank),
                                                                                                             ((x10_int)1)))))
                                                                                {
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                    if (true)
                                                                                    {
                                                                                        
                                                                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                        x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                    }
                                                                                    
                                                                                }
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                ret21898 =
                                                                                  (__extension__ ({
                                                                                    
                                                                                    //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int i22565 =
                                                                                      x__i22564;
                                                                                    
                                                                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int ret22566;
                                                                                    
                                                                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                    goto __ret22567; __ret22567: {
                                                                                    {
                                                                                        
                                                                                        //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                        if (x10aux::nullCheck(x__22563)->
                                                                                              FMGL(rail))
                                                                                        {
                                                                                            
                                                                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                            ret22566 =
                                                                                              (x10aux::nullCheck(x__22563)->
                                                                                                 FMGL(raw))->__apply(i22565);
                                                                                            
                                                                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                            goto __ret22567_end_;
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            
                                                                                            //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                            if (true &&
                                                                                                !(x10aux::nullCheck(x__22563)->
                                                                                                    FMGL(region)->contains(
                                                                                                    i22565)))
                                                                                            {
                                                                                                
                                                                                                //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                x10::array::Array<void>::raiseBoundsError(
                                                                                                  i22565);
                                                                                            }
                                                                                            
                                                                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                            ret22566 =
                                                                                              (x10aux::nullCheck(x__22563)->
                                                                                                 FMGL(raw))->__apply(((x10_int) ((i22565) - (x10aux::nullCheck(x__22563)->
                                                                                                                                               FMGL(layout_min0)))));
                                                                                            
                                                                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                            goto __ret22567_end_;
                                                                                        }
                                                                                        
                                                                                    }goto __ret22567_end_; __ret22567_end_: ;
                                                                                    }
                                                                                    ret22566;
                                                                                    }))
                                                                                    ;
                                                                                ret21898;
                                                                                }))
                                                                                ) - ((__extension__ ({
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10::array::Array<x10_int>* p__21903 =
                                                                                      x10aux::nullCheck(this21873)->
                                                                                        FMGL(degrees);
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int p__21904 =
                                                                                      ((x10_int) ((node21860) - (((x10_int)1))));
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int ret21910;
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10::array::Array<x10_int>* x__22568 =
                                                                                      p__21903;
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int x__i22569 =
                                                                                      p__21904;
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__22568)->
                                                                                                                   FMGL(rank),
                                                                                                                 ((x10_int)1)))))
                                                                                    {
                                                                                        
                                                                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                        if (true)
                                                                                        {
                                                                                            
                                                                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                        }
                                                                                        
                                                                                    }
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                    ret21910 =
                                                                                      (__extension__ ({
                                                                                        
                                                                                        //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                        x10_int i22570 =
                                                                                          x__i22569;
                                                                                        
                                                                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                        x10_int ret22571;
                                                                                        
                                                                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                        goto __ret22572; __ret22572: {
                                                                                        {
                                                                                            
                                                                                            //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                            if (x10aux::nullCheck(x__22568)->
                                                                                                  FMGL(rail))
                                                                                            {
                                                                                                
                                                                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                ret22571 =
                                                                                                  (x10aux::nullCheck(x__22568)->
                                                                                                     FMGL(raw))->__apply(i22570);
                                                                                                
                                                                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                goto __ret22572_end_;
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                
                                                                                                //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                if (true &&
                                                                                                    !(x10aux::nullCheck(x__22568)->
                                                                                                        FMGL(region)->contains(
                                                                                                        i22570)))
                                                                                                {
                                                                                                    
                                                                                                    //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                    x10::array::Array<void>::raiseBoundsError(
                                                                                                      i22570);
                                                                                                }
                                                                                                
                                                                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                ret22571 =
                                                                                                  (x10aux::nullCheck(x__22568)->
                                                                                                     FMGL(raw))->__apply(((x10_int) ((i22570) - (x10aux::nullCheck(x__22568)->
                                                                                                                                                   FMGL(layout_min0)))));
                                                                                                
                                                                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                goto __ret22572_end_;
                                                                                            }
                                                                                            
                                                                                        }goto __ret22572_end_; __ret22572_end_: ;
                                                                                        }
                                                                                        ret22571;
                                                                                        }))
                                                                                        ;
                                                                                    ret21910;
                                                                                    }))
                                                                                    )));
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Branch_c
                                                                                goto __ret21875_end_;
                                                                            }
                                                                            
                                                                        }goto __ret21875_end_; __ret21875_end_: ;
                                                                        }
                                                                        
                                                                    ret21874;
                                                                    }))
                                                                    ));
                                                                }
                                                                else
                                                                {
                                                                    
                                                                    //#line 199 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                    if ((x10aux::struct_equals(node,
                                                                                               ((x10_int)0))))
                                                                    {
                                                                        
                                                                        //#line 200 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                        this->
                                                                          FMGL(startIndex) =
                                                                          ((x10_int)0);
                                                                    }
                                                                    else
                                                                    {
                                                                        
                                                                        //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                        this->
                                                                          FMGL(startIndex) =
                                                                          (__extension__ ({
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::array::Array<x10_int>* p__21915 =
                                                                              this->
                                                                                FMGL(degrees);
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int p__21916 =
                                                                              ((x10_int) ((node) - (((x10_int)1))));
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int ret21922;
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::array::Array<x10_int>* x__22573 =
                                                                              p__21915;
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int x__i22574 =
                                                                              p__21916;
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                            if (!((x10aux::struct_equals(x10aux::nullCheck(x__22573)->
                                                                                                           FMGL(rank),
                                                                                                         ((x10_int)1)))))
                                                                            {
                                                                                
                                                                                //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                if (true)
                                                                                {
                                                                                    
                                                                                    //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                    x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                }
                                                                                
                                                                            }
                                                                            
                                                                            //#line 202 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                            ret21922 =
                                                                              (__extension__ ({
                                                                                
                                                                                //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int i22575 =
                                                                                  x__i22574;
                                                                                
                                                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int ret22576;
                                                                                
                                                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                goto __ret22577; __ret22577: {
                                                                                {
                                                                                    
                                                                                    //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                    if (x10aux::nullCheck(x__22573)->
                                                                                          FMGL(rail))
                                                                                    {
                                                                                        
                                                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                        ret22576 =
                                                                                          (x10aux::nullCheck(x__22573)->
                                                                                             FMGL(raw))->__apply(i22575);
                                                                                        
                                                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                        goto __ret22577_end_;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        
                                                                                        //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                        if (true &&
                                                                                            !(x10aux::nullCheck(x__22573)->
                                                                                                FMGL(region)->contains(
                                                                                                i22575)))
                                                                                        {
                                                                                            
                                                                                            //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                            x10::array::Array<void>::raiseBoundsError(
                                                                                              i22575);
                                                                                        }
                                                                                        
                                                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                        ret22576 =
                                                                                          (x10aux::nullCheck(x__22573)->
                                                                                             FMGL(raw))->__apply(((x10_int) ((i22575) - (x10aux::nullCheck(x__22573)->
                                                                                                                                           FMGL(layout_min0)))));
                                                                                        
                                                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                        goto __ret22577_end_;
                                                                                    }
                                                                                    
                                                                                }goto __ret22577_end_; __ret22577_end_: ;
                                                                                }
                                                                                ret22576;
                                                                                }))
                                                                                ;
                                                                            ret21922;
                                                                            }))
                                                                            ;
                                                                        }
                                                                        
                                                                    
                                                                    //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                    this->
                                                                      FMGL(deg) =
                                                                      (__extension__ ({
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                        x10::array::Array<x10_int>* p__21927 =
                                                                          this->
                                                                            FMGL(degrees);
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                        x10_int p__21928 =
                                                                          node;
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                        x10_int ret21934;
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                        x10::array::Array<x10_int>* x__22578 =
                                                                          p__21927;
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                        x10_int x__i22579 =
                                                                          p__21928;
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                        if (!((x10aux::struct_equals(x10aux::nullCheck(x__22578)->
                                                                                                       FMGL(rank),
                                                                                                     ((x10_int)1)))))
                                                                        {
                                                                            
                                                                            //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                            if (true)
                                                                            {
                                                                                
                                                                                //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                            }
                                                                            
                                                                        }
                                                                        
                                                                        //#line 203 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                        ret21934 =
                                                                          (__extension__ ({
                                                                            
                                                                            //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int i22580 =
                                                                              x__i22579;
                                                                            
                                                                            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int ret22581;
                                                                            
                                                                            //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                            goto __ret22582; __ret22582: {
                                                                            {
                                                                                
                                                                                //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                if (x10aux::nullCheck(x__22578)->
                                                                                      FMGL(rail))
                                                                                {
                                                                                    
                                                                                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                    ret22581 =
                                                                                      (x10aux::nullCheck(x__22578)->
                                                                                         FMGL(raw))->__apply(i22580);
                                                                                    
                                                                                    //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                    goto __ret22582_end_;
                                                                                }
                                                                                else
                                                                                {
                                                                                    
                                                                                    //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                    if (true &&
                                                                                        !(x10aux::nullCheck(x__22578)->
                                                                                            FMGL(region)->contains(
                                                                                            i22580)))
                                                                                    {
                                                                                        
                                                                                        //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                        x10::array::Array<void>::raiseBoundsError(
                                                                                          i22580);
                                                                                    }
                                                                                    
                                                                                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                    ret22581 =
                                                                                      (x10aux::nullCheck(x__22578)->
                                                                                         FMGL(raw))->__apply(((x10_int) ((i22580) - (x10aux::nullCheck(x__22578)->
                                                                                                                                       FMGL(layout_min0)))));
                                                                                    
                                                                                    //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                    goto __ret22582_end_;
                                                                                }
                                                                                
                                                                            }goto __ret22582_end_; __ret22582_end_: ;
                                                                            }
                                                                            ret22581;
                                                                            }))
                                                                            ;
                                                                        ret21934;
                                                                        }))
                                                                        ;
                                                                    
                                                                    //#line 205 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
                                                                    {
                                                                        x10_int i;
                                                                        for (
                                                                             //#line 205 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                             i =
                                                                               this->
                                                                                 FMGL(startIndex);
                                                                             ((i) < (this->
                                                                                       FMGL(deg)));
                                                                             
                                                                             //#line 205 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                             i =
                                                                               ((x10_int) ((i) + (((x10_int)1)))))
                                                                        {
                                                                            
                                                                            //#line 206 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                            res =
                                                                              ((res) + (x10aux::nullCheck(this->
                                                                                                            FMGL(weights))->__apply(
                                                                                          i)));
                                                                        }
                                                                    }
                                                                    }
                                                                    
                                                                    //#line 210 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10Return_c
                                                                    return res;
                                                                    
                                                                }
                                                                
                                                                //#line 213 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10MethodDecl_c
                                                                x10_int
                                                                  Graph::nb_neighbors(
                                                                  x10_int node) {
                                                                    
                                                                    //#line 217 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                    if ((x10aux::struct_equals(node,
                                                                                               ((x10_int)0))))
                                                                    {
                                                                        
                                                                        //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10Return_c
                                                                        return (__extension__ ({
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::array::Array<x10_int>* p__21939 =
                                                                              this->
                                                                                FMGL(degrees);
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                                                            ;
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int ret21946;
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::array::Array<x10_int>* x__22583 =
                                                                              p__21939;
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                            if (!((x10aux::struct_equals(x10aux::nullCheck(x__22583)->
                                                                                                           FMGL(rank),
                                                                                                         ((x10_int)1)))))
                                                                            {
                                                                                
                                                                                //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                if (true)
                                                                                {
                                                                                    
                                                                                    //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                    x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                }
                                                                                
                                                                            }
                                                                            
                                                                            //#line 218 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                            ret21946 =
                                                                              (__extension__ ({
                                                                                
                                                                                //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Empty_c
                                                                                ;
                                                                                
                                                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int ret22584;
                                                                                
                                                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                goto __ret22585; __ret22585: {
                                                                                {
                                                                                    
                                                                                    //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                    if (x10aux::nullCheck(x__22583)->
                                                                                          FMGL(rail))
                                                                                    {
                                                                                        
                                                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                        ret22584 =
                                                                                          (x10aux::nullCheck(x__22583)->
                                                                                             FMGL(raw))->__apply(((x10_int)0));
                                                                                        
                                                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                        goto __ret22585_end_;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        
                                                                                        //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                        if (true &&
                                                                                            !(x10aux::nullCheck(x__22583)->
                                                                                                FMGL(region)->contains(
                                                                                                ((x10_int)0))))
                                                                                        {
                                                                                            
                                                                                            //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                            x10::array::Array<void>::raiseBoundsError(
                                                                                              ((x10_int)0));
                                                                                        }
                                                                                        
                                                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                        ret22584 =
                                                                                          (x10aux::nullCheck(x__22583)->
                                                                                             FMGL(raw))->__apply(((x10_int) ((((x10_int)0)) - (x10aux::nullCheck(x__22583)->
                                                                                                                                                 FMGL(layout_min0)))));
                                                                                        
                                                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                        goto __ret22585_end_;
                                                                                    }
                                                                                    
                                                                                }goto __ret22585_end_; __ret22585_end_: ;
                                                                                }
                                                                                ret22584;
                                                                                }))
                                                                                ;
                                                                            ret21946;
                                                                            }))
                                                                            ;
                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10Return_c
                                                                            return ((x10_int) (((__extension__ ({
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10::array::Array<x10_int>* p__21951 =
                                                                                  this->
                                                                                    FMGL(degrees);
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int p__21952 =
                                                                                  node;
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int ret21958;
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10::array::Array<x10_int>* x__22586 =
                                                                                  p__21951;
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int x__i22587 =
                                                                                  p__21952;
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                if (!((x10aux::struct_equals(x10aux::nullCheck(x__22586)->
                                                                                                               FMGL(rank),
                                                                                                             ((x10_int)1)))))
                                                                                {
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                    if (true)
                                                                                    {
                                                                                        
                                                                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                        x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                    }
                                                                                    
                                                                                }
                                                                                
                                                                                //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                ret21958 =
                                                                                  (__extension__ ({
                                                                                    
                                                                                    //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int i22588 =
                                                                                      x__i22587;
                                                                                    
                                                                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int ret22589;
                                                                                    
                                                                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                    goto __ret22590; __ret22590: {
                                                                                    {
                                                                                        
                                                                                        //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                        if (x10aux::nullCheck(x__22586)->
                                                                                              FMGL(rail))
                                                                                        {
                                                                                            
                                                                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                            ret22589 =
                                                                                              (x10aux::nullCheck(x__22586)->
                                                                                                 FMGL(raw))->__apply(i22588);
                                                                                            
                                                                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                            goto __ret22590_end_;
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            
                                                                                            //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                            if (true &&
                                                                                                !(x10aux::nullCheck(x__22586)->
                                                                                                    FMGL(region)->contains(
                                                                                                    i22588)))
                                                                                            {
                                                                                                
                                                                                                //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                x10::array::Array<void>::raiseBoundsError(
                                                                                                  i22588);
                                                                                            }
                                                                                            
                                                                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                            ret22589 =
                                                                                              (x10aux::nullCheck(x__22586)->
                                                                                                 FMGL(raw))->__apply(((x10_int) ((i22588) - (x10aux::nullCheck(x__22586)->
                                                                                                                                               FMGL(layout_min0)))));
                                                                                            
                                                                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                            goto __ret22590_end_;
                                                                                        }
                                                                                        
                                                                                    }goto __ret22590_end_; __ret22590_end_: ;
                                                                                    }
                                                                                    ret22589;
                                                                                    }))
                                                                                    ;
                                                                                ret21958;
                                                                                }))
                                                                                ) - ((__extension__ ({
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10::array::Array<x10_int>* p__21963 =
                                                                                      this->
                                                                                        FMGL(degrees);
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int p__21964 =
                                                                                      ((x10_int) ((node) - (((x10_int)1))));
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int ret21970;
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10::array::Array<x10_int>* x__22591 =
                                                                                      p__21963;
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int x__i22592 =
                                                                                      p__21964;
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__22591)->
                                                                                                                   FMGL(rank),
                                                                                                                 ((x10_int)1)))))
                                                                                    {
                                                                                        
                                                                                        //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                        if (true)
                                                                                        {
                                                                                            
                                                                                            //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                        }
                                                                                        
                                                                                    }
                                                                                    
                                                                                    //#line 220 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                    ret21970 =
                                                                                      (__extension__ ({
                                                                                        
                                                                                        //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                        x10_int i22593 =
                                                                                          x__i22592;
                                                                                        
                                                                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                        x10_int ret22594;
                                                                                        
                                                                                        //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                        goto __ret22595; __ret22595: {
                                                                                        {
                                                                                            
                                                                                            //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                            if (x10aux::nullCheck(x__22591)->
                                                                                                  FMGL(rail))
                                                                                            {
                                                                                                
                                                                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                ret22594 =
                                                                                                  (x10aux::nullCheck(x__22591)->
                                                                                                     FMGL(raw))->__apply(i22593);
                                                                                                
                                                                                                //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                goto __ret22595_end_;
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                
                                                                                                //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                if (true &&
                                                                                                    !(x10aux::nullCheck(x__22591)->
                                                                                                        FMGL(region)->contains(
                                                                                                        i22593)))
                                                                                                {
                                                                                                    
                                                                                                    //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                    x10::array::Array<void>::raiseBoundsError(
                                                                                                      i22593);
                                                                                                }
                                                                                                
                                                                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                ret22594 =
                                                                                                  (x10aux::nullCheck(x__22591)->
                                                                                                     FMGL(raw))->__apply(((x10_int) ((i22593) - (x10aux::nullCheck(x__22591)->
                                                                                                                                                   FMGL(layout_min0)))));
                                                                                                
                                                                                                //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                goto __ret22595_end_;
                                                                                            }
                                                                                            
                                                                                        }goto __ret22595_end_; __ret22595_end_: ;
                                                                                        }
                                                                                        ret22594;
                                                                                        }))
                                                                                        ;
                                                                                    ret21970;
                                                                                    }))
                                                                                    )));
                                                                                    
                                                                            }
                                                                            
                                                                        }
                                                                        
                                                                    
                                                                    //#line 223 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10MethodDecl_c
                                                                    x10_double
                                                                      Graph::nb_selfloops(
                                                                      x10_int node) {
                                                                        
                                                                        //#line 227 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                        if ((x10aux::struct_equals(node,
                                                                                                   ((x10_int)0))))
                                                                        {
                                                                            
                                                                            //#line 228 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                            this->
                                                                              FMGL(startIndex) =
                                                                              ((x10_int)0);
                                                                        }
                                                                        else
                                                                        {
                                                                            
                                                                            //#line 230 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                            this->
                                                                              FMGL(startIndex) =
                                                                              (__extension__ ({
                                                                                
                                                                                //#line 230 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10::array::Array<x10_int>* p__21975 =
                                                                                  this->
                                                                                    FMGL(degrees);
                                                                                
                                                                                //#line 230 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int p__21976 =
                                                                                  ((x10_int) ((node) - (((x10_int)1))));
                                                                                
                                                                                //#line 230 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int ret21982;
                                                                                
                                                                                //#line 230 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10::array::Array<x10_int>* x__22596 =
                                                                                  p__21975;
                                                                                
                                                                                //#line 230 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int x__i22597 =
                                                                                  p__21976;
                                                                                
                                                                                //#line 230 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                if (!((x10aux::struct_equals(x10aux::nullCheck(x__22596)->
                                                                                                               FMGL(rank),
                                                                                                             ((x10_int)1)))))
                                                                                {
                                                                                    
                                                                                    //#line 230 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                    if (true)
                                                                                    {
                                                                                        
                                                                                        //#line 230 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                        x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                    }
                                                                                    
                                                                                }
                                                                                
                                                                                //#line 230 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                ret21982 =
                                                                                  (__extension__ ({
                                                                                    
                                                                                    //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int i22598 =
                                                                                      x__i22597;
                                                                                    
                                                                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int ret22599;
                                                                                    
                                                                                    //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                    goto __ret22600; __ret22600: {
                                                                                    {
                                                                                        
                                                                                        //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                        if (x10aux::nullCheck(x__22596)->
                                                                                              FMGL(rail))
                                                                                        {
                                                                                            
                                                                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                            ret22599 =
                                                                                              (x10aux::nullCheck(x__22596)->
                                                                                                 FMGL(raw))->__apply(i22598);
                                                                                            
                                                                                            //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                            goto __ret22600_end_;
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            
                                                                                            //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                            if (true &&
                                                                                                !(x10aux::nullCheck(x__22596)->
                                                                                                    FMGL(region)->contains(
                                                                                                    i22598)))
                                                                                            {
                                                                                                
                                                                                                //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                x10::array::Array<void>::raiseBoundsError(
                                                                                                  i22598);
                                                                                            }
                                                                                            
                                                                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                            ret22599 =
                                                                                              (x10aux::nullCheck(x__22596)->
                                                                                                 FMGL(raw))->__apply(((x10_int) ((i22598) - (x10aux::nullCheck(x__22596)->
                                                                                                                                               FMGL(layout_min0)))));
                                                                                            
                                                                                            //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                            goto __ret22600_end_;
                                                                                        }
                                                                                        
                                                                                    }goto __ret22600_end_; __ret22600_end_: ;
                                                                                    }
                                                                                    ret22599;
                                                                                    }))
                                                                                    ;
                                                                                ret21982;
                                                                                }))
                                                                                ;
                                                                            }
                                                                            
                                                                        
                                                                        //#line 231 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                        this->
                                                                          FMGL(deg) =
                                                                          (__extension__ ({
                                                                            
                                                                            //#line 231 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::array::Array<x10_int>* p__21987 =
                                                                              this->
                                                                                FMGL(degrees);
                                                                            
                                                                            //#line 231 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int p__21988 =
                                                                              node;
                                                                            
                                                                            //#line 231 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int ret21994;
                                                                            
                                                                            //#line 231 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::array::Array<x10_int>* x__22601 =
                                                                              p__21987;
                                                                            
                                                                            //#line 231 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int x__i22602 =
                                                                              p__21988;
                                                                            
                                                                            //#line 231 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                            if (!((x10aux::struct_equals(x10aux::nullCheck(x__22601)->
                                                                                                           FMGL(rank),
                                                                                                         ((x10_int)1)))))
                                                                            {
                                                                                
                                                                                //#line 231 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                if (true)
                                                                                {
                                                                                    
                                                                                    //#line 231 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                    x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                }
                                                                                
                                                                            }
                                                                            
                                                                            //#line 231 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                            ret21994 =
                                                                              (__extension__ ({
                                                                                
                                                                                //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int i22603 =
                                                                                  x__i22602;
                                                                                
                                                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int ret22604;
                                                                                
                                                                                //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                goto __ret22605; __ret22605: {
                                                                                {
                                                                                    
                                                                                    //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                    if (x10aux::nullCheck(x__22601)->
                                                                                          FMGL(rail))
                                                                                    {
                                                                                        
                                                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                        ret22604 =
                                                                                          (x10aux::nullCheck(x__22601)->
                                                                                             FMGL(raw))->__apply(i22603);
                                                                                        
                                                                                        //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                        goto __ret22605_end_;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        
                                                                                        //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                        if (true &&
                                                                                            !(x10aux::nullCheck(x__22601)->
                                                                                                FMGL(region)->contains(
                                                                                                i22603)))
                                                                                        {
                                                                                            
                                                                                            //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                            x10::array::Array<void>::raiseBoundsError(
                                                                                              i22603);
                                                                                        }
                                                                                        
                                                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                        ret22604 =
                                                                                          (x10aux::nullCheck(x__22601)->
                                                                                             FMGL(raw))->__apply(((x10_int) ((i22603) - (x10aux::nullCheck(x__22601)->
                                                                                                                                           FMGL(layout_min0)))));
                                                                                        
                                                                                        //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                        goto __ret22605_end_;
                                                                                    }
                                                                                    
                                                                                }goto __ret22605_end_; __ret22605_end_: ;
                                                                                }
                                                                                ret22604;
                                                                                }))
                                                                                ;
                                                                            ret21994;
                                                                            }))
                                                                            ;
                                                                        
                                                                        //#line 232 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
                                                                        {
                                                                            x10_int i;
                                                                            for (
                                                                                 //#line 232 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                 i =
                                                                                   this->
                                                                                     FMGL(startIndex);
                                                                                 ((i) < (this->
                                                                                           FMGL(deg)));
                                                                                 
                                                                                 //#line 232 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                 i =
                                                                                   ((x10_int) ((i) + (((x10_int)1)))))
                                                                            {
                                                                                
                                                                                //#line 234 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                if ((x10aux::struct_equals(x10aux::nullCheck(this->
                                                                                                                               FMGL(links))->__apply(
                                                                                                             i),
                                                                                                           node)))
                                                                                {
                                                                                    
                                                                                    //#line 235 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                    if ((!x10aux::struct_equals(x10aux::nullCheck(this->
                                                                                                                                    FMGL(weights))->size(),
                                                                                                                ((x10_int)0))))
                                                                                    {
                                                                                        
                                                                                        //#line 236 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10Return_c
                                                                                        return x10aux::nullCheck(this->
                                                                                                                   FMGL(weights))->__apply(
                                                                                                 i);
                                                                                        
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        
                                                                                        //#line 238 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10Return_c
                                                                                        return 1.0;
                                                                                        
                                                                                    }
                                                                                    
                                                                                }
                                                                                
                                                                            }
                                                                        }
                                                                        
                                                                        //#line 241 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10Return_c
                                                                        return 0.0;
                                                                        }
                                                                        
                                                                        //#line 244 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10MethodDecl_c
                                                                        void
                                                                          Graph::display_binary(
                                                                          x10::lang::String* filename,
                                                                          x10::lang::String* filename_w,
                                                                          x10_int type_file) {
                                                                            
                                                                            //#line 246 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                                                            x10::io::Console::
                                                                              FMGL(OUT__get)()->x10::io::Printer::println(
                                                                              reinterpret_cast<x10::lang::Any*>(x10aux::makeStringLit("in display_binary part!!")));
                                                                            
                                                                            //#line 247 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                                                            x10::io::Console::
                                                                              FMGL(OUT__get)()->x10::io::Printer::println(
                                                                              reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10::lang::String::__plus(x10::lang::String::__plus(x10aux::makeStringLit("filename, filename_w = "), filename), x10aux::makeStringLit(", ")), filename_w)));
                                                                            
                                                                            //#line 248 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::io::File* foutput =
                                                                              
                                                                            ((new (memset(x10aux::alloc<x10::io::File>(), 0, sizeof(x10::io::File))) x10::io::File()))
                                                                            ;
                                                                            
                                                                            //#line 248 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
                                                                            (foutput)->::x10::io::File::_constructor(
                                                                              filename);
                                                                            
                                                                            //#line 249 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10::io::FileWriter* fwriter =
                                                                              foutput->openWrite();
                                                                            
                                                                            //#line 250 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                            this->
                                                                              FMGL(deg) =
                                                                              x10aux::nullCheck(this->
                                                                                                  FMGL(links_r))->
                                                                                FMGL(size);
                                                                            
                                                                            //#line 252 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                            this->
                                                                              FMGL(indexs) =
                                                                              (__extension__ ({
                                                                                
                                                                                //#line 252 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                x10::array::Array<x10_int>* alloc11656 =
                                                                                  
                                                                                ((new (memset(x10aux::alloc<x10::array::Array<x10_int> >(), 0, sizeof(x10::array::Array<x10_int>))) x10::array::Array<x10_int>()))
                                                                                ;
                                                                                
                                                                                //#line 268 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                x10_int size22607 =
                                                                                  this->
                                                                                    FMGL(deg);
                                                                                
                                                                                //#line 270 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                x10::array::RectRegion1D* myReg22606 =
                                                                                  
                                                                                ((new (memset(x10aux::alloc<x10::array::RectRegion1D>(), 0, sizeof(x10::array::RectRegion1D))) x10::array::RectRegion1D()))
                                                                                ;
                                                                                
                                                                                //#line 270 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10ConstructorCall_c
                                                                                (myReg22606)->::x10::array::RectRegion1D::_constructor(
                                                                                  ((x10_int) ((size22607) - (((x10_int)1)))));
                                                                                
                                                                                //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                alloc11656->
                                                                                  FMGL(region) =
                                                                                  reinterpret_cast<x10::array::Region*>(myReg22606);
                                                                                
                                                                                //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                alloc11656->
                                                                                  FMGL(rank) =
                                                                                  ((x10_int)1);
                                                                                
                                                                                //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                alloc11656->
                                                                                  FMGL(rect) =
                                                                                  true;
                                                                                
                                                                                //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                alloc11656->
                                                                                  FMGL(zeroBased) =
                                                                                  true;
                                                                                
                                                                                //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                alloc11656->
                                                                                  FMGL(rail) =
                                                                                  true;
                                                                                
                                                                                //#line 271 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                alloc11656->
                                                                                  FMGL(size) =
                                                                                  size22607;
                                                                                
                                                                                //#line 273 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                alloc11656->
                                                                                  FMGL(layout_min0) =
                                                                                  alloc11656->
                                                                                    FMGL(layout_stride1) =
                                                                                  alloc11656->
                                                                                    FMGL(layout_min1) =
                                                                                  ((x10_int)0);
                                                                                
                                                                                //#line 274 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                alloc11656->
                                                                                  FMGL(layout) =
                                                                                  (x10aux::class_cast_unchecked<x10::array::Array<x10_int>*>(X10_NULL));
                                                                                
                                                                                //#line 275 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                alloc11656->
                                                                                  FMGL(raw) =
                                                                                  x10::util::IndexedMemoryChunk<void>::allocate<x10_int >(size22607, 8, false, true);
                                                                                alloc11656;
                                                                            }))
                                                                            ;
                                                                            
                                                                            //#line 254 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                                                            x10aux::nullCheck(fwriter)->writeInt(
                                                                              this->
                                                                                FMGL(deg));
                                                                            
                                                                            //#line 256 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                            x10_int tot =
                                                                              ((x10_int)0);
                                                                            
                                                                            //#line 259 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
                                                                            {
                                                                                for (
                                                                                     //#line 259 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                     this->
                                                                                       FMGL(startIndex) =
                                                                                       ((x10_int)0);
                                                                                     ((this->
                                                                                         FMGL(startIndex)) < (this->
                                                                                                                FMGL(deg)));
                                                                                     
                                                                                     //#line 259 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.StmtExpr_c
                                                                                     (__extension__ ({
                                                                                         
                                                                                         //#line 259 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                         Graph* x22219 =
                                                                                           this;
                                                                                         
                                                                                         //#line 259 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                                                                         ;
                                                                                         x10aux::nullCheck(x22219)->
                                                                                           FMGL(startIndex) =
                                                                                           ((x10_int) ((x10aux::nullCheck(x22219)->
                                                                                                          FMGL(startIndex)) + (((x10_int)1))));
                                                                                     }))
                                                                                     )
                                                                                {
                                                                                    
                                                                                    //#line 260 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                    tot =
                                                                                      ((x10_int) ((tot) + (x10aux::nullCheck((__extension__ ({
                                                                                                               
                                                                                                               //#line 260 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                               x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* p__22224 =
                                                                                                                 this->
                                                                                                                   FMGL(links_r);
                                                                                                               
                                                                                                               //#line 260 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                               x10_int p__22225 =
                                                                                                                 this->
                                                                                                                   FMGL(startIndex);
                                                                                                               
                                                                                                               //#line 260 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                               x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22231;
                                                                                                               
                                                                                                               //#line 260 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                               x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* x__22608 =
                                                                                                                 p__22224;
                                                                                                               
                                                                                                               //#line 260 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                               x10_int x__i22609 =
                                                                                                                 p__22225;
                                                                                                               
                                                                                                               //#line 260 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                                               if (!((x10aux::struct_equals(x10aux::nullCheck(x__22608)->
                                                                                                                                              FMGL(rank),
                                                                                                                                            ((x10_int)1)))))
                                                                                                               {
                                                                                                                   
                                                                                                                   //#line 260 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                                                   if (true)
                                                                                                                   {
                                                                                                                       
                                                                                                                       //#line 260 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                                                       x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                                                   }
                                                                                                                   
                                                                                                               }
                                                                                                               
                                                                                                               //#line 260 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                               ret22231 =
                                                                                                                 (__extension__ ({
                                                                                                                   
                                                                                                                   //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                                                   x10_int i22610 =
                                                                                                                     x__i22609;
                                                                                                                   
                                                                                                                   //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                                                   x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22611;
                                                                                                                   
                                                                                                                   //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                                                   goto __ret22612; __ret22612: {
                                                                                                                   {
                                                                                                                       
                                                                                                                       //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                                       if (x10aux::nullCheck(x__22608)->
                                                                                                                             FMGL(rail))
                                                                                                                       {
                                                                                                                           
                                                                                                                           //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                                           ret22611 =
                                                                                                                             (x10aux::nullCheck(x__22608)->
                                                                                                                                FMGL(raw))->__apply(i22610);
                                                                                                                           
                                                                                                                           //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                                           goto __ret22612_end_;
                                                                                                                       }
                                                                                                                       else
                                                                                                                       {
                                                                                                                           
                                                                                                                           //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                                           if (true &&
                                                                                                                               !(x10aux::nullCheck(x__22608)->
                                                                                                                                   FMGL(region)->contains(
                                                                                                                                   i22610)))
                                                                                                                           {
                                                                                                                               
                                                                                                                               //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                                               x10::array::Array<void>::raiseBoundsError(
                                                                                                                                 i22610);
                                                                                                                           }
                                                                                                                           
                                                                                                                           //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                                           ret22611 =
                                                                                                                             (x10aux::nullCheck(x__22608)->
                                                                                                                                FMGL(raw))->__apply(((x10_int) ((i22610) - (x10aux::nullCheck(x__22608)->
                                                                                                                                                                              FMGL(layout_min0)))));
                                                                                                                           
                                                                                                                           //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                                           goto __ret22612_end_;
                                                                                                                       }
                                                                                                                       
                                                                                                                   }goto __ret22612_end_; __ret22612_end_: ;
                                                                                                                   }
                                                                                                                   ret22611;
                                                                                                                   }))
                                                                                                                   ;
                                                                                                               ret22231;
                                                                                                               }))
                                                                                                               )->size())));
                                                                                    
                                                                                    //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10::array::Array<x10_int>* p__22624 =
                                                                                      this->
                                                                                        FMGL(indexs);
                                                                                    
                                                                                    //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int p__22625 =
                                                                                      this->
                                                                                        FMGL(startIndex);
                                                                                    
                                                                                    //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int p__22626 =
                                                                                      x10aux::nullCheck((__extension__ ({
                                                                                          
                                                                                          //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                          x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* p__22627 =
                                                                                            this->
                                                                                              FMGL(links_r);
                                                                                          
                                                                                          //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                          x10_int p__22628 =
                                                                                            this->
                                                                                              FMGL(startIndex);
                                                                                          
                                                                                          //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                          x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22629;
                                                                                          
                                                                                          //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                          x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* x__22613 =
                                                                                            p__22627;
                                                                                          
                                                                                          //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                          x10_int x__i22614 =
                                                                                            p__22628;
                                                                                          
                                                                                          //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                          if (!((x10aux::struct_equals(x10aux::nullCheck(x__22613)->
                                                                                                                         FMGL(rank),
                                                                                                                       ((x10_int)1)))))
                                                                                          {
                                                                                              
                                                                                              //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                              if (true)
                                                                                              {
                                                                                                  
                                                                                                  //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                                  x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                              }
                                                                                              
                                                                                          }
                                                                                          
                                                                                          //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                          ret22629 =
                                                                                            (__extension__ ({
                                                                                              
                                                                                              //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                              x10_int i22615 =
                                                                                                x__i22614;
                                                                                              
                                                                                              //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                              x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22616;
                                                                                              
                                                                                              //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                              goto __ret22617; __ret22617: {
                                                                                              {
                                                                                                  
                                                                                                  //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                  if (x10aux::nullCheck(x__22613)->
                                                                                                        FMGL(rail))
                                                                                                  {
                                                                                                      
                                                                                                      //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                      ret22616 =
                                                                                                        (x10aux::nullCheck(x__22613)->
                                                                                                           FMGL(raw))->__apply(i22615);
                                                                                                      
                                                                                                      //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                      goto __ret22617_end_;
                                                                                                  }
                                                                                                  else
                                                                                                  {
                                                                                                      
                                                                                                      //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                      if (true &&
                                                                                                          !(x10aux::nullCheck(x__22613)->
                                                                                                              FMGL(region)->contains(
                                                                                                              i22615)))
                                                                                                      {
                                                                                                          
                                                                                                          //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                          x10::array::Array<void>::raiseBoundsError(
                                                                                                            i22615);
                                                                                                      }
                                                                                                      
                                                                                                      //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                      ret22616 =
                                                                                                        (x10aux::nullCheck(x__22613)->
                                                                                                           FMGL(raw))->__apply(((x10_int) ((i22615) - (x10aux::nullCheck(x__22613)->
                                                                                                                                                         FMGL(layout_min0)))));
                                                                                                      
                                                                                                      //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                      goto __ret22617_end_;
                                                                                                  }
                                                                                                  
                                                                                              }goto __ret22617_end_; __ret22617_end_: ;
                                                                                              }
                                                                                              ret22616;
                                                                                              }))
                                                                                              ;
                                                                                          ret22629;
                                                                                          }))
                                                                                          )->size();
                                                                                      
                                                                                    
                                                                                    //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int ret22630;
                                                                                    
                                                                                    //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10::array::Array<x10_int>* x__22618 =
                                                                                      p__22624;
                                                                                    
                                                                                    //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int x__i22619 =
                                                                                      p__22625;
                                                                                    
                                                                                    //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                    x10_int x__v22620 =
                                                                                      p__22626;
                                                                                    
                                                                                    //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                    if (!((x10aux::struct_equals(x10aux::nullCheck(x__22618)->
                                                                                                                   FMGL(rank),
                                                                                                                 ((x10_int)1)))))
                                                                                    {
                                                                                        
                                                                                        //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                        if (true)
                                                                                        {
                                                                                            
                                                                                            //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                            x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                        }
                                                                                        
                                                                                    }
                                                                                    
                                                                                    //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                    ret22630 =
                                                                                      (__extension__ ({
                                                                                        
                                                                                        //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                        x10_int i22621 =
                                                                                          x__i22619;
                                                                                        
                                                                                        //#line 554 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                        x10_int v22622 =
                                                                                          x__v22620;
                                                                                        
                                                                                        //#line 553 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                        x10_int ret22623;
                                                                                        
                                                                                        //#line 555 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                        if (x10aux::nullCheck(x__22618)->
                                                                                              FMGL(rail))
                                                                                        {
                                                                                            
                                                                                            //#line 557 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                            (x10aux::nullCheck(x__22618)->
                                                                                               FMGL(raw))->__set(i22621, v22622);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            
                                                                                            //#line 559 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                            if (true &&
                                                                                                !(x10aux::nullCheck(x__22618)->
                                                                                                    FMGL(region)->contains(
                                                                                                    i22621)))
                                                                                            {
                                                                                                
                                                                                                //#line 560 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                x10::array::Array<void>::raiseBoundsError(
                                                                                                  i22621);
                                                                                            }
                                                                                            
                                                                                            //#line 562 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                            (x10aux::nullCheck(x__22618)->
                                                                                               FMGL(raw))->__set(((x10_int) ((i22621) - (x10aux::nullCheck(x__22618)->
                                                                                                                                           FMGL(layout_min0)))), v22622);
                                                                                        }
                                                                                        
                                                                                        //#line 564 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                        ret22623 =
                                                                                          v22622;
                                                                                        ret22623;
                                                                                    }))
                                                                                    ;
                                                                                    
                                                                                    //#line 261 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Local_c
                                                                                    ret22630;
                                                                                    
                                                                                    //#line 262 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                                                                    x10aux::nullCheck(fwriter)->writeInt(
                                                                                      tot);
                                                                                    }
                                                                                    }
                                                                                    
                                                                                
                                                                                //#line 266 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
                                                                                {
                                                                                    for (
                                                                                         //#line 266 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                         this->
                                                                                           FMGL(startIndex) =
                                                                                           ((x10_int)0);
                                                                                         ((this->
                                                                                             FMGL(startIndex)) < (this->
                                                                                                                    FMGL(deg)));
                                                                                         
                                                                                         //#line 266 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.StmtExpr_c
                                                                                         (__extension__ ({
                                                                                             
                                                                                             //#line 266 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                             Graph* x22261 =
                                                                                               this;
                                                                                             
                                                                                             //#line 266 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                                                                             ;
                                                                                             x10aux::nullCheck(x22261)->
                                                                                               FMGL(startIndex) =
                                                                                               ((x10_int) ((x10aux::nullCheck(x22261)->
                                                                                                              FMGL(startIndex)) + (((x10_int)1))));
                                                                                         }))
                                                                                         )
                                                                                    {
                                                                                        
                                                                                        //#line 267 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
                                                                                        {
                                                                                            x10_int i;
                                                                                            for (
                                                                                                 //#line 267 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                 i =
                                                                                                   ((x10_int)0);
                                                                                                 ((i) < (x10aux::nullCheck((__extension__ ({
                                                                                                             
                                                                                                             //#line 267 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                             x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* p__22266 =
                                                                                                               this->
                                                                                                                 FMGL(links_r);
                                                                                                             
                                                                                                             //#line 267 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                             x10_int p__22267 =
                                                                                                               this->
                                                                                                                 FMGL(startIndex);
                                                                                                             
                                                                                                             //#line 267 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                             x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22273;
                                                                                                             
                                                                                                             //#line 267 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                             x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* x__22631 =
                                                                                                               p__22266;
                                                                                                             
                                                                                                             //#line 267 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                             x10_int x__i22632 =
                                                                                                               p__22267;
                                                                                                             
                                                                                                             //#line 267 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                                             if (!((x10aux::struct_equals(x10aux::nullCheck(x__22631)->
                                                                                                                                            FMGL(rank),
                                                                                                                                          ((x10_int)1)))))
                                                                                                             {
                                                                                                                 
                                                                                                                 //#line 267 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                                                 if (true)
                                                                                                                 {
                                                                                                                     
                                                                                                                     //#line 267 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                                                     x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                                                 }
                                                                                                                 
                                                                                                             }
                                                                                                             
                                                                                                             //#line 267 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                             ret22273 =
                                                                                                               (__extension__ ({
                                                                                                                 
                                                                                                                 //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                                                 x10_int i22633 =
                                                                                                                   x__i22632;
                                                                                                                 
                                                                                                                 //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                                                 x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22634;
                                                                                                                 
                                                                                                                 //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                                                 goto __ret22635; __ret22635: {
                                                                                                                 {
                                                                                                                     
                                                                                                                     //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                                     if (x10aux::nullCheck(x__22631)->
                                                                                                                           FMGL(rail))
                                                                                                                     {
                                                                                                                         
                                                                                                                         //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                                         ret22634 =
                                                                                                                           (x10aux::nullCheck(x__22631)->
                                                                                                                              FMGL(raw))->__apply(i22633);
                                                                                                                         
                                                                                                                         //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                                         goto __ret22635_end_;
                                                                                                                     }
                                                                                                                     else
                                                                                                                     {
                                                                                                                         
                                                                                                                         //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                                         if (true &&
                                                                                                                             !(x10aux::nullCheck(x__22631)->
                                                                                                                                 FMGL(region)->contains(
                                                                                                                                 i22633)))
                                                                                                                         {
                                                                                                                             
                                                                                                                             //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                                             x10::array::Array<void>::raiseBoundsError(
                                                                                                                               i22633);
                                                                                                                         }
                                                                                                                         
                                                                                                                         //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                                         ret22634 =
                                                                                                                           (x10aux::nullCheck(x__22631)->
                                                                                                                              FMGL(raw))->__apply(((x10_int) ((i22633) - (x10aux::nullCheck(x__22631)->
                                                                                                                                                                            FMGL(layout_min0)))));
                                                                                                                         
                                                                                                                         //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                                         goto __ret22635_end_;
                                                                                                                     }
                                                                                                                     
                                                                                                                 }goto __ret22635_end_; __ret22635_end_: ;
                                                                                                                 }
                                                                                                                 ret22634;
                                                                                                                 }))
                                                                                                                 ;
                                                                                                             ret22273;
                                                                                                             }))
                                                                                                             )->size()));
                                                                                                 
                                                                                                 //#line 267 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                 i =
                                                                                                   ((x10_int) ((i) + (((x10_int)1)))))
                                                                                                 {
                                                                                                     
                                                                                                     //#line 268 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                     x10_int dest =
                                                                                                       x10aux::nullCheck((__extension__ ({
                                                                                                           
                                                                                                           //#line 268 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                           x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* p__22278 =
                                                                                                             this->
                                                                                                               FMGL(links_r);
                                                                                                           
                                                                                                           //#line 268 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                           x10_int p__22279 =
                                                                                                             this->
                                                                                                               FMGL(startIndex);
                                                                                                           
                                                                                                           //#line 268 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                           x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22285;
                                                                                                           
                                                                                                           //#line 268 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                           x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* x__22636 =
                                                                                                             p__22278;
                                                                                                           
                                                                                                           //#line 268 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                           x10_int x__i22637 =
                                                                                                             p__22279;
                                                                                                           
                                                                                                           //#line 268 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                                           if (!((x10aux::struct_equals(x10aux::nullCheck(x__22636)->
                                                                                                                                          FMGL(rank),
                                                                                                                                        ((x10_int)1)))))
                                                                                                           {
                                                                                                               
                                                                                                               //#line 268 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                                               if (true)
                                                                                                               {
                                                                                                                   
                                                                                                                   //#line 268 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                                                   x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                                               }
                                                                                                               
                                                                                                           }
                                                                                                           
                                                                                                           //#line 268 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                           ret22285 =
                                                                                                             (__extension__ ({
                                                                                                               
                                                                                                               //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                                               x10_int i22638 =
                                                                                                                 x__i22637;
                                                                                                               
                                                                                                               //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                                               x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22639;
                                                                                                               
                                                                                                               //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                                               goto __ret22640; __ret22640: {
                                                                                                               {
                                                                                                                   
                                                                                                                   //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                                   if (x10aux::nullCheck(x__22636)->
                                                                                                                         FMGL(rail))
                                                                                                                   {
                                                                                                                       
                                                                                                                       //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                                       ret22639 =
                                                                                                                         (x10aux::nullCheck(x__22636)->
                                                                                                                            FMGL(raw))->__apply(i22638);
                                                                                                                       
                                                                                                                       //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                                       goto __ret22640_end_;
                                                                                                                   }
                                                                                                                   else
                                                                                                                   {
                                                                                                                       
                                                                                                                       //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                                       if (true &&
                                                                                                                           !(x10aux::nullCheck(x__22636)->
                                                                                                                               FMGL(region)->contains(
                                                                                                                               i22638)))
                                                                                                                       {
                                                                                                                           
                                                                                                                           //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                                           x10::array::Array<void>::raiseBoundsError(
                                                                                                                             i22638);
                                                                                                                       }
                                                                                                                       
                                                                                                                       //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                                       ret22639 =
                                                                                                                         (x10aux::nullCheck(x__22636)->
                                                                                                                            FMGL(raw))->__apply(((x10_int) ((i22638) - (x10aux::nullCheck(x__22636)->
                                                                                                                                                                          FMGL(layout_min0)))));
                                                                                                                       
                                                                                                                       //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                                       goto __ret22640_end_;
                                                                                                                   }
                                                                                                                   
                                                                                                               }goto __ret22640_end_; __ret22640_end_: ;
                                                                                                               }
                                                                                                               ret22639;
                                                                                                               }))
                                                                                                               ;
                                                                                                           ret22285;
                                                                                                           }))
                                                                                                           )->__apply(
                                                                                                             i)->
                                                                                                         FMGL(first);
                                                                                                       
                                                                                                     
                                                                                                     //#line 269 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                                                                                     x10aux::nullCheck(fwriter)->writeInt(
                                                                                                       dest);
                                                                                                     }
                                                                                                 }
                                                                                                 
                                                                                        }
                                                                                        }
                                                                                        
                                                                                    
                                                                                    //#line 290 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                                                                    x10aux::nullCheck(fwriter)->close();
                                                                                    
                                                                                    //#line 294 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                    if ((x10aux::struct_equals(type_file,
                                                                                                               ((x10_int)1))))
                                                                                    {
                                                                                        
                                                                                        //#line 295 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                        x10::io::File* foutput_w =
                                                                                          
                                                                                        ((new (memset(x10aux::alloc<x10::io::File>(), 0, sizeof(x10::io::File))) x10::io::File()))
                                                                                        ;
                                                                                        
                                                                                        //#line 295 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
                                                                                        (foutput_w)->::x10::io::File::_constructor(
                                                                                          filename_w);
                                                                                        
                                                                                        //#line 296 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                        x10::io::FileWriter* fwriter_w =
                                                                                          foutput_w->openWrite();
                                                                                        
                                                                                        //#line 297 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
                                                                                        {
                                                                                            for (
                                                                                                 //#line 297 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                                 this->
                                                                                                   FMGL(startIndex) =
                                                                                                   ((x10_int)0);
                                                                                                 ((this->
                                                                                                     FMGL(startIndex)) < (this->
                                                                                                                            FMGL(deg)));
                                                                                                 
                                                                                                 //#line 297 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.StmtExpr_c
                                                                                                 (__extension__ ({
                                                                                                     
                                                                                                     //#line 297 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                     Graph* x22308 =
                                                                                                       this;
                                                                                                     
                                                                                                     //#line 297 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Empty_c
                                                                                                     ;
                                                                                                     x10aux::nullCheck(x22308)->
                                                                                                       FMGL(startIndex) =
                                                                                                       ((x10_int) ((x10aux::nullCheck(x22308)->
                                                                                                                      FMGL(startIndex)) + (((x10_int)1))));
                                                                                                 }))
                                                                                                 )
                                                                                            {
                                                                                                
                                                                                                //#line 299 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.For_c
                                                                                                {
                                                                                                    x10_int i;
                                                                                                    for (
                                                                                                         //#line 299 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                         i =
                                                                                                           ((x10_int)0);
                                                                                                         ((i) < (x10aux::nullCheck((__extension__ ({
                                                                                                                     
                                                                                                                     //#line 299 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                                     x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* p__22313 =
                                                                                                                       this->
                                                                                                                         FMGL(links_r);
                                                                                                                     
                                                                                                                     //#line 299 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                                     x10_int p__22314 =
                                                                                                                       this->
                                                                                                                         FMGL(startIndex);
                                                                                                                     
                                                                                                                     //#line 299 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                                     x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22320;
                                                                                                                     
                                                                                                                     //#line 299 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                                     x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* x__22641 =
                                                                                                                       p__22313;
                                                                                                                     
                                                                                                                     //#line 299 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                                     x10_int x__i22642 =
                                                                                                                       p__22314;
                                                                                                                     
                                                                                                                     //#line 299 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                                                     if (!((x10aux::struct_equals(x10aux::nullCheck(x__22641)->
                                                                                                                                                    FMGL(rank),
                                                                                                                                                  ((x10_int)1)))))
                                                                                                                     {
                                                                                                                         
                                                                                                                         //#line 299 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                                                         if (true)
                                                                                                                         {
                                                                                                                             
                                                                                                                             //#line 299 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                                                             x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                                                         }
                                                                                                                         
                                                                                                                     }
                                                                                                                     
                                                                                                                     //#line 299 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                                     ret22320 =
                                                                                                                       (__extension__ ({
                                                                                                                         
                                                                                                                         //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                                                         x10_int i22643 =
                                                                                                                           x__i22642;
                                                                                                                         
                                                                                                                         //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                                                         x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22644;
                                                                                                                         
                                                                                                                         //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                                                         goto __ret22645; __ret22645: {
                                                                                                                         {
                                                                                                                             
                                                                                                                             //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                                             if (x10aux::nullCheck(x__22641)->
                                                                                                                                   FMGL(rail))
                                                                                                                             {
                                                                                                                                 
                                                                                                                                 //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                                                 ret22644 =
                                                                                                                                   (x10aux::nullCheck(x__22641)->
                                                                                                                                      FMGL(raw))->__apply(i22643);
                                                                                                                                 
                                                                                                                                 //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                                                 goto __ret22645_end_;
                                                                                                                             }
                                                                                                                             else
                                                                                                                             {
                                                                                                                                 
                                                                                                                                 //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                                                 if (true &&
                                                                                                                                     !(x10aux::nullCheck(x__22641)->
                                                                                                                                         FMGL(region)->contains(
                                                                                                                                         i22643)))
                                                                                                                                 {
                                                                                                                                     
                                                                                                                                     //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                                                     x10::array::Array<void>::raiseBoundsError(
                                                                                                                                       i22643);
                                                                                                                                 }
                                                                                                                                 
                                                                                                                                 //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                                                 ret22644 =
                                                                                                                                   (x10aux::nullCheck(x__22641)->
                                                                                                                                      FMGL(raw))->__apply(((x10_int) ((i22643) - (x10aux::nullCheck(x__22641)->
                                                                                                                                                                                    FMGL(layout_min0)))));
                                                                                                                                 
                                                                                                                                 //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                                                 goto __ret22645_end_;
                                                                                                                             }
                                                                                                                             
                                                                                                                         }goto __ret22645_end_; __ret22645_end_: ;
                                                                                                                         }
                                                                                                                         ret22644;
                                                                                                                         }))
                                                                                                                         ;
                                                                                                                     ret22320;
                                                                                                                     }))
                                                                                                                     )->size()));
                                                                                                         
                                                                                                         //#line 299 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                         i =
                                                                                                           ((x10_int) ((i) + (((x10_int)1)))))
                                                                                                         {
                                                                                                             
                                                                                                             //#line 301 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                             x10_double weight =
                                                                                                               x10aux::nullCheck((__extension__ ({
                                                                                                                   
                                                                                                                   //#line 301 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                                   x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* p__22325 =
                                                                                                                     this->
                                                                                                                       FMGL(links_r);
                                                                                                                   
                                                                                                                   //#line 301 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                                   x10_int p__22326 =
                                                                                                                     this->
                                                                                                                       FMGL(startIndex);
                                                                                                                   
                                                                                                                   //#line 301 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                                   x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22332;
                                                                                                                   
                                                                                                                   //#line 301 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                                   x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>* x__22646 =
                                                                                                                     p__22325;
                                                                                                                   
                                                                                                                   //#line 301 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                                                   x10_int x__i22647 =
                                                                                                                     p__22326;
                                                                                                                   
                                                                                                                   //#line 301 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                                                   if (!((x10aux::struct_equals(x10aux::nullCheck(x__22646)->
                                                                                                                                                  FMGL(rank),
                                                                                                                                                ((x10_int)1)))))
                                                                                                                   {
                                                                                                                       
                                                                                                                       //#line 301 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10If_c
                                                                                                                       if (true)
                                                                                                                       {
                                                                                                                           
                                                                                                                           //#line 301 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": polyglot.ast.Throw_c
                                                                                                                           x10aux::throwException(x10aux::nullCheck(x10::lang::FailedDynamicCheckException::_make(x10aux::makeStringLit("!(x$0.rank == 1)"))));
                                                                                                                       }
                                                                                                                       
                                                                                                                   }
                                                                                                                   
                                                                                                                   //#line 301 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                                   ret22332 =
                                                                                                                     (__extension__ ({
                                                                                                                       
                                                                                                                       //#line 453 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                                                       x10_int i22648 =
                                                                                                                         x__i22647;
                                                                                                                       
                                                                                                                       //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10LocalDecl_c
                                                                                                                       x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >* ret22649;
                                                                                                                       
                                                                                                                       //#line 452 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Labeled_c
                                                                                                                       goto __ret22650; __ret22650: {
                                                                                                                       {
                                                                                                                           
                                                                                                                           //#line 454 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                                           if (x10aux::nullCheck(x__22646)->
                                                                                                                                 FMGL(rail))
                                                                                                                           {
                                                                                                                               
                                                                                                                               //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                                               ret22649 =
                                                                                                                                 (x10aux::nullCheck(x__22646)->
                                                                                                                                    FMGL(raw))->__apply(i22648);
                                                                                                                               
                                                                                                                               //#line 456 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                                               goto __ret22650_end_;
                                                                                                                           }
                                                                                                                           else
                                                                                                                           {
                                                                                                                               
                                                                                                                               //#line 458 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": x10.ast.X10If_c
                                                                                                                               if (true &&
                                                                                                                                   !(x10aux::nullCheck(x__22646)->
                                                                                                                                       FMGL(region)->contains(
                                                                                                                                       i22648)))
                                                                                                                               {
                                                                                                                                   
                                                                                                                                   //#line 459 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10Call_c
                                                                                                                                   x10::array::Array<void>::raiseBoundsError(
                                                                                                                                     i22648);
                                                                                                                               }
                                                                                                                               
                                                                                                                               //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": Eval of x10.ast.X10LocalAssign_c
                                                                                                                               ret22649 =
                                                                                                                                 (x10aux::nullCheck(x__22646)->
                                                                                                                                    FMGL(raw))->__apply(((x10_int) ((i22648) - (x10aux::nullCheck(x__22646)->
                                                                                                                                                                                  FMGL(layout_min0)))));
                                                                                                                               
                                                                                                                               //#line 461 "/Users/donaldchi/Dropbox/sx10-2.3.1_2.2/x10.dist/stdlib/x10.jar:x10/array/Array.x10": polyglot.ast.Branch_c
                                                                                                                               goto __ret22650_end_;
                                                                                                                           }
                                                                                                                           
                                                                                                                       }goto __ret22650_end_; __ret22650_end_: ;
                                                                                                                       }
                                                                                                                       ret22649;
                                                                                                                       }))
                                                                                                                       ;
                                                                                                                   ret22332;
                                                                                                                   }))
                                                                                                                   )->__apply(
                                                                                                                     i)->
                                                                                                                 FMGL(second);
                                                                                                               
                                                                                                             
                                                                                                             //#line 302 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                                                                                             x10aux::nullCheck(fwriter_w)->writeDouble(
                                                                                                               weight);
                                                                                                             }
                                                                                                         }
                                                                                                         
                                                                                                }
                                                                                                }
                                                                                                
                                                                                            
                                                                                            //#line 305 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                                                                            x10aux::nullCheck(fwriter_w)->close();
                                                                                        }
                                                                                        
                                                                                    }
                                                                                    
                                                                                    //#line 310 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10MethodDecl_c
                                                                                    void
                                                                                      Graph::renumber(
                                                                                      x10_int type_file) {
                                                                                     
                                                                                    }
                                                                                    
                                                                                    //#line 348 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10MethodDecl_c
                                                                                    void
                                                                                      Graph::main(
                                                                                      x10::array::Array<x10::lang::String*>* args) {
                                                                                        
                                                                                        //#line 349 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                        x10::util::Timer* timer =
                                                                                          
                                                                                        ((new (memset(x10aux::alloc<x10::util::Timer>(), 0, sizeof(x10::util::Timer))) x10::util::Timer()))
                                                                                        ;
                                                                                        
                                                                                        //#line 350 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                        x10_long start =
                                                                                          x10::lang::RuntimeNatives::currentTimeMillis();
                                                                                        
                                                                                        //#line 351 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                        x10::lang::String* fileName =
                                                                                          x10aux::makeStringLit("../../Data/BlondelMethod/arxiv.txt");
                                                                                        
                                                                                        //#line 352 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                        Graph* g =
                                                                                          
                                                                                        ((new (memset(x10aux::alloc<Graph>(), 0, sizeof(Graph))) Graph()))
                                                                                        ;
                                                                                        
                                                                                        //#line 352 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10ConstructorCall_c
                                                                                        (g)->::Graph::_constructor(
                                                                                          fileName,
                                                                                          ((x10_int)0),
                                                                                          ((x10_int)9376),
                                                                                          ((x10_int)48214));
                                                                                        
                                                                                        //#line 354 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10LocalDecl_c
                                                                                        x10_long end =
                                                                                          x10::lang::RuntimeNatives::currentTimeMillis();
                                                                                        
                                                                                        //#line 355 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10Call_c
                                                                                        x10::io::Console::
                                                                                          FMGL(OUT__get)()->x10::io::Printer::println(
                                                                                          reinterpret_cast<x10::lang::Any*>(x10::lang::String::__plus(x10::lang::String::__plus(x10aux::makeStringLit("It used "), ((x10_long) ((end) - (start)))), x10aux::makeStringLit("ms"))));
                                                                                    }
                                                                                    
                                                                                    //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10MethodDecl_c
                                                                                    Graph*
                                                                                      Graph::Graph____this__Graph(
                                                                                      ) {
                                                                                        
                                                                                        //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10Return_c
                                                                                        return this;
                                                                                        
                                                                                    }
                                                                                    
                                                                                    //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": x10.ast.X10MethodDecl_c
                                                                                    void
                                                                                      Graph::__fieldInitializers11008(
                                                                                      ) {
                                                                                        
                                                                                        //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                        this->
                                                                                          FMGL(total_weight) =
                                                                                          0.0;
                                                                                        
                                                                                        //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                        this->
                                                                                          FMGL(nb_nodes) =
                                                                                          ((x10_int)0);
                                                                                        
                                                                                        //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                        this->
                                                                                          FMGL(nb_links) =
                                                                                          ((x10_int)0);
                                                                                        
                                                                                        //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                        this->
                                                                                          FMGL(degrees) =
                                                                                          (x10aux::class_cast_unchecked<x10::array::Array<x10_int>*>(X10_NULL));
                                                                                        
                                                                                        //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                        this->
                                                                                          FMGL(links) =
                                                                                          (x10aux::class_cast_unchecked<x10::util::ArrayList<x10_int>*>(X10_NULL));
                                                                                        
                                                                                        //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                        this->
                                                                                          FMGL(weights) =
                                                                                          (x10aux::class_cast_unchecked<x10::util::ArrayList<x10_double>*>(X10_NULL));
                                                                                        
                                                                                        //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                        this->
                                                                                          FMGL(links_r) =
                                                                                          (x10aux::class_cast_unchecked<x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>*>(X10_NULL));
                                                                                        
                                                                                        //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                        this->
                                                                                          FMGL(startIndex) =
                                                                                          ((x10_int)0);
                                                                                        
                                                                                        //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                        this->
                                                                                          FMGL(deg) =
                                                                                          ((x10_int)0);
                                                                                        
                                                                                        //#line 12 "/Users/donaldchi/Dropbox (smartnova)/work/Louvain-x10/version/12.10PerfectVersion/Graph.x10": Eval of x10.ast.X10FieldAssign_c
                                                                                        this->
                                                                                          FMGL(indexs) =
                                                                                          (x10aux::class_cast_unchecked<x10::array::Array<x10_int>*>(X10_NULL));
                                                                                    }
                                                                                    const x10aux::serialization_id_t Graph::_serialization_id = 
                                                                                        x10aux::DeserializationDispatcher::addDeserializer(Graph::_deserializer, x10aux::CLOSURE_KIND_NOT_ASYNC);
                                                                                    
                                                                                    void Graph::_serialize_body(x10aux::serialization_buffer& buf) {
                                                                                        buf.write(this->FMGL(total_weight));
                                                                                        buf.write(this->FMGL(nb_nodes));
                                                                                        buf.write(this->FMGL(nb_links));
                                                                                        buf.write(this->FMGL(degrees));
                                                                                        buf.write(this->FMGL(links));
                                                                                        buf.write(this->FMGL(weights));
                                                                                        buf.write(this->FMGL(links_r));
                                                                                        buf.write(this->FMGL(startIndex));
                                                                                        buf.write(this->FMGL(deg));
                                                                                        buf.write(this->FMGL(indexs));
                                                                                        
                                                                                    }
                                                                                    
                                                                                    x10::lang::Reference* Graph::_deserializer(x10aux::deserialization_buffer& buf) {
                                                                                        Graph* this_ = new (memset(x10aux::alloc<Graph>(), 0, sizeof(Graph))) Graph();
                                                                                        buf.record_reference(this_);
                                                                                        this_->_deserialize_body(buf);
                                                                                        return this_;
                                                                                    }
                                                                                    
                                                                                    void Graph::_deserialize_body(x10aux::deserialization_buffer& buf) {
                                                                                        FMGL(total_weight) = buf.read<x10_double>();
                                                                                        FMGL(nb_nodes) = buf.read<x10_int>();
                                                                                        FMGL(nb_links) = buf.read<x10_int>();
                                                                                        FMGL(degrees) = buf.read<x10::array::Array<x10_int>*>();
                                                                                        FMGL(links) = buf.read<x10::util::ArrayList<x10_int>*>();
                                                                                        FMGL(weights) = buf.read<x10::util::ArrayList<x10_double>*>();
                                                                                        FMGL(links_r) = buf.read<x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>*>();
                                                                                        FMGL(startIndex) = buf.read<x10_int>();
                                                                                        FMGL(deg) = buf.read<x10_int>();
                                                                                        FMGL(indexs) = buf.read<x10::array::Array<x10_int>*>();
                                                                                    }
                                                                                    
                                                                                    
                                                                                x10aux::RuntimeType Graph::rtt;
                                                                                void Graph::_initRTT() {
                                                                                    if (rtt.initStageOne(&rtt)) return;
                                                                                    const x10aux::RuntimeType** parents = NULL; 
                                                                                    rtt.initStageTwo("Graph",x10aux::RuntimeType::class_kind, 0, parents, 0, NULL, NULL);
                                                                                }
                                                                                
                                                                                /* END of Graph */
/*************************************************/
