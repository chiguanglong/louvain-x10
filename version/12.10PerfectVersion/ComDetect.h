#ifndef __COMDETECT_H
#define __COMDETECT_H

#include <x10rt.h>


#define X10_LANG_INT_H_NODEPS
#include <x10/lang/Int.h>
#undef X10_LANG_INT_H_NODEPS
#define X10_LANG_INT_H_NODEPS
#include <x10/lang/Int.h>
#undef X10_LANG_INT_H_NODEPS
#define X10_LANG_DOUBLE_H_NODEPS
#include <x10/lang/Double.h>
#undef X10_LANG_DOUBLE_H_NODEPS
#define X10_LANG_DOUBLE_H_NODEPS
#include <x10/lang/Double.h>
#undef X10_LANG_DOUBLE_H_NODEPS
namespace x10 { namespace lang { 
class String;
} } 
namespace x10 { namespace array { 
template<class TPMGL(T)> class Array;
} } 
namespace x10 { namespace io { 
class Printer;
} } 
namespace x10 { namespace io { 
class Console;
} } 
namespace x10 { namespace lang { 
class Any;
} } 
namespace x10 { namespace lang { 
class Boolean;
} } 
namespace x10 { namespace compiler { 
class CompilerFlags;
} } 
namespace x10 { namespace lang { 
class FailedDynamicCheckException;
} } 
namespace x10 { namespace util { 
template<class TPMGL(T)> class IndexedMemoryChunk;
} } 
namespace x10 { namespace array { 
class Region;
} } 
namespace x10 { namespace util { 
class Timer;
} } 
namespace x10 { namespace lang { 
class Long;
} } 
class Community;
class Graph;
class ComDetect : public x10::lang::X10Class   {
    public:
    RTT_H_DECLS_CLASS
    
    x10::lang::String* FMGL(filename);
    
    x10_int FMGL(it_random);
    
    x10_double FMGL(precision);
    
    x10_int FMGL(type_file);
    
    x10_int FMGL(passTimes);
    
    virtual void checkArgs(x10::array::Array<x10::lang::String*>* args);
    static void main(x10::array::Array<x10::lang::String*>* args);
    virtual ComDetect* ComDetect____this__ComDetect();
    void _constructor();
    
    static ComDetect* _make();
    
    virtual void __fieldInitializers3118();
    
    // Serialization
    public: static const x10aux::serialization_id_t _serialization_id;
    
    public: virtual x10aux::serialization_id_t _get_serialization_id() {
         return _serialization_id;
    }
    
    public: virtual void _serialize_body(x10aux::serialization_buffer& buf);
    
    public: static x10::lang::Reference* _deserializer(x10aux::deserialization_buffer& buf);
    
    public: void _deserialize_body(x10aux::deserialization_buffer& buf);
    
};

#endif // COMDETECT_H

class ComDetect;

#ifndef COMDETECT_H_NODEPS
#define COMDETECT_H_NODEPS
#ifndef COMDETECT_H_GENERICS
#define COMDETECT_H_GENERICS
#endif // COMDETECT_H_GENERICS
#endif // __COMDETECT_H_NODEPS
