#ifndef __GRAPH_H
#define __GRAPH_H

#include <x10rt.h>


#define X10_LANG_DOUBLE_H_NODEPS
#include <x10/lang/Double.h>
#undef X10_LANG_DOUBLE_H_NODEPS
#define X10_LANG_DOUBLE_H_NODEPS
#include <x10/lang/Double.h>
#undef X10_LANG_DOUBLE_H_NODEPS
#define X10_LANG_INT_H_NODEPS
#include <x10/lang/Int.h>
#undef X10_LANG_INT_H_NODEPS
#define X10_LANG_INT_H_NODEPS
#include <x10/lang/Int.h>
#undef X10_LANG_INT_H_NODEPS
#define X10_UTIL_PAIR_H_NODEPS
#include <x10/util/Pair.h>
#undef X10_UTIL_PAIR_H_NODEPS
namespace x10 { namespace array { 
template<class TPMGL(T)> class Array;
} } 
namespace x10 { namespace util { 
template<class TPMGL(T)> class ArrayList;
} } 
namespace x10 { namespace lang { 
class String;
} } 
namespace x10 { namespace io { 
class File;
} } 
namespace x10 { namespace io { 
class FileReader;
} } 
namespace x10 { namespace io { 
class Reader;
} } 
namespace x10 { namespace io { 
class Printer;
} } 
namespace x10 { namespace io { 
class Console;
} } 
namespace x10 { namespace lang { 
class Any;
} } 
namespace x10 { namespace array { 
class RectRegion1D;
} } 
namespace x10 { namespace array { 
class Region;
} } 
namespace x10 { namespace util { 
template<class TPMGL(T)> class IndexedMemoryChunk;
} } 
namespace x10 { namespace lang { 
class Boolean;
} } 
namespace x10 { namespace compiler { 
class CompilerFlags;
} } 
namespace x10 { namespace lang { 
class FailedDynamicCheckException;
} } 
namespace x10 { namespace lang { 
class Math;
} } 
namespace x10 { namespace lang { 
template<class TPMGL(T)> class Iterator;
} } 
namespace x10 { namespace io { 
template<class TPMGL(T)> class ReaderIterator;
} } 
namespace x10 { namespace compiler { 
class Inline;
} } 
namespace x10 { namespace compiler { 
class NonEscaping;
} } 
namespace x10 { namespace io { 
class FileWriter;
} } 
namespace x10 { namespace io { 
class Writer;
} } 
namespace x10 { namespace io { 
class OutputStreamWriter;
} } 
namespace x10 { namespace util { 
class Timer;
} } 
namespace x10 { namespace lang { 
class Long;
} } 
class Graph : public x10::lang::X10Class   {
    public:
    RTT_H_DECLS_CLASS
    
    x10_double FMGL(total_weight);
    
    x10_int FMGL(nb_nodes);
    
    x10_int FMGL(nb_links);
    
    x10::array::Array<x10_int>* FMGL(degrees);
    
    x10::util::ArrayList<x10_int>* FMGL(links);
    
    x10::util::ArrayList<x10_double>* FMGL(weights);
    
    x10::array::Array<x10::util::ArrayList<x10::util::Pair<x10_int, x10_double> >*>*
      FMGL(links_r);
    
    x10_int FMGL(startIndex);
    
    x10_int FMGL(deg);
    
    x10::array::Array<x10_int>* FMGL(indexs);
    
    void _constructor();
    
    static Graph* _make();
    
    void _constructor(x10::lang::String* filename, x10::lang::String* filename_w,
                      x10_int type_file);
    
    static Graph* _make(x10::lang::String* filename, x10::lang::String* filename_w,
                        x10_int type_file);
    
    void _constructor(x10::lang::String* filename, x10_int type_file,
                      x10_int maxNode, x10_int direct, x10_boolean do_renumber);
    
    static Graph* _make(x10::lang::String* filename, x10_int type_file,
                        x10_int maxNode, x10_int direct, x10_boolean do_renumber);
    
    void _constructor(x10::lang::String* filename, x10_int type_file,
                      x10_int maxNode, x10_int edgeNum);
    
    static Graph* _make(x10::lang::String* filename, x10_int type_file,
                        x10_int maxNode, x10_int edgeNum);
    
    virtual x10_double weighted_degree(x10_int node);
    virtual x10_int nb_neighbors(x10_int node);
    virtual x10_double nb_selfloops(x10_int node);
    virtual void display_binary(x10::lang::String* filename,
                                x10::lang::String* filename_w,
                                x10_int type_file);
    virtual void renumber(x10_int type_file);
    static void main(x10::array::Array<x10::lang::String*>* args);
    virtual Graph* Graph____this__Graph();
    virtual void __fieldInitializers11008();
    
    // Serialization
    public: static const x10aux::serialization_id_t _serialization_id;
    
    public: virtual x10aux::serialization_id_t _get_serialization_id() {
         return _serialization_id;
    }
    
    public: virtual void _serialize_body(x10aux::serialization_buffer& buf);
    
    public: static x10::lang::Reference* _deserializer(x10aux::deserialization_buffer& buf);
    
    public: void _deserialize_body(x10aux::deserialization_buffer& buf);
    
};

#endif // GRAPH_H

class Graph;

#ifndef GRAPH_H_NODEPS
#define GRAPH_H_NODEPS
#ifndef GRAPH_H_GENERICS
#define GRAPH_H_GENERICS
#endif // GRAPH_H_GENERICS
#endif // __GRAPH_H_NODEPS
