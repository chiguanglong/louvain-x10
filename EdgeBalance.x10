import x10.util.*;
import x10.util.ArrayList;
import x10.compiler.*;
import x10.lang.*;
import x10.lang.Place;
import org.scalegraph.util.tuple.*;
import x10.array.PlaceGroup;
import x10.array.Region;
import x10.io.File;
import x10.io.FileWriter;
import x10.io.Printer;

public class EdgeBalance {
	var g:Graph;
	var size:int;

	var startP:int;
	var endP:int;
	var band:int;
	var nodeBand:int;

	public def this(){ 
	}

	public def this(filename:String, placeNum:int) { //0.000001; //min_modularity.	
		 
		//read graph separately and init community info 
		g = new Graph(filename, null, 0);
		size = g.nb_nodes;
        band = (size+1) / placeNum;

        var count:Array[int] = new Array[int](placeNum, 0);

        for (var i:int=0; i<placeNum; i++) {
        	startP = i*band;
        	endP = (i!=(placeNum-1)) ? (i+1)*band : (size+1);
			nodeBand = band*i;
        	for (node in startP..(endP-1)) {
				var startIndex:int = 0;
        		var deg:int  = 0;
				if (node == 0) 
			        startIndex = 0;
			    else
			        startIndex = g.degrees(node-1);
			    deg = g.degrees(node);
        		count(i) += deg - startIndex;
        	}
        }

        Console.OUT.println("PlaceNum = " + placeNum);
        for (i in count) {
        	// Console.OUT.println("Place: " + i + ", Edges: " + count(i));
        	Console.OUT.println(count(i));
        }
	}

	public static def main(args:Array[String]) {
		var placeNum:int = 0;
		var filename:String = null;

		for (i in args) {
            if (args(i).equals("-i")) {
                filename = args(i+1);
            } else if(args(i).equals("-p")) {
            	placeNum = Int.parse(args(i+1));
            }   
        }
		val balance = new EdgeBalance(filename, placeNum);
    }
}